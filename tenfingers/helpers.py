import gzip
import io
from struct import pack, unpack
from Cryptodome import Random


def network_independent_pack_size_to_8bytes(size):
    return pack('>Q', size)  # Q equals unsigned long long


def network_independent_unpack_8bytes_to_size(raw_data):
    if len(raw_data) >= 8:
        return unpack('>Q', raw_data[:8])[0]
    print('#Error network_independent_unpack_8bytes_to_size() needs at least 8 bytes')
    return 0


# Header for the data before being encrypted and saved off
# [8 bytes rnd][8 bytes digest size][digest variable size][plaintext (variable size)]
def generate_new_header(digest, original_file_name, is_folder):
    rnd = Random.get_random_bytes(8)
    digest_8byte_size = network_independent_pack_size_to_8bytes(len(digest))
    orig_file_name_8byte_size = network_independent_pack_size_to_8bytes(len(original_file_name))
    f = b'0'
    if is_folder:
        f = b'1'
    return rnd + digest_8byte_size + digest + orig_file_name_8byte_size + original_file_name.encode() + f


# Returns digest + plaintext data from a decrypted aes file
def extract_digest_and_plaintext_from_data_with_header(decrypted_aes_file):
    p = 8
    compressed_digest_size = decrypted_aes_file[p:p+8]
    p += 8
    digest_size = network_independent_unpack_8bytes_to_size(compressed_digest_size)
    digest = decrypted_aes_file[p:p+digest_size]
    p += digest_size

    compressed_orig_file_name_size = decrypted_aes_file[p:p + 8]
    p += 8
    orig_file_name_size = network_independent_unpack_8bytes_to_size(compressed_orig_file_name_size)
    original_file_name = decrypted_aes_file[p:p + orig_file_name_size]
    p += orig_file_name_size

    f = decrypted_aes_file[p:p + 1]
    p += 1
    is_folder = f == b'1'

    # Now p point on the plain data

    print('DIGEST digest_size:', digest_size)
    print('DIGEST ORIGFILE SIZE, data:', orig_file_name_size, original_file_name)
    print('DIGEST is_folder:', f, is_folder)
    return digest, decrypted_aes_file[p:], original_file_name.decode(), is_folder


# When you get an int over the network that was encoded like this: str(i).encode()
# Returns None if there is trouble
def network_byte_to_int(byte_chunk):
    if byte_chunk:
        byte_chunk = byte_chunk.decode()
        if byte_chunk.isdigit():
            byte_chunk = int(byte_chunk)
        else:
            byte_chunk = None  # Bail out on this one, node is serving crap to us
    return byte_chunk


# Returns a string formatted for human reading, input file_size in bytes
def human_readable_file_size(size_in_bytes, short=False):
    if short:
        suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    else:
        suffixes = ['Bytes', 'KiloBytes', 'MegaBytes', 'GigaBytes', 'TeraBytes', 'PetaBytes']
    i = 0
    while size_in_bytes >= 1024 and i < len(suffixes)-1:
        size_in_bytes /= 1024.
        i += 1
    f = ('%.2f' % size_in_bytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, suffixes[i])


# Gzip byte data
def g_zip(data: bytes) -> bytes:
    out = io.BytesIO()
    with gzip.GzipFile(fileobj=out, mode='wb') as fo:
        fo.write(data)
    bytes_obj = out.getvalue()
    return bytes_obj


# GUnzip byte data
def g_unzip(bytes_obj: bytes) -> bytes:
    return gzip.decompress(bytes_obj)


# Tries to figure out the public ip address for the network this PC is on
def get_public_ip():
    ip = '127.0.0.1'
    import socket
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Connect to Mindoki.com and ask for the IP that asked (e.g probably our public ip address)
        s.connect(("www.mindoki.com", 80))
        s.sendall(b"GET /ip_responder.php HTTP/1.1\r\nHost: mindoki.com\r\nAccept: text/html\r\n\r\n")
        r = s.recv(4096).decode()

        split = r.split('MINDOKI_IP_RESPONDER_')
        if len(split) == 2:
            split = split[1].split('_END')
            if len(split) == 2:
                ip = split[0]
            else:
                print('Got mal formed answer from IP responder:', r)

        else:
            print('Got mal formed answer from IP responder:', r)
    except:
        print("Could not connect to IP responder, please configure your public IP:PORT manually")

    return ip
