# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

echo '------------- Build the setup ----------------'

################################################
# Build a listener only

rm dist/setup
rm dist/setup.exe
rm setup
rm setup.exe

# Clean up the package folder
rm -f -r ./tenfingers_release/*
mkdir tenfingers_release
mkdir tenfingers_release/py_source


echo '[Clean up build directories]'

rm -f -r dist/*

echo '[make executables:]'

if [ $(uname -o) == "Msys" ]
then
	echo 'Build Windows version'
	venv/Scripts/pyinstaller.exe setup.py --clean --onefile
else
	echo 'Build Linux version'
	pyinstaller setup.py --clean --onefile
fi

rm listener.spec
rm setup.spec
rm 10f.spec

mkdir tenfingers_release/bin
cp -f -u dist/setup* ./

echo '[Done.]'

