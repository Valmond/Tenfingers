import socket

# Connect to this socket to get your public IP
# It only works for IPv4

my_ip = "192.168.0.46"
listen_port = 46000;

# Start listener
socket = socket.socket();
socket.bind((my_ip, listen_port));
socket.listen();

print("Listens");

# Accept incoming requests
while(True):

    (client_socket, client_ip) = socket.accept();

    msg = str(client_ip)
    client_socket.send(msg.encode());

    print("Sent: %s, to client"%(msg));
    
    client_socket.close();
