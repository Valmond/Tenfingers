# Copyright (C) 2023 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import sys
import os
import tempfile
import glob

from access import insert_folder
from get_link_from_data import create_and_save_link_database
from get_data_from_link import get_link_file_version


def remove_10f_files(folder_):
    # Remove all old 10f .files
    for file_to_delete in glob.glob(folder_ + "/*.10f"):
        os.remove(file_to_delete)
        #print("Delete old lingering tenfingers files:", file_to_delete)


# As tenfingers files are exported in the tenfingers soure directory, lets make a convenient function to move them elsewhere
def move_tenfingers_files_from_src(target_folder):
    pass


def generate_index(book_name, base_folder, intermediate_folder=''):
    actual_folder = str(os.path.join(base_folder, intermediate_folder))
    files = []
    folders = []
    for a in os.listdir(actual_folder):
        full_path_file = os.path.join(actual_folder, a)
        if os.path.isdir(full_path_file):
            folders.append(a)
            generate_index(book_name, base_folder, os.path.join(intermediate_folder, a))
        else:
            if os.path.isfile(full_path_file):
                # Skip the index.htm files as it is this specific file we are generating
                if a != 'index.htm':
                    files.append(a)

    if intermediate_folder == '':
        index_html = '<head></head><body>' + book_name + '<br>\n<br>\n<br>\n'
    else:
        index_html = '<head></head><body>' + intermediate_folder + '<br>\n<br>\n<br>\n'

    # Directories
    if len(folders) > 0:
        index_html += 'Directories:<br>\n'
        for d in folders:
            index_html += '<a href="' + d + '/index.htm">' + os.path.join(intermediate_folder, d) + '</a><br>\n'
    index_html += '<a href="../index.htm">..</a><br>\n'
    index_html += '<hr>\n'

    # Files in this directory
    if len(files) > 0:
        index_html += 'Files:<br>\n'
    for f in files:
        index_html += '<a href="' + f + '">' + f + '</a><br>\n'

    index_html += '<br>\n'

    index_html += '<body>'
    with open(os.path.join(actual_folder, 'index.htm'), 'w') as f:
        f.write(index_html)


def make_book(book_name, folder):
    print("Name of the book:", book_name)
    print("Folder          :", folder)

    remove_10f_files(folder)

    chapters = []

    # Make a temporary directory for all our *.10f files we will generate
    with tempfile.TemporaryDirectory() as tmp_dir_name:
        #print('created temporary directory: ', tmp_dir_name)

        #print('Generate the book:')

        # Get all base level directories and insert them into the tenfingers database
        for chapter in os.listdir(folder):
            file_path = os.path.join(folder, chapter)
            if os.path.isdir(file_path):
                print("Chapter found:", chapter)
                # Make the local path
                local_path = os.path.relpath(file_path, folder)

                # Make an index.html for this chapter
                generate_index(book_name, os.path.join(folder, chapter))

                # Make a chapter.10f link for this chapter
                # print("file_path=", file_path, "folder=", folder)
                # print("From folder", folder, " insert", local_path)
                insert_folder(chapter, folder, local_path, silent=True)

                # Export the link files
                create_and_save_link_database(chapter, tmp_dir_name, silent=True)

                tf_name = chapter + ".10f"

                chapters.append(chapter)

        # Make an index.htm for all the folders
        #book_version = get_link_file_version(translation_file)
        chapters.sort()
        index_html = '<head></head><body>' + book_name + '<br>\n<br>\n'
        index_html += 'Chapters:<br>\n<br>\n'
        for chapter in chapters:
            # Get the version of this chapter
            chapter_version = get_link_file_version(os.path.join(tmp_dir_name, chapter + '.10f'))
            index_html += '<a href="' + chapter + '/index.htm?version=' + str(chapter_version) + '">' + chapter + '</a> Version ' + str(chapter_version) + '<br>\n'
        index_html += '<body>'
        with open(os.path.join(tmp_dir_name, 'index.htm'), 'w') as f:
            f.write(index_html)

        # Make/update/share the book.10f (including the index.htm file + the 10f files in the base directory)
        print('Insert or update the book link [' + book_name + ']')
        insert_folder(book_name, tmp_dir_name, './', silent=True)

    # Clean up:
    remove_10f_files(folder)

    # After clean up
    print('Export the book link [' + book_name + ']')
    create_and_save_link_database(book_name, '/home/loulou/Desktop/TenfingersBook', silent=True)

    print('Done!')


if len(sys.argv) != 3:
    print('Usage:')
    print('tenfingers_bool name_of_book folder')
    print('Example:')
    print('python3 ./tenfingers_bool.py my_stuff /home/myname/my_stuff')
    print('This will make or update a link for any file (except tenfinger files) in the folder /home/myname/my_stuff (recursively)')
    print('and subsequently make or update a shared tenfingers link containing them all by the name my_stuff')
    print('it will be exported (or updated) as:')
    print('my_stuff.10f')
    print('my_stuff_tr.10f')
    print('my_stuff_sub.10f')
    print('')
    print('Share these three files with people you want to share \'my_stuff\' with')
    print('')
    print('Note: do not put an index.htm in a chapter, it will be overwritten.')
else:
    make_book(sys.argv[1], sys.argv[2])
