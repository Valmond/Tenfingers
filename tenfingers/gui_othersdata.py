import os
import shutil
import wx
import wx.lib.mixins.listctrl as listmix
from wx.lib.agw import ultimatelistctrl as ULC

APPNAME = 'Tenfingers sharing protocol simple GUI'
APPVERSION = '1.0'
MAIN_WIDTH = 800
MAIN_HEIGHT = 500

OTHERS_10F_DIR = "gui_others_data"  # Here will other peoples 10f files be stored (10f, tr/sub, atf)


########################################################################

class GfxOthersData(wx.Panel, listmix.ColumnSorterMixin):
    def __init__(self, parent):
        # Declare variables so python won't throw a fit
        self.all_other_data = {}
        self.information_text = None
        self.list_ctrl = None

        # Setup
        self.parent = parent
        self.setup()

    def get_link_version(self, link_file_path):  # Returns -1 on error, else the links version
        from get_data_from_link import get_link_file_version
        v = get_link_file_version(link_file_path)
        return v

    def setup(self):

        wx.Panel.__init__(self, self.parent, -1, style=wx.WANTS_CHARS, size=(MAIN_WIDTH, MAIN_HEIGHT))

        self.information_text = wx.StaticText(self.parent, label="...")

        self.all_other_data = {}

        self.list_ctrl = ULC.UltimateListCtrl(self, -1, agwStyle=ULC.ULC_REPORT | ULC.ULC_HAS_VARIABLE_ROW_HEIGHT)
        self.list_ctrl.InsertColumn(0, "Name", width=240)
        self.list_ctrl.InsertColumn(1, "Info", wx.LIST_FORMAT_RIGHT)
        self.list_ctrl.InsertColumn(2, "Download")

        self.all_other_data = self.get_others_data()  # returns a numbered dict containing tuples
        items = self.all_other_data.items()
        index = 0
        for key, data in items:
            # Insert the link name
            link_name = data[0]
            pos = self.list_ctrl.InsertStringItem(index, link_name)

            # Get its version
            file_path = os.path.join(OTHERS_10F_DIR, link_name)
            link_version = self.get_link_version(file_path)
            self.list_ctrl.SetStringItem(index, 1, "version: " + str(link_version))

            # self.list_ctrl.SetStringItem(index, 2, data[2])
            button = wx.Button(self.list_ctrl, id=wx.ID_ANY, label="Download")
            self.list_ctrl.SetItemWindow(pos, col=2, wnd=button, expand=True)
            self.list_ctrl.SetItemData(index, key)

            button.Bind(wx.EVT_BUTTON, self.get_data_from_link)
            button.SetName(str(index))  # Store off the index in its name until we figure out how to do it better

            #self.all_other_data[index] = (data[0], "")  # Save the file
            index += 1

        # Now that the list exists we can init the other base class,
        # see wx/lib/mixins/listctrl.py

        # Add a "Insert new file data button
        self.insert_button = wx.Button(self, id=wx.ID_ANY, label="Track a link", pos=(640, 5))
        self.insert_button.Bind(wx.EVT_BUTTON, self.insert_new_link)

        listmix.ColumnSorterMixin.__init__(self, 3)
        self.Bind(wx.EVT_LIST_COL_CLICK, self.OnColClick, self.list_ctrl)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.list_ctrl, 1, wx.ALL | wx.EXPAND, 5)
        #sizer.Add(self.information_text, 0, wx.ALL | wx.EXPAND, 5)
        self.SetSizer(sizer)

    def get_data_from_link(self, event):
        print("get_data_from_link")
        download_id = event.GetEventObject().GetName()  # Yeah I know...
        print("", download_id)
        row = self.all_other_data[int(download_id)]
        link_name = row[0]
        # generate the folder for where we should putput it
        from pathlib import Path
        base_name = Path(link_name).stem
        print("Download ", link_name, flush=True)  # [0] = the name of the download
        link_file = os.path.join(OTHERS_10F_DIR, link_name)
        output_folder = os.path.join(OTHERS_10F_DIR, base_name)
        import access as ac
        ac.download('./' + link_file, './' + output_folder)
        from gui_main import Notifier
        Notifier.notify("The content of [" + link_name + "] was downloaded in " + output_folder)

        # Open the folder in Linux "Explorer"
        os.system('xdg-open "%s"' % output_folder)

        # As maybe versions have changed, just do a brutal GUI update. Maybe check if we actually got a newer version...
        self.setup()

    def set_information_text(self, txt):
        self.information_text.SetLabel("Info: " + txt)

    @staticmethod
    def _copy(src, dst):  # Just eat the try/catch for the optionnal files
        try:
            shutil.copy(src, dst)
        except:
            return False
        return True

    def insert_new_link(self, event):
        fdlg = wx.FileDialog(None, "Choose a tenfingers link to track:", "", "", "*.10f", wx.FD_OPEN)
        if fdlg.ShowModal() == wx.ID_OK:
            file_path = fdlg.GetPath()
            from pathlib import Path
            name = Path(file_path).stem  # eg. test
            #name_w_ext = os.path.basename(file_path)  # ex test.txt
            base_folder = os.path.dirname(file_path)
            print("Track data:", file_path, " with the name", name, "from the base folder", base_folder)
            # Don't allow tracking of _tr or _sub files:
            if name.endswith("_tr"):
                name = name[:-3]  # remove the _tr
            if name.endswith("_sub"):
                name = name[:-4]  # remove the _sub
            # does the file exist
            base_file_path_no_ext = os.path.join(base_folder, name)
            from gui_main import Notifier
            if os.path.isfile(base_file_path_no_ext + ".10f") and self._copy(base_file_path_no_ext + ".10f", OTHERS_10F_DIR):
                self._copy(base_file_path_no_ext + "_tr.10f", OTHERS_10F_DIR)
                self._copy(base_file_path_no_ext + "_sub.10f", OTHERS_10F_DIR)
                self._copy(base_file_path_no_ext + "_tr.atf", OTHERS_10F_DIR)
                self._copy(base_file_path_no_ext + "_sub.atf", OTHERS_10F_DIR)
                Notifier.notify("The link [" + name + "] was imported, you can now download its content)")

                # Refresh the GUI
                self.setup()
            else:
                Notifier.notify("The link [" + name + "] could not be copied)", "error")







    def GetListCtrl(self):
        return self.list_ctrl

    #----------------------------------------------------------------------
    def OnColClick(self, event):
        pass

    @staticmethod
    def get_others_data():  # Scans the folder holding "others" 10f files and returns a list of all the base files (so excluding translation & substitution files)
        other_list = {}
        # Todo: get the version !
        index = 0
        for file in os.listdir(OTHERS_10F_DIR):
            if file.endswith(".10f"):
                if (not file.endswith("_sub.10f")) and (not file.endswith("_tr.10f")):
                    print(os.path.join(OTHERS_10F_DIR, file))
                    other_list[index] = (file,)
                    index += 1
        return other_list


########################################################################
class MyForm(wx.Frame):

    #----------------------------------------------------------------------
    def __init__(self):
        wx.Frame.__init__(self,None,wx.ID_ANY,'%s v%s' % (APPNAME,APPVERSION),size=(MAIN_WIDTH,MAIN_HEIGHT),style=wx.MINIMIZE_BOX | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX | wx.CLIP_CHILDREN)

        # Add a panel so it looks the correct on all platforms
        panel = GfxOthersData(self)

#----------------------------------------------------------------------
# Run the program
if __name__ == "__main__":
    app = wx.App(False)
    frame = MyForm()
    frame.Show()
    app.MainLoop()