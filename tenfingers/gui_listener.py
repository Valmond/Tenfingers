import subprocess
import time

import wx

# Todo: put this in the programs parameters (not in the tenfingers database ofc)
PARAMETER_START_LISTENER_AT_STARTUP = False  # True

class GfxNode:
    def __init__(self, panel):

        self.information_text = wx.StaticText(panel, label="...")

        self.index = 0

        # Listener process
        self.node_process = None
        self.listener_subprocess = None

        # Set up the GUI

        self.start_button = wx.Button(panel, wx.ID_ANY, 'Start node', (10, 10))
        self.start_button.Bind(wx.EVT_BUTTON, self.start_stop)

        # Output from listener
        self.listener_output = wx.TextCtrl(panel, -1, "Out put from the listener node.", (480, 320), size=(500, 300), style= wx.TE_MULTILINE)

        # Best known nodes?
        #self.list_ctrl = wx.ListCtrl(panel, size=(-1, 100), style=wx.LC_REPORT | wx.BORDER_SUNKEN)
        #self.list_ctrl.InsertColumn(1, "Yolo")
        #self.list_ctrl.InsertColumn(2, "Yolo2")
        #self.list_ctrl.InsertColumn(3, "Yolo3")

        sizer = wx.BoxSizer(wx.VERTICAL)
        #sizer.Add(self.list_ctrl, 0, wx.ALL | wx.EXPAND, 5)
        sizer.Add(self.start_button, 0, wx.ALL | wx.CENTER, 5)
        sizer.Add(self.listener_output , 0, wx.ALL | wx.CENTER, 5)
        sizer.Add(self.information_text, 0, wx.ALL | wx.EXPAND, 5)
        panel.SetSizer(sizer)

        # GUI setup done

        if PARAMETER_START_LISTENER_AT_STARTUP:
            self.start_listener()

    def __del__(self):
        if self.is_listener_running():  # Kill/Stop it
            self.listener_subprocess.kill()
    #

    def update_listener_output(self):  # Read the log from the listener and put it in the text widget
        try:
            with open("./listener.log", 'r') as f:
                lines = str(f.read())
                t = type(lines)
                print(lines)
                self.listener_output.SetValue(str(lines))
        except:
            self.listener_output.SetValue("Could not read the listener.log file.")

    def clear_log_file(self):
        try:
            with open("./listener.log", 'w') as f:
                pass
        except:
            pass


    def start_listener(self):
        if not self.is_listener_running():
            self.clear_log_file()
            self.listener_subprocess = subprocess.Popen(["python3", "./listener.py"])
            self.set_information_text("Node started.")
            self.start_button.SetLabel("Stop node")
            time.sleep(2)  # Let the listener write its log
            self.update_listener_output()

    def stop_listener(self):
        if self.is_listener_running():  # Kill/Stop it
            print("Stop the node", flush=True)
            self.start_button.SetLabel("Start node")
            # Stop the subprocess
            self.listener_subprocess.kill()
            self.listener_subprocess = None

            self.clear_log_file()
            self.set_information_text("Node stopped.")


    def start_stop(self, event):
        if self.is_listener_running():
            self.stop_listener()
        else:  # Start it all up
            self.start_listener()

    #
    def is_listener_running(self):
        return self.listener_subprocess is not None
        # return self.node_process.is_alive()  # Doesn't function as expected

    def set_information_text(self, txt):
        self.information_text.SetLabel("Info: " + txt)

    # Tests & checks

    def is_listener_ip_accessible(self):  # Is it possible for other nodes to contact the listener from WAN internet
        # Todo
        return False


    def button_pressed(self, event):
        print("Button pressed.")

    def add_line(self, event):
        line = "%s Some data, maybe a film" % self.index
        self.list_ctrl.InsertItem(self.index, "")  # Inserts a New shiny line
        self.list_ctrl.SetItem(self.index, 0, line)  # Adds to the new shiny line
        self.list_ctrl.SetItem(self.index, 1, "1.5MB")
        self.list_ctrl.SetItem(self.index, 2, "2/4 shares")
        self.index += 1
