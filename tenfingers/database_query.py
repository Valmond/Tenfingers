# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import os
import re
import shutil

import database_connection as dc
from helpers import human_readable_file_size
from configuration import get_configuration_value, get_exclude_ip_list
from accept_swicharoo import do_accept_swicharoo
import locator
import filepath as fp

"""
Query helper for the database
"""


def get_is_testrun():  # True for testruns so we can generate unsafe small RSA keys
    if get_configuration_value('_unsafe_small_rsa_keys', 0) == 1:
        return True
    return False


def get_owned_file_size(data_owned_id):
    name = get_name_of_owned_data_id(data_owned_id)
    if name is not None:
        name = str(name) + '.aes'
        fp = os.path.join(get_configuration_value("data_owned_folder"), name)
        if os.path.exists(fp):
            return os.path.getsize(fp)
        else:
            print("#Warning: database contains missing file [3]:", fp)
    return 0


# Returns true if IP is on the exclude list in the config file
def ip_on_exclude_list(ip):
    exclude_ip_list = get_exclude_ip_list()
    for exclude in exclude_ip_list:  # WAS several exclude regex, is for now just one.
        if re.search(exclude, ip):
            return True
    return False


# Returns true if this IP:PORT is my address
def is_my_ip_port(ip, port):
    if isinstance(port, int):
        port = str(port)
    my_port = get_configuration_value('port')
    if isinstance(my_port, int):
        my_port = str(my_port)
    return get_configuration_value('ip') == ip and my_port == port


#

# -------------------------- Links -----------------------------------

# --- The shared data (that we share for another node)


# Returns None if 'name'+'rsa_pub' does not have an entry in the database
# Else returns a row of NAME, version, KEY, IV, RSA_PUB
def get_link_data_by_name_and_rsapub(name, rsa_pub):  # Getting data for a link not owned by us, but stored by us (from a swicharoo)
    cursor = dc.execute("SELECT id, name, data_version, iv FROM data_shared WHERE name = ? AND pubkey = ?", (name, rsa_pub))
    rows = cursor.fetchall()
    if len(rows) > 0:
        return rows[0]

    # So we don't share the data for anyone, maybe it's our original data someone wants?
    cursor = dc.execute("SELECT id, name, data_version, iv FROM data_owned WHERE name = ? AND pubkey = ?", (name, rsa_pub))
    rows = cursor.fetchall()
    if len(rows) > 0:
        return rows[0]

    return None


# This is needed for when we let another node update its data
# Returns the shared_data id, or zero if not found
def get_shared_data_id_by_name_and_rsa_pub(name, rsa_pub):  # Getting data for a link not owned by us, but stored by us (from a swicharoo)
    tmp = dc.query_one("SELECT id FROM data_shared WHERE name = ? AND pubkey = ?", (name, rsa_pub))
    if tmp is None:
        return 0
    return tmp

# -------------------------- Folders -----------------------------------


# Tries to insert, there is a unique key for name+pubkey making duplicates not possible
# Makes a new folder (auto increment) and savces it in the database
# Returns the shared_data_id
def insert_data_shared(name, pubkey):
    dc.insert('INSERT INTO data_shared (name, pubkey) VALUES(?, ?)', (name, pubkey))
    # Refact this, we just need 'folder' to be the next int (a unique int)
    shared_data_id = get_shared_data_id_by_name_and_rsa_pub(name, pubkey)
    dc.update('UPDATE data_shared SET folder = ? WHERE name = ? AND pubkey = ?', (str(shared_data_id), name, pubkey))
    return shared_data_id  # get_shared_data_folder_by_name_and_rsa_pub(name, pubkey)


# To use with dummy files. Name must be unique!
def insert_data_owned_dummy(name):
    pubkey = "dummy"
    dc.insert('INSERT INTO data_owned (name, pubkey, wanted_fingers) VALUES(?, ?, ?)', (name, pubkey, 0))
    # Refact this, we just need 'folder' to be the next int (a unique int)
    owned_data_id = get_link_data_id_by_name(name)
    return owned_data_id  # get_shared_data_folder_by_name_and_rsa_pub(name, pubkey)


def get_shared_data_folder_by_name_and_rsa_pub(name, rsa_pub):  # Getting data for a link not owned by us, but stored by us (from a swicharoo)
    tmp = dc.query_one("SELECT folder FROM data_shared WHERE name = ? AND pubkey = ?", (name, rsa_pub))
    if tmp is None:
        return ''
    return str(tmp)


def set_shared_data_version(shared_data_id, new_version, new_nonce):
    dc.update("UPDATE data_shared SET data_version = ?, iv = ? WHERE id = ?", (new_version, new_nonce, shared_data_id))


def get_shared_data_version(shared_data_id):
    tmp = dc.query_one("SELECT data_version FROM data_shared WHERE id = ? LIMIT 1", (shared_data_id,))
    if tmp is None:
        return 0
    return tmp


# Returns None if not found
def get_shared_data_name(shared_data_id):
    tmp = dc.query_one("SELECT name FROM data_shared WHERE id = ? LIMIT 1", (shared_data_id,))
    return tmp


def get_shared_data_folder_number(shared_data_id):
    tmp = dc.query_one("SELECT folder FROM data_shared WHERE id = ? LIMIT 1", (shared_data_id,))
    return tmp


# --- Our data that we would like other nodes to share


# Just get a bunch of data from the existing our data
def get_our_data_information(name):  # Getting data for a link owned by us
    cursor = dc.execute("SELECT id, key, iv, next_safe_iv, data_version FROM data_owned WHERE NAME = ?", (name,))

    rows = cursor.fetchall()
    if len(rows) > 0:
        return rows[0]
    return None


# Returns None if 'name' does not have an entry in the database
# Else returns a row of NAME, KEY, IV
def get_link_data_by_name(name):  # Getting data for a link owned by us, caller will get the RSA pub key from the database
    cursor = dc.execute("SELECT name, key, iv FROM data_owned WHERE name = ? ", (name,))  # comme to make the () a tuple...

    rows = cursor.fetchall()
    if len(rows) > 0:
        return rows[0]
    return None


# Returns None if 'name' does not have an entry in the database
# Else returns id of data_owned
def get_link_data_id_by_name(name):  # Getting data for a link owned by us
    cursor = dc.execute("SELECT id FROM data_owned WHERE name = ? ", (name,))

    rows = cursor.fetchall()
    if len(rows) > 0:
        return rows[0][0]
    return None


# For our data
def get_rsa_key_by_data_id(data_owned_id, private_key=False):  # Getting rsa keys for a link owned by us
    if private_key:
        cursor = dc.execute("SELECT privkey FROM data_owned WHERE id = ? ", (data_owned_id,))
    else:
        cursor = dc.execute("SELECT pubkey FROM data_owned WHERE id = ? ", (data_owned_id,))
    rows = cursor.fetchall()
    if len(rows) > 0:
        return rows[0][0]
    return None


# Returns None if 'name' does not have an entry in the database
# Else returns a row of NAME
def get_name_of_owned_data_id(data_owned_id):  # Getting data for a link owned by us, caller will get the RSA pub key from the database
    return dc.query_one('SELECT name FROM data_owned WHERE id = ?', (data_owned_id,))


def get_nonce_from_data_id(data_owned_id):
    return dc.query_one('SELECT iv FROM data_owned WHERE id = ?', (data_owned_id,))


def get_random_local_data_id():  # todo: "WHERE wanted_fingers < SUM( ... fingers ...)"
    # As it can return zero rows, we'll need to do it the old way
    cursor = dc.execute("SELECT id FROM data_owned WHERE key != '' ORDER BY RANDOM() LIMIT 1;")
    rows = cursor.fetchall()
    if len(rows) > 0:
        return rows[0][0]
    return None


# Return one of our shared data that has no one sharing it yet (no finger)
# Or if they have all fingers, return the lowest one
def get_random_our_not_shared_data():
    rows = dc.query_all("SELECT id FROM data_owned WHERE wanted_fingers > 0 ORDER BY RANDOM()")
    best_fingers = 0
    best_data_id = None
    for row in rows:
        data_owned_id = row[0]
        fingers = dc.query_one("SELECT count(data_owned.id) FROM data_owned INNER JOIN finger ON finger.data_owned_id = data_owned.id WHERE data_owned.id = ?",
                               (data_owned_id, ))
        if fingers == 0:
            return data_owned_id
        if best_fingers == 0:
            best_fingers = fingers
            best_data_id = data_owned_id
        else:
            if fingers < best_fingers:
                best_fingers = fingers
                best_data_id = data_owned_id
    return best_data_id


def get_wanted_fingers(data_owned_id):
    return dc.query_one('SELECT wanted_fingers FROM data_owned WHERE id = ?', (data_owned_id,))


def set_wanted_fingers(owned_data_id, wanted_fingers):
    dc.update("UPDATE data_owned SET wanted_fingers = ? WHERE id = ?", (wanted_fingers, owned_data_id))


def get_owned_data_version(data_owned_id):
    return dc.query_one('SELECT data_version FROM data_owned WHERE id = ?', (data_owned_id,))


def set_owned_data_version(owned_data_id, new_version):
    dc.update("UPDATE data_owned SET data_version = ? WHERE id = ?", (new_version, owned_data_id))


def set_next_data_version(owned_data_id):
    dc.update("UPDATE data_owned SET data_version = data_version + 1 WHERE id = ?", (owned_data_id, ))
#

# -------------------------- Nodes -----------------------------------

#


# Takes any node and returns its ID, or None if no nodes
def get_random_node_id(only_validated_nodes=True, exclude_node_by_rsa=None):
    ip = get_configuration_value('ip')
    port = get_configuration_value('port')

    if exclude_node_by_rsa is None:
        exclude_node_by_rsa = "DUMMY"

    if only_validated_nodes:
        cursor = dc.execute('SELECT id FROM node WHERE last_connection_unixtime != 0 AND (ip != ? OR port != ?) AND pubkey != ? ORDER BY RANDOM() LIMIT 1;', (ip, port, exclude_node_by_rsa))
    else:
        cursor = dc.execute('SELECT id FROM node WHERE (ip != ? OR port != ?) AND pubkey != ? ORDER BY RANDOM() LIMIT 1;', (ip, port, exclude_node_by_rsa))
    rows = cursor.fetchall()
    if rows is None:
        return None
    if len(rows) > 0:
        return rows[0][0]
    return None


def get_node_data(node_id):  # returns ip, port, RSAPubKey or None
    cursor = dc.execute("SELECT ip, port, pubkey FROM node WHERE id = ? LIMIT 1", (node_id,))
    rows = cursor.fetchall()
    if len(rows) > 0:
        return rows[0]
    return None


def node_exists(rsa_pub_key):
    cursor = dc.execute("SELECT id FROM node WHERE pubkey = ? LIMIT 1", (rsa_pub_key, ))
    rows = cursor.fetchall()
    return len(rows) > 0


def get_node_id(rsa_pub_key):  # ip:port no longer used to differentiate nodes
    tmp = dc.query_one("SELECT id FROM node WHERE pubkey = ?", (rsa_pub_key, ))
    if tmp is None:
        return 0
    return tmp


# New version
def get_node_id_from_rsapubkey(rsa_pub_key):
    tmp = dc.query_one("SELECT id FROM node WHERE pubkey = ?", (rsa_pub_key, ))
    if tmp is None:
        return 0
    return tmp


# Which type of file is it (base, trans, sub)
# 0 = base_data
# 1 = translation_data
# 2 = substitution data
# None = does not exist
def get_translation_file_type(data_owned_id):
    data_type = dc.query_one('SELECT address_translation_file FROM data_owned WHERE id = ?', (data_owned_id,))
    if not data_type or not data_type.isdigit():
        print("#ERROR trying to get get_translation_file_type for inexisting (owned) data id: " + str(data_owned_id))

    return int(data_type)


# When inserting a node, it is not explicitly stated that they know our IP:PORT address
# This will be dealt by with the scheduler, or if you
# are Sure the node knows our address, you can use update_our_address_for_node() to set it,
# sparing the scheduler some work and the need to wait for it to be done.
def insert_node(ip, port, rsa_pub_key, rem='UNKNOWN PROVENANCE'):
    # Check if is our address (happens when we change IP:PORT config and another node serves us our old address)
    if not node_exists(rsa_pub_key):
        _, pub = get_rsa_hex_keys()
        if rsa_pub_key != pub:
            # Check it is not on the avoid list and not our IP:PORT
            if not ip_on_exclude_list(ip) and not is_my_ip_port(ip, port):
                print('Got new node, insert it: INSERT new node: ', ip, port, rsa_pub_key[-20:])
                dc.insert("INSERT INTO node (ip, port, pubkey, rem) VALUES(?, ?, ?, ?)", (ip, port, rsa_pub_key, rem))
                # print('INSERT NODE rsa end:', rsa_pub_key[:12])
        # else:
        #    print("Refused node address: Got served our own stale address")


# Update the nodes address
def update_node_address(node_id, ip, port):
    connection = dc.get_connection()
    cursor = connection.cursor()
    cursor.execute("UPDATE node SET ip = ?, port = ? WHERE id = ? ", (ip, port, node_id))
    connection.commit()


# 'Our' addresses are what we know other nodes think is our address (used when we change address)
def update_our_address_for_node(node_id, our_ip, our_port):
    connection = dc.get_connection()
    cursor = connection.cursor()
    print("node_id, ourip ourport", node_id, our_ip, our_port, flush=True)
    cursor.execute("UPDATE node SET our_ip = ?, our_port = ? WHERE id = ? ", (our_ip, our_port, node_id))
    connection.commit()


# Check if the node have our address (otherwise it means we have changed our address, and the node needs to be contacted and given our new address)
def is_nodes_our_address_correct(node_id, our_ip, our_port):
    cursor = dc.execute("SELECT id FROM node WHERE id = ? AND our_ip = ? AND out_port = ? ", (node_id, our_ip, our_port))
    rows = cursor.fetchall()
    return len(rows) > 0


def update_node_quality(node_id, success):
    connection = dc.get_connection()
    cursor = connection.cursor()
    if success:
        cursor.execute("UPDATE node SET connection_tries=connection_tries+1, last_connection_unixtime=DateTime('now') WHERE id = ? ", (node_id,))  # comme to make the () a tuple...
    else:
        cursor.execute("UPDATE node SET connection_tries=connection_tries+1, connection_fails=connection_fails+1 WHERE id = ? ", (node_id,))  # comme to make the () a tuple...
    connection.commit()


# returns the node id a finger points to
def get_node_from_finger(finger_id):
    tmp = dc.query_one("SELECT node_id FROM finger WHERE id = ?", (finger_id, ))
    if tmp is None:
        return 0
    return tmp


# Deletes this data that we own, In The DataBase. Returns true if the data did exist, false otherwise
def delete_owned_data(owned_data_name):
    data_owned_id = get_link_data_id_by_name(owned_data_name)
    if data_owned_id:
        dc.update("DELETE FROM data_owned WHERE name = ?", (owned_data_name,))
        return True
    return False


#

# -------------------------- Fingers -----------------------------------

#

# How many fingers point on this data
def get_number_of_fingers(data_owned_id):
    data_version = get_highest_version(data_owned_id)
    tmp = dc.query_one("SELECT COUNT(*) FROM finger WHERE data_owned_id = ? AND data_version = ?", (data_owned_id, data_version))
    if tmp is None:
        return 0
    return tmp


# version or None if the finger does not exists
def get_finger_version(finger_id):
    return dc.query_one("SELECT data_version FROM finger WHERE id = ?", (finger_id, ))


# returns all finger_id pointing to this data
def get_all_fingers(data_owned_id):
    r = []
    if data_owned_id:
        rows = dc.query_all("SELECT id FROM finger WHERE data_owned_id = ?", (data_owned_id, ))
        for row in rows:  # maybe this is not necessary... but as it should work, lets check that later
            r.append(row[0])
    return r


# returns all data_owned_id pointed to by this finger
def get_all_data_node_shares(node_id):
    r = []
    if node_id:
        rows = dc.query_all("SELECT data_owned_id FROM finger WHERE node_id = ?", (node_id, ))
        for row in rows:  # maybe this is not necessary... but as it should work, lets check that later
            r.append(row[0])
    return r


# returns all finger_id pointing to this data, with a specific version
def get_fingers_by_version(data_owned_id, version):
    rows = dc.query_all("SELECT id FROM finger WHERE data_owned_id = ? AND data_version = ?", (data_owned_id, version))
    r = []
    for row in rows:  # maybe this is not necessary... but as it should work, lets check that later
        r.append(row[0])
    return r


# Runs through the fingers and returns the highest data_version, or 0 if none
def get_highest_version(data_owned_id):
    fingers = get_all_fingers(data_owned_id)
    data_version = 0
    for finger_id in fingers:
        finger_version = get_finger_version(finger_id)
        if finger_version > data_version:
            data_version = finger_version
    return data_version


# returns node_id, data_owned_id or 0, 0 if non existing
def get_finger_data(finger_id):
    cursor = dc.execute("SELECT node_id, data_owned_id FROM finger WHERE id = ?", (finger_id, ))
    rows = cursor.fetchall()
    if len(rows) > 0:
        return rows[0]
    return None
    

# Add one finger (e.g. another node) that points on one of our data
# Also adds the correct address to this data
#
def insert_finger(node_id, data_owned_id, data_shared_id):
    # print("insert_finger:", node_id, data_owned_id)
    dc.insert("INSERT INTO finger (node_id, data_owned_id, data_shared_id) VALUES (?, ?, ?)", (node_id, data_owned_id, data_shared_id))


# returns finger_id or zero
def get_finger_id(node_id, data_owned_id):
    cursor = dc.execute("SELECT id FROM finger WHERE node_id = ? AND data_owned_id = ?", (node_id, data_owned_id))
    rows = cursor.fetchall()
    if len(rows) > 0:
        return rows[0]
    return None


def update_finger_quality(node_id, finger_id, success):
    connection = dc.get_connection()
    cursor = connection.cursor()
    if success:
        cursor.execute("UPDATE finger SET connection_tries=connection_tries+1, last_connection_unixtime=DateTime('now')  WHERE id = ? ", (finger_id,))  # comme to make the () a tuple...
    else:
        cursor.execute("UPDATE finger SET connection_tries=connection_tries+1, connection_fails=connection_fails+1  WHERE id = ? ", (finger_id,))  # comme to make the () a tuple...
    connection.commit()


# Use when a node doesn't serve the correct data
def update_finger_data_integrity_quality(finger_id, success):
    connection = dc.get_connection()
    cursor = connection.cursor()
    if success:
        cursor.execute("UPDATE finger SET data_integrity_tries=data_integrity_tries+1, last_connection_unixtime=DateTime('now')  WHERE id = ? ", (finger_id,))  # comme to make the () a tuple...
    else:
        cursor.execute("UPDATE finger SET data_integrity_tries=data_integrity_tries+1, data_integrity_fails=data_integrity_fails+1  WHERE id = ? ", (finger_id,))  # comme to make the () a tuple...
    connection.commit()


# returns finger_id, or 0 if no finger found
def get_finger(data_owned_id, other_node_id):
    r = dc.query_one("SELECT finger.id FROM finger JOIN node ON node.id = finger.node_id WHERE finger.data_owned_id = ? AND node.id = ? LIMIT 1", (data_owned_id, other_node_id))
    if r:
        return r
    return 0


# Takes any finger, and returns its ID, or None if there are no fingers in the database
# if no_dummies==True then return only fingers not pointing on a data_owned dummy
def get_random_finger_id(dummies):
    if dummies:
        return dc.query_one('SELECT finger.id FROM finger join data_owned on data_owned.id = finger.data_owned_id where data_owned.pubkey = "dummy" ORDER BY RANDOM() LIMIT 1')
    else:
        return dc.query_one('SELECT finger.id FROM finger join data_owned on data_owned.id = finger.data_owned_id where data_owned.pubkey != "dummy" ORDER BY RANDOM() LIMIT 1')


def get_fingers_node_id(finger_id):
    return dc.query_one("SELECT node_id FROM finger WHERE id = ? LIMIT 1", (finger_id, ))


def get_shared_data_if_from_finger(finger_id):
    return dc.query_one("SELECT data_shared_id FROM finger WHERE id = ? LIMIT 1", (finger_id, ))


# When user updates data, other nodes will have stale data, get a finger pointing to a node in this situation
def get_stale_data_fingers():
    # Get all data we own (LIMIT a bit)
    stale_fingers = []
    # Get a bunch of data we share, to control:
    cursor = dc.execute("SELECT id, data_version FROM data_owned ORDER BY RANDOM() LIMIT 10")
    rows = cursor.fetchall()
    for row in rows:
        # check if the data is stale:
        data_owned_id = row[0]
        data_version = row[1]
        cursor2 = dc.execute("SELECT id, node_id, data_owned_id, data_version FROM finger WHERE data_owned_id = ? AND data_version < ?", (data_owned_id, data_version))

        all_stale_fingers = cursor2.fetchall()
        for sf in all_stale_fingers:
            # Add the Actual, new data version:
            tmp = {0: sf[0], 1: sf[1], 2: sf[2], 3: sf[3], 4: data_version}
            stale_fingers.append(tmp)

    return stale_fingers


# Deletes all the fingers related to this data that we own. Returns true if the data exist, false otherwise.
def delete_fingers(owned_data_name):
    data_owned_id = get_link_data_id_by_name(owned_data_name)
    if data_owned_id:
        print('Delete fingers pointing on owned data id', data_owned_id)
        all_fingers = get_all_fingers(data_owned_id)
        print('number of fingers:', len(all_fingers))
        for finger_id in all_fingers:
            print('  > Delete finger pointing on owned data id', finger_id)
            # Get the data we are reciprocally sharing for this finger and delete it:
            shared_data_id = get_shared_data_if_from_finger(finger_id)
            if shared_data_id:
                delete_shared_data(shared_data_id)

        # Remove all fingers
        dc.update("DELETE FROM finger WHERE data_owned_id = ?", (data_owned_id,))

        return True
    return False


# Delete this specific finger, and the data that we share for it
def delete_finger_and_its_shared_data(finger_id):
    # Delete the folder that contains the data we share for them
    shared_data_id = get_shared_data_if_from_finger(finger_id)
    if shared_data_id:
        delete_shared_data(shared_data_id)

    dc.update("DELETE FROM finger WHERE id = ?", (finger_id,))

#


############################################
#     Data

def delete_shared_data(shared_data_id):
    # get_shared_data_name
    folder_number = get_shared_data_folder_number(shared_data_id)
    if folder_number:
        folder_path = os.path.join(locator.create_path(get_configuration_value("data_shared_folder")), str(folder_number))
        print('Delete shared folder:', folder_path)
        shutil.rmtree(folder_path)
        #delete from db now!
        dc.update("DELETE FROM data_shared WHERE id = ?", (shared_data_id,))


# returns a data_owned_id that doesn't yet have it's full number of fingers, or zero
# Do not use a data that is already stored by rsa_pub_key
# Checks if it is validated by our size policy, disk isn't full etc
def get_valid_data_id_needing_finger_for_swicharoo(other_file_size, other_rsa_pub_key, other_name):  # todo: use filesize in some way (+/- half magnitude, config, ...)
    other_node_id = get_node_id(other_rsa_pub_key)

    for i in range(1, 50):  # todo: make this a SQL oneliner (join count fingers on data)
        rows = dc.query_all('SELECT id, name FROM data_owned ORDER BY RANDOM() LIMIT 1;')
        if len(rows) != 1:  # No files at all to share
            return 0
        row = rows[0]
        data_owned_id = row[0]
        my_data_name = row[1]
        # Check other node isn't already storing it:
        if data_owned_id is not None:
            # Check the other node doesn't already store this data:
            if get_finger(data_owned_id, other_node_id) == 0:
                has_fingers = get_number_of_fingers(data_owned_id)
                needs_fingers = get_wanted_fingers(data_owned_id)
                if needs_fingers > has_fingers:

                    # Do we accept this file?
                    my_file_size = os.path.getsize(fp.generate_aes_filepath(my_data_name))

                    if do_accept_swicharoo(my_file_size, other_file_size):
                        return data_owned_id

    return 0


#


def get_afar_flag_from_name(data_name):
    data_owned_id = get_link_data_id_by_name(data_name)
    if data_owned_id:
        return get_afar_flag_from_id(data_owned_id)
    else:
        print("#Error: could not get afar flag for data name [" + data_name + "]", flush=True)
        return 0


def get_afar_flag_from_id(data_owned_id):  # Hide my IP:PORT address in links & _tr/_sub files
    v = dc.query_one('SELECT hide_my_address FROM data_owned WHERE id = ?', (data_owned_id,))
    if v:
        return v != 0
    return False


def _set_afar_flag(data_name, value):  # Just sets this on (the global one sets name, name+_tr,_sub)
    data_owned_id = get_link_data_id_by_name(data_name)
    if data_owned_id:
        old_value = get_afar_flag_from_id(data_owned_id)
        if old_value != value:
            if value:
                insert = 1
            else:
                insert = 0
            dc.update("UPDATE data_owned SET hide_my_address = ? WHERE id = ? LIMIT 1", (insert, data_owned_id))

            # Flag it for update
            set_next_data_version(data_owned_id)
    else:
        print("#Error: could not set afar flag for data name [" + data_name + "]", flush=True)


def set_afar_flag(data_name, value):  # Hide my IP:PORT address in links & _tr/_sub files
    _set_afar_flag(data_name, value)
    _set_afar_flag(data_name + '_tr', value)
    _set_afar_flag(data_name + '_sub', value)
    # Watch out (1) The named entry should have address_translation file set to 0
    # Watch out (2) _tr and _sub files must be updated accordingly
    # Watch out (3) you need to update the data_version of all the changed data_owned entries
    # OR make only the BASE (non _tr/_sub) have the tag...

# -------------------------- Addresses -----------------------------------

############################################
#     Addresses for shared data


# Add a new address for this data_owned_id
def add_address(data_owned_id, ip, port, rsa_pub_key, digest, encrypted_address):
    # Check it is not on the avoid list and not our IP:PORT
    if not ip_on_exclude_list(ip) and not is_my_ip_port(ip, port):
        dc.insert('INSERT INTO address (pubkey, ip, port, data_id, digest, encrypted_address) VALUES(?, ?, ?, ?, ?, ?)', (rsa_pub_key, ip, port, data_owned_id, digest, encrypted_address))


# Returns all the addresses pointing on this data
def get_encrypted_addresses(data_id):
    address_list = []
    cursor = dc.execute("SELECT encrypted_address FROM address WHERE data_id = ? ", (data_id, ))
    rows = cursor.fetchall()
    for row in rows:
        # print('address:', row[0])
        address_list.append(row[0])

    return address_list


# returns a string ip:port:rsapub,ip:port:rsapub,ip:port:rsapub ...
def get_address_list(data_owned_id):
    # Create the address list:
    ip = get_configuration_value('ip')
    port = get_configuration_value('port')

    prv, pub = get_rsa_hex_keys()

    address_list = ip + ':' + str(port) + ':' + pub

    fingers = get_all_fingers(data_owned_id)
    for finger_id in fingers:
        node_id, _ = get_finger_data(finger_id)  # get the node of this finger (one of the fingers pointing on our data)
        tmp = get_node_data(node_id)
        if tmp:
            ip = tmp[0]
            port = tmp[1]
            rsa_pub_key = tmp[2]

            address = ',' + ip + ':' + port + ':' + rsa_pub_key
            address_list = address_list + address
    return address_list


# returns a dict todo: refact get_address_list() to use this function
# Note: PORT will be in string format
def get_address_list_as_dict(data_owned_id, afar=False):  # afar = we don't put our IP:PORT in there
    # Create the address list:
    ip = get_configuration_value('ip')
    port = str(get_configuration_value('port'))
    prv, pub = get_rsa_hex_keys()

    if afar:
        d = {}
    else:
        d = {pub: {'ip': ip, 'port': port}}

    fingers = get_all_fingers(data_owned_id)
    for finger_id in fingers:
        node_id, _ = get_finger_data(finger_id)  # get the node of this finger (one of the fingers pointing on our data)
        tmp = get_node_data(node_id)
        if tmp:
            ip = tmp[0]
            port = tmp[1]
            rsa_pub_key = tmp[2]

            # Note: we change port to a string as it we use it to compare with
            d[rsa_pub_key] = {'ip': ip, 'port': port}

    return d


#

# -------------------------- Owners Keys -----------------------------------

############################################
#     Owners RSA keys

# Returns two str
def get_rsa_hex_keys():  # Returns RSA keypair in hex format, or None if they do not exists / cannot be accessed.
    connection = dc.get_connection()
    cursor = connection.cursor()
    cursor.execute("SELECT privkey, pubkey FROM rsa ORDER BY id DESC")  # If there are several, take the last one

    rows = cursor.fetchall()
    if len(rows) > 1:
        print('Warning! You have multiple RSA keypairs in your datbase! Returning newest one...')
    if len(rows) > 0:
        row = rows[0]
        return row[0], row[1]
    return None, None


def set_rsa_hex_keys(textkey_prv, textkey_pub):
    connection = dc.get_connection()
    cursor = connection.cursor()
    print('Pubkey=', textkey_pub)

    cursor.execute("INSERT INTO rsa (privkey, pubkey ) VALUES(?, ?);", (textkey_prv, textkey_pub))
    connection.commit()


# -------------------------- Flush information to console -----------------------------------

#


# prints information about our own owned data
def flush_owned_data(no_translation_files=False):
    if no_translation_files:
        rows = dc.query_all("SELECT id, name, data_version, hide_my_address FROM data_owned WHERE address_translation_file = 0")
    else:
        rows = dc.query_all("SELECT id, name, data_version, hide_my_address FROM data_owned")

    if len(rows) == 0:
        print('You do not share any data yet.')
        return

    output_lines = []
    line = {0: 'fingers:', 1: 'version:', 2: 'size:', 3: 'afar:', 4: 'name:', 5: 'file:'}
    output_lines.append(line)

    for data in rows:  # maybe this is not necessary... but as it should work, lets check that later
        data_owned_id = data[0]
        data_version = data[2]
        afar_flag = data[3]
        wanted_fingers = get_wanted_fingers(data_owned_id)
        actual_fingers = get_number_of_fingers(data_owned_id)
        file_size = get_owned_file_size(data_owned_id)

        line = {}
        line[0] = str(actual_fingers)+'/'+str(wanted_fingers)
        line[1] = str(data_version)
        line[2] = human_readable_file_size(file_size, True)
        if afar_flag:
            line[3] = 'Yes'
        else:
            line[3] = 'No'
        line[4] = '['+str(data[1])+']'

        output_lines.append(line)

    # Calculate the max sizes
    max_size = [0, 0, 0, 0, 0, 0]
    for line in output_lines:
        for i in range(0, 5):
            s = len(line[i])
            max_size[i] = max(max_size[i], s)

    for line in output_lines:
        out_line = ''
        for i in range(0, 5):
            s = len(line[i])
            out_line += line[i]
            # Add missing spaces, +1
            missing_spaces = max_size[i] - s + 1
            out_line += ' '*missing_spaces
        print(out_line)


# prints information about shared data
def get_nof_shared_data():
    rows = dc.query_all("SELECT id FROM data_shared")
    return len(rows)


# Shows the success ratio for all nodes with success>0
# Also shows the number of nodes at zero success
def flush_known_nodes():
    bad_or_new_nodes = dc.query_one("SELECT COUNT (*) FROM node WHERE connection_tries = connection_fails AND connection_tries > 0")
    never_connected_to_nodes = dc.query_one("SELECT COUNT (*) FROM node WHERE connection_tries = 0")
    rows = dc.query_all("SELECT ip, port, connection_tries, connection_fails FROM node WHERE connection_tries > connection_fails AND connection_tries > 0")

    print('Good nodes: ' + str(len(rows)) + '  (If any, they will be listed below)')
    print('New or bad nodes: ' + str(bad_or_new_nodes) + '  (We have tried but never achieved a connection to these nodes)')
    print('Never connected to nodes: ' + str(never_connected_to_nodes) + '  (Debug nodes, not yet checked out nodes...)')

    print('')
    print('Good nodes:')
    for data in rows:
        ip = data[0]
        port = data[1]
        tries = data[2]
        fails = data[3]
        success = tries - fails
        success_rate = int((10000 * success)/tries) / 100
        print('Node: address=' + ip + ':' + port + ' tries/fails: '+str(tries)+'/'+str(fails)+ ' ['+str(success_rate)+'% success]' )
    # count of bad nodes/too new nodes to have a ratio:

#

#


# Shows information of one of our owned data
def flush_fingers(name):
    data_owned_id = get_link_data_id_by_name(name)
    if data_owned_id:
        rows = dc.query_all("SELECT id, node_id, data_version, connection_tries, connection_fails, last_connection_unixtime FROM finger WHERE data_owned_id = ?", (data_owned_id,))
        r = []
        print('name id  node_id  version   tries/fails [%]   <last successful finger connection>')
        for data in rows:  # maybe this is not necessary... but as it should work, lets check that later
            percent = '---'
            tries = data[3]
            fails = data[4]
            success = tries - fails
            if tries > 0:
                percent = str(int(100 * success / tries))+'%'
            print(name, ':',  data[0], data[1], data[2], str(data[3]) + '/' + str(data[4]), percent, str(data[5]))

    else:
        print('Unknown data:', name)

