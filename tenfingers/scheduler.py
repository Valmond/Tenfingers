# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import os
import sys
import time
import json

from struct import pack, unpack
try:
    from Cryptodome.PublicKey import RSA
except ImportError:
    print("Error 2: You need to install Cryptodome:\npip3 install pycryptodomex\n")
    exit(-7)

import random
import traceback

from socket_communication import SecureCommunication
import database_query as dq
import rsa_helper as rh
import database_connection as dc
from configuration import get_configuration_value
import address_registry as ar
import logging
from helpers import network_byte_to_int
import filepath as fp


# todo: randomly call requre_update_data_information for fingers


"""
The scheduler makes certain commands at certain intervals like:

* Trying to find new nodes
* Check if nodes are up (and update the DB accordingly)
* Remove stale nodes
* Check that all our data have 10 fingers, or try to get a new finger
* Verify fingers pointing on our data (and update the DB accordingly)
* Remove stale fingers

"""

"""
For convenience, we'll make the different functions in this file
Later on when there are several, we should move them to separate source files.

"""

_log_event_calls = False  # prints to stdout all the calls the scheduler does

###################################
#       DB helpers
#


# Updates the nodes quality
def update_node_quality(node_id, success):
    # Updates db::node: CONNECTION_TRIES, CONNECTION_FAILS, LAST_CONNECTION_UNIXTIME
    # get a random node:
    node_id = dq.get_random_node_id(only_validated_nodes=False)
    if node_id is not None:
        info = dq.get_node_data(node_id)
        sc = SecureCommunication(info[0], info[1], info[2]) #  IT IS NOT str(...) but ENCODE/DECODE!!!!!
        dq.update_node_quality(node_id, sc.is_active())
        sc.close()


###################################
#       NODES
#


def get_node_connection(node_id):
    tmp = dq.get_node_data(node_id)
    ip = str(tmp[0])
    port = tmp[1]
    rsa_pub_key = str(tmp[2])
    # print('Try to connect to random node:', ip, port) #, rsa_pub_key)
    # print('using rsa type, len', type(rsa_pub_key), len(rsa_pub_key))
    # print('using rsa', rsa_pub_key[-10:])
    sc = SecureCommunication(ip, port, RSA.importKey(bytes.fromhex(rsa_pub_key)))  # rsa_pub here is the one linked to the other Node
    return sc  # You check it's active!


# Returns a secure, functionnal node, or None
def get_random_node_connection(only_validated_nodes=True):
    node_id = dq.get_random_node_id(only_validated_nodes)  # todo: remove the False in the call
    if node_id is None:
        return None, 0
    # sc = SecureCommunication(ip, port, RSA.importKey(bytes.fromhex(rsa_pub_key)))  # rsa_pub here is the one linked to the other Node
    sc = get_node_connection(node_id)
    if sc.is_active():
        return sc, node_id
    return None, node_id


# One of the scheduled events:
def find_new_nodes():
    # Get a random nod in our db:node, connect to it and ask for some nodes
    sc, node_id = get_random_node_connection(False)  # todo: remove the False in the call

    if sc is None:
        # print('Random node is not up (Find new Nodes).')
        return
    # print('Ask for nodes from node', node_id)
    # Query for some random node(s)
    ip = get_configuration_value('ip')
    port = get_configuration_value('port')
    _, rsa_pub = rh.get_rsa_keypair(True)

    # Send which port we prefer
    port = str(port).encode()
    # and our public key
    rsa_pub = rsa_pub.exportKey('DER').hex().encode()

    # print('Send SEND_NODES req.', flush=True)
    json_answer = sc.send(b'SEND_NODES' + str(ip).encode() + b',' + port + b',' + rsa_pub)  # ,' + aa + b',' + bb + b',' + rr)  # expected return: json with a node: ip, port, rsa_pub_key, or an empty json if no nodes: {}
    # print('Send SEND_NODES req. Got Answer:', json_answer[:48], flush=True)
    if json_answer:
        try:
            # print('Got an answer for our SEND NODES, lets try to json decode it:')
            j = json.loads(json_answer.decode())  # decode to string and then put JSON-data to a variable
            # insert it in the database
            if 'ip' in j and 'port' in j and 'rsa_pub_key' in j:
                # convert ip & rsa_pub_key from bytes to str, port is an integer so no need.
                # print('Got node:', str(j['ip']), ':', j['port'])
                dq.insert_node(str(j['ip']), j['port'], str(j['rsa_pub_key']), 'From Scheduler SEND_NODES')
            # else here is when either the json is correct but misses information, or when it's empty because there are no nodes
            #else:
            #    print('#No information in json, bailing out:', json_answer.decode())
        except:
            print('JSON Exception!')
            print('tried to decode:', json_answer.decode())
            return
    else:
        print('Got empty answer. Bail out.')


# don't forget to use update_node_quality()


# One of the scheduled events, checks one node and
# updates its quality based on if we cqn connect to it or not
def check_node_quality():
    # update connection_tries & connection_fails
    # update last_connection_datetime if the connection worked
    sc, node_id = get_random_node_connection(False)  # todo: remove the False in the call
    node_up = False
    if sc is not None:
        node_up = sc.is_active()

    # print('### UPDATE NODE QUALITY: ', node_id, node_up)
    dq.update_node_quality(node_id, node_up)

    if node_up:
        sc.close()


# One of the scheduled events:
def remove_stale_nodes():
    # When we have a large number of nodes, cull the ones no longer working
    # by removing the oldest db:nodes::last_connection_datetime
    pass


def verify_our_address_in_nodes():
    # Verify all nodes know our current address
    our_ip = get_configuration_value('ip')
    our_port = get_configuration_value('port')

    # Find nodes who think our address is an old one
    node_id = dc.query_one("SELECT id FROM node WHERE our_ip != ? OR our_port != ? ORDER BY RANDOM() LIMIT 1", (our_ip, our_port))
    if node_id is not None:
        # Send UPDATEADDRESS requeest to node:
        sc = get_node_connection(node_id)
        if sc.is_active():

            their_our_ip = dc.query_one("SELECT our_ip FROM node WHERE id = ? ", (node_id, ))
            their_our_port = dc.query_one("SELECT our_port FROM node WHERE id = ? ", (node_id, ))

            print("STALE_ADDRESS: Found a node who has an old version of our address: id:", node_id, flush=True)
            print("STALE_ADDRESS: their address keps:", their_our_ip, their_our_port, flush=True)
            prv, pub = dq.get_rsa_hex_keys()
            _, rsa_pub_hex = dq.get_rsa_hex_keys()
            # Send our address update:
            print("STALE_ADDRESS: send our address:", our_ip, our_port, flush=True)
            digest_query = sc.send(b'NEWADDRESS' + str(our_ip).encode() + b',' + str(our_port).encode() + b',' + rsa_pub_hex.encode())

            if digest_query is not None:
                if sc.is_active():
                    digest = rh.create_digest(digest_query, RSA.importKey(bytes.fromhex(prv)))
                    # print('Okay all systems go!', flush=True)
                    #   <<        Send Recv           >>
                    answer = sc.send(digest)

                    if answer == b'OK':
                        # dc.update("UPDATE NODE SET our_ip = ?, out_port = ? WHERE id = ? LIMIT 1", (our_ip, our_port, node_id))
                        print("STALE_ADDRESS: update our db:  node_id, our address", node_id, our_ip, our_port, flush=True)

                        dq.update_our_address_for_node(node_id, our_ip, our_port)
                        print("Our new address was successfully sent to node " + str(node_id) + " and then updated locally.")
                    else:
                        print("Our new address sent to node " + str(node_id) + " was REJECTED because of wrong digest")
                else:
                    print("Our new address sent to node " + str(node_id) + " was REJECTED because node closed connection unexpectedly")
                    if digest_query is not None:
                        print("however they sent back this:", digest_query)
            else:
                print("Our new address sent to node " + str(node_id) + " was REJECTED because node did not even answer our request call")

#

###################################
#       Our Data
#


# Call this when user has updated its data (so it is a newer version)
# We will ask the finger(s node) to accept a newer version of the data shared
def _update_finger_data(finger_id, node_id, data_owned_id, new_data_version, file_size):
    # print('Try to update finger id:', finger_id, node_id, data_owned_id, new_data_version, file_size)
    tmp = dq.get_node_data(node_id)
    if tmp:
        ip = str(tmp[0])
        port = tmp[1]
        other_rsa_pub_key = str(tmp[2])
        sc = SecureCommunication(ip, port, RSA.importKey(bytes.fromhex(other_rsa_pub_key)))
        if not sc.is_active():
            # node is not up
            #print('Could not connect for update to ', ip, ":", port, ' so bail out.')
            return False

        # get data information & data
        name = dq.get_name_of_owned_data_id(data_owned_id)
        if not name:
            print('SCHEDULER ERROR: get_link_data_by_data_id did not find finger id  ' + str(data_owned_id))
            return False

        # get our PubKey:
        prv, pub = rh.get_rsa_keypair()
        _, rsa_pub_hex = dq.get_rsa_hex_keys()
        # rsa_pub_hex = pub.exportKey('DER').hex().encode()

        nonce = dq.get_nonce_from_data_id(data_owned_id)

        file_rsa_pub = dq.get_rsa_key_by_data_id(data_owned_id)
        file_rsa_prv = dq.get_rsa_key_by_data_id(data_owned_id, private_key=True)

        # Watch out, nonce can be 0 which is valid
        if nonce is None or not file_rsa_pub or not file_rsa_prv:
            print('SCHEDULER ERROR: get_link_data_by_data_id did not find data for finger id ' + str(data_owned_id))
            return False

        # ask for an update
        query = b'UPDATEDATA' + pack('>Q', file_size) + b',' + name.encode() + b',' +\
                file_rsa_pub.encode() + b',' + str(new_data_version).encode() + b',' + str(nonce).encode()
        #logging.info('SCHEDULER query:')
        #logging.info(query)

        random_checksum = sc.send(query)
        if not random_checksum:
            print('Got no answer')
            sc.close()
            return False

        # make a digest and send back for authentification, prove we actually own the data to be updated:
        digest = rh.create_digest(random_checksum, RSA.importKey(bytes.fromhex(file_rsa_prv)))
        # print('Calculating digest:', digest)
        # send back digest
        answer = sc.send(digest)
        if not answer:
            print('No answer for digest request, bail out.')
            return False
        if answer[:10] != b'ACCEPTING!':

            if answer[:10] == b'NO_NEED_TO':  # They are up to date
                print("They already store the latest version, update our db.")
                dc.update("UPDATE finger SET data_version = ? WHERE id = ?", (new_data_version, finger_id))
                return True
            else:
                print('Refresh data query was turned down for our [' + name + ']:', answer[:10], ' node_id=', node_id, ' finger_id=', finger_id)
                # As they visibly no longer share our data lets remove the finger:
                logging.info('SCHEDULER got answer "' + str(answer) + '" for finger_id=' + str(finger_id))  # Todo show only 10 characters
                logging.info('SCHEDULER _update_finger_data: DELETE FINGER finger_id=' + str(finger_id))
                dq.delete_finger_and_its_shared_data(finger_id)
                sc.close()
                return False
        # Refresh accepted, lets go through with it:

        # ip = get_configuration_value('ip')
        # get our PubKey:
        if not prv:
            print('No prv.')
            return False

        with open(fp.generate_aes_relative_path(name), 'rb') as f:
            file_data = f.read()
        if file_data is None:
            print('Refresh: File not found', fp.generate_aes_relative_path(name))
            sc.close()
            return False

        # other_ip, other_port, other_pub_key = dq.get_node_data(node_id)
        answer = sc.send(file_data)
        if answer != b'OK':
            #print('Refresh sending data failed')
            return False

        sc.close()
        #print('Refresh successful, do this sql:')
        # print("UPDATE finger SET data_version = ? WHERE id = ?", (new_data_version, finger_id))
        dc.update("UPDATE finger SET data_version = ? WHERE id = ?", (new_data_version, finger_id))
        #print('SQL done.')
        return True
    else:
        print('#Error: _update_finger_data was called but we could not get information through get_node_data(' + str(node_id) + ')')
        return False


# Try to get a new swicharoo for one of our data. Private function used by check_data_fingers()
# Returns true on success, otherwise False
def _get_new_finger(data_owned_id):
    if not data_owned_id:
        print('#ERROR _get_new_finger(data_owned_id) called with data_owned_id=', data_owned_id)
        return False
    # As socket communication can fail in so many ways and we even know we'll be having stale addresses in our database, make a big try-catch here:
    sc = None
    try:
        # Connect to a random node
        # todo: only select nodes that do not store our data already!
        sc, node_id = get_random_node_connection(False)  # todo: remove the False in the call (use only verified nodes)
        if sc is None:
            return False

        # get our PubKey, ip, port
        prv, pub = dq.get_rsa_hex_keys()
        if not prv:
            sc.close()
            return False
        ip = get_configuration_value('ip')
        port = get_configuration_value('port')

        # get data information
        name = dq.get_name_of_owned_data_id(data_owned_id)
        my_version = dq.get_owned_data_version(data_owned_id)

        # print('Check we do not already have the finger (node_id, data_owned_id)', node_id, data_owned_id)
        # Verify we are not already have a finger with this node:
        # todo: don't ask nodes that already store our data, this seems it isn't enough, maybe as sometimes we accidentally maskerade with different ip:ports
        # Actually it seems like we check IP:PORT and not the actual RSA Pubkey?
        # Or just we contact the same node, but over another IP (like a 192. ... instead of public IP) we think they are different
        # but they are actually the same => they already store our data
        finger_id = dq.get_finger_id(node_id, data_owned_id)
        # print('Scheduler: We got a finger_id:', finger_id)
        if finger_id is not None:  # This node already stores the data
            # print('Scheduler: We already have data stored at this node')
            sc.close()
            return False

        # print('Scheduler: Ask for new finger for data_owned_id='+str(data_owned_id)+' node_id='+str(node_id)+ ' data name='+name+' (origname='+my_original_file_name+')')
        # get the actual datasize from the data_owned_id
        file_size = dq.get_owned_file_size(data_owned_id)

        my_nonce = dq.get_nonce_from_data_id(data_owned_id)

        file_specific_rsa_pub = dq.get_rsa_key_by_data_id(data_owned_id)

        # ask for a swicharoo
        query = b'SWICHAROO?' + pack('>Q', file_size) + pub.encode() + b'|' + name.encode() + b'|' + ip.encode() + b'|' +\
                str(port).encode() + b'|' + str(my_version).encode() + b'|' + str(my_nonce).encode() +\
                b'|' + str(file_specific_rsa_pub).encode()
        #   <<        Send Recv           >>
        answer = sc.send(query)
        if answer[:10] != b'YES_PLEASE':
            if answer[:10] == b'ALREADY_DO':
                # They already store our data, duh
                print('Scheduler: SWICHAROO query was turned down because they already store our data, node_id, data_owned_id:', node_id, data_owned_id)
                pass
            else:
                if answer[:10] == b'SRYNODATA.':
                    print('Scheduler: SWICHAROO turned down because they have nothing to share.')
                else:
                    print('Scheduler: SWICHAROO query was turned down, reason:', answer)
            sc.close()  # Close as this node does not behave well right now
            return False

        # Get size other node want us to store and check if we are OK:
        data = answer[10:]  # Remove the b'YES_PLEASE'
        data_size = data[:8]
        data = data[8:]  # Remove the size
        if len(data_size) >= 8:
            other_file_size = unpack('>Q',  data_size)[0]
        else:
            sc.close()  # Close as the node
            print('Scheduler: unpack needs ay teals 8 bytes, got less')
            return False

        # print('other_file_size:', other_file_size, flush=True)

        if other_file_size > get_configuration_value('swicharoo_file_maxsize_kb')*1024:
            sc.send(b'U_TOO_BIG!')
            sc.close()  # Close as the node has no more space anyways
            print('Scheduler: their file is too big (max=' + str(get_configuration_value('swicharoo_file_maxsize_kb')*1024) + '): their file size=', other_file_size)
            return False

        split = data.split(b'|')
        if len(split) != 3:
            sc.send(b'ERROR_LEN!')
            print('Scheduler: SWICHAROO error split should be 4, is:', len(split))
            sc.close()  # Close as the node has no more space anyway
            return False

        # Get all information from the other node
        other_name = split[0].decode()
        other_file_rsa_pub = split[1].decode()
        digest_query = split[2]  # probably do not decode() this but use as is

        if digest_query is None or not sc.is_active():
            print('Other node refused our swicharoo query, bailing out too.', flush=True)
            sc.close()
            return False

        # If we should do it this way, we should also discern nodes by rsa,ip,port? or have several ip:port for 1 node...
        other_ip, other_port, other_pub_key = dq.get_node_data(node_id)  # Why do we both accept these data from the listener, but also get them from our database?

        # Check if we already store the data:
        if dq.get_link_data_by_name_and_rsapub(other_name, other_file_rsa_pub):  # We already store this data for this node!
            print('Scheduler: our swicharoo failed as we already store the data the other node proposes to switch.')
            sc.send(b'DATA_AGAIN')
            sc.close()
            return False

        # print('Do digest, digest_query=', digest_query, flush=True)
        # print('Do digest type=', type(digest_query), flush=True)

        digest = rh.create_digest(digest_query, RSA.importKey(bytes.fromhex(prv)))
        #   <<        Send Recv           >>
        other_data = sc.send(digest)

        # Save their data
        #others_folder = dq.get_folder(other_pub_key)
        # I now have other: name, data and the specific file rda pub key, save off data and update DB
        data_shared_id = dq.insert_data_shared(other_name, other_file_rsa_pub)  # Others data is now stored here for retrieval (by name + pubkey), deprecated other_original_file_name
        others_folder = dq.get_shared_data_folder_by_name_and_rsa_pub(other_name, other_file_rsa_pub)
        file_folder = os.path.join(get_configuration_value("data_shared_folder"), others_folder) + '/'
        if not os.path.exists(file_folder):
            os.makedirs(file_folder)

        save_name = file_folder + other_name + '.aes'
        # print('Scheduler: Swicharoo, save off others file:', save_name, flush=True)
        with open(save_name, 'wb') as f:
            f.write(other_data)


        # print('Got their data, send ours:', flush=True)

        # send our data, expect b'ok' back
        # get the data
        with open(fp.generate_aes_relative_path(name), 'rb') as f:
            file_data = f.read()
        if file_data is None:
            print('Scheduler: Our original file not found:', fp.generate_aes_relative_path(name), flush=True)
            sc.close()
            return False

        # Send our data
        #   <<        Send Recv           >>
        ack = sc.send(file_data)
        # print('Got final ACK:', ack, flush=True)

        # Insert new finger for my data
        dq.insert_finger(node_id, data_owned_id, data_shared_id)

        # As an address have now changed (the data has one more address),
        # updates tr & sub files according to what kind of data this is (base_data, TR, SUB)
        ar.refresh_data_addresses(data_owned_id)

        sc.close()

        print('Scheduler: Swicharoo successful, changed our ' + name + ' for theirs: ' + other_name, flush=True)
        return True

    except:
        if sc is not None:
            sc.close()
    return False


# One of the scheduled events:
def check_data_fingers():  # Tries to get a new finger for data that does not have enough fingers
    # Get data that is not yet shared at all
    data_owned_id = dq.get_random_our_not_shared_data()
    # print('SCHEDULER: Found data with few or no fingers, data_owned_id:', data_owned_id)
    if data_owned_id is not None:
        # Try to get 2 new fingers for a random data (if needed)
        check_data_finger(data_owned_id, 2)

    # Just get a random node
    data_owned_id = dq.get_random_local_data_id()
    if data_owned_id is not None:
        # Try to get 2 new fingers for a random data (if needed)
        check_data_finger(data_owned_id, 2)


# Tries 'tries' times to get a new finger for this data, if needed
def check_data_finger(data_owned_id, tries=10, print_output=True):
    newly_acquired_fingers = 0
    fingers = dq.get_number_of_fingers(data_owned_id)
    wanted_fingers = dq.get_wanted_fingers(data_owned_id)
    if wanted_fingers <= 0:
        return
    while fingers < wanted_fingers and tries > 0:
        if _get_new_finger(data_owned_id):
            fingers += 1
            newly_acquired_fingers += 1
            if print_output:
                print('New finger acquired (' + str(fingers) + '/' + str(wanted_fingers) + ')', flush=True)
        tries = tries - 1


# Get a finger and check if it carries our data
# If the finger is up
# Check version of data and update if needed
# If it does no longer carry our data, remove the finger
def _check_finger_carries_data(sc, name, data_rsa_pub, shared_data_id, finger_id):
    try:
        data_version = dq.get_owned_data_version(shared_data_id)
        filepath = fp.generate_aes_relative_path(name)
        with open(filepath, 'rb') as f:
            f.seek(0, os.SEEK_END)
            file_size = f.tell()

            if file_size > 0:
                # print("file size", file_size, flush=True)

                s_verification_chunk = 256

                chunk_size = min(s_verification_chunk, file_size)
                # Get a random starting point
                start_byte = 0
                a = file_size - chunk_size
                if a > 0:
                    start_byte = random.randint(0, a)
                # print("start_byte, file size, chunk_size", start_byte, file_size, chunk_size, flush=True)

                read_bytes = chunk_size
                #logging.info('SCHEDULER verify finger by sending ONEFINGER')
                query = 'ONEFINGER:' + name + ',' + data_rsa_pub + ',' + str(start_byte) + ',' + str(read_bytes)  # rsa in plaintext here as we use it as a name

                q = sc.send(query.encode())
                their_version = None
                their_file_size = 0
                nonce = None

                success = False
                reason = 'unknown'

                if q == b'NotStoring':  # They don't have it
                    reason = 'The node has lost the data, lets delete the finger'
                    logging.info('SCHEDULER Node has lost our data, delete finger ' + str(finger_id) + ' ')
                    # Then delete the finger and the data we share for it
                    dq.delete_finger_and_its_shared_data(finger_id)
                else:

                    if q:
                        r = q.split(b',')
                        if len(r) == 3:
                            their_version = network_byte_to_int(r[0])
                            their_file_size = network_byte_to_int(r[1])
                            nonce = r[2]

                    # logging.info('SCHEDULER got back version ' + str(their_version))

                    # They are supposed to send back their version
                    if their_version and their_file_size and nonce is not None:  # nonce here is a string
                        if their_version >= data_version:
                            #logging.info('SCHEDULER ok so verify ...')

                            # Get the data
                            encrypted_data = sc.send(b'GO')
                            if encrypted_data is not None:

                                if encrypted_data == b'NotStoring':
                                    reason = 'they do no longer store our data, lets delete the finger'
                                    logging.info('SCHEDULER Node no longer stores our data, delete finger ' + str(finger_id) + ' ')

                                    # Then delete the finger and the data we share for it
                                    dq.delete_finger_and_its_shared_data(finger_id)
                                else:
                                    if len(encrypted_data) == read_bytes:
                                        # print("Read our bytes and verify", flush=True)
                                        # Set back file pointer to start_byte, according to the beginning of the file
                                        f.seek(start_byte, os.SEEK_SET)
                                        our_data_part = f.read(read_bytes)

                                        if encrypted_data == our_data_part:
                                            success = True
                                        else:
                                            reason = 'data sent back missmatch ours: ' + str(
                                                str(our_data_part[:10]) + ' got: ' + str(encrypted_data[:10]))
                                    if len(encrypted_data) != read_bytes:
                                        reason = 'data sent back missmatch size, ours: ' + str(len(our_data_part)) + ' got: ' + str(len(encrypted_data))


                            else:
                                logging.info('SCHEDULER encrypted_data is None')
                                reason = 'they closed the connection too early'
                                pass  # They got a stale version, take a note / update
                                # todo: make a champ in the fingers DB "had_stale_version" and update it here
                        else:
                            print('SCHEDULER Got old version, thank and leave')
                            sc.send(b'THANKS_BYE')
                            reason = 'they have an old version'

                    else:  # Unknown reasons or network errors
                        if q:
                            reason = 'communication error, q=' + str(q)
                        else:
                            reason = 'communication error we did not get q'
                        pass


                if success:
                    pass
                    #logging.info('SCHEDULER verify: SUCCESS')
                    #print('Check if Finger [' + str(finger_id) + '] is sharing our data [' + name + ']: Success.', flush=True)
                else:
                    logging.info('SCHEDULER Finger [' + str(finger_id) + '] sharing our data [' + name + '] check failed, reason: ' + reason)
                    #print('Check if Finger [' + str(finger_id) + '] is sharing our data [' + name + ']: FAIL.', flush=True)

                # Update statistics
                dq.update_finger_data_integrity_quality(finger_id, success)


    except:
        pass
        # print('Verification of data, some error for data:', filepath, flush=True)


# One of the scheduled events:
# Checks if the node containing this finger responds
# Close to check_node_quality except it runs for the lifetime
# of the finger, not of the node
# We should NOT exclude dummy files, we need to know when they become obsolete too.
# That is why we run this function twice, with check_dummy_files = True & False
def check_finger_quality(check_the_dummy_files):  # Take one random finger and check if it's responding, update DB accordingly
    # take a random finger and call up it's node
    finger_id = dq.get_random_finger_id(check_the_dummy_files)
    if finger_id:
        # print('check_finger_quality', finger_id)
        finger_data = dq.get_finger_data(finger_id)
        if finger_data:
            node_id = finger_data[0]
            data_owned_id = finger_data[1]

            tmp = dq.get_node_data(node_id)
            if tmp:
                other_ip = str(tmp[0])
                other_port = tmp[1]
                other_rsa_pub_key = str(tmp[2])
                #logging.info('SCHEDULER check finger quality')
                sc = SecureCommunication(other_ip, other_port, RSA.importKey(bytes.fromhex(other_rsa_pub_key)))  # rsa_pub here is the one linked to the other Node
                #logging.info('SCHEDULER update finger quality, finger active=' + str(sc.is_active()))
                dq.update_finger_quality(node_id, finger_id, sc.is_active())

                if sc.is_active():


                    name = dq.get_name_of_owned_data_id(data_owned_id)
                    if name:
                        #logging.info('SCHEDULER call _check_finger_carries_data')
                        # print("data_owned_id, name, my_origfilename = ", data_owned_id, name, my_origfilename)
                        data_rsa_pub = dq.get_rsa_key_by_data_id(data_owned_id)
                        # This one updates data_integrity_tries / data_integrity_fails
                        _check_finger_carries_data(sc, name, data_rsa_pub, data_owned_id, finger_id)
                #logging.info('SCHEDULER close connection, done.')
                sc.close()
            else:
                print('Warning: node', node_id, ' has no information')
        else:
            print('Warning: finger', finger_id, ' does no longer exist.')


# One of the scheduled events:
def remove_stale_fingers():  # Sometimes we'll have to let go of a finger because it doesn't work well enough
    # For a random data that has 10 fingers
    # remove the oldest db:nodes::last_connection_datetime, if older than some day

    pass


# Here we check if user has updated data we own, and that other nodes share
# If so, lets try to send the new data to the nodes
def refresh_stale_fingers():
    stale_fingers_data = dq.get_stale_data_fingers()
    for finger_data in stale_fingers_data:
        finger_id = finger_data[0]
        node_id = finger_data[1]
        data_owned_id = finger_data[2]
        data_version = finger_data[3]  # The fingers (old) version
        new_data_version = finger_data[4]

        #print('STALE FINGER id=', finger_id, ' node_id:', node_id, ' data_owned_id:', data_owned_id, 'data_version:', data_version, 'new_data_version:', new_data_version)
        # Ask for a refresh
        file_size = dq.get_owned_file_size(data_owned_id)
        _update_finger_data(finger_id, node_id, data_owned_id, new_data_version, file_size)


###################################
#       The actual Scheduling mechanism
#
def run_scheduler(run_continously=True):
    #logging.info('SCHEDULER: run one iteration:')
    first_run = True
    while run_continously or first_run:
        first_run = False

        try:

            # Nodes
            if _log_event_calls:
                print('-------------------------------------------------------- 1')
                print('[Nodes]#Find new nodes:')
            find_new_nodes()

            if _log_event_calls:
                print('-------------------------------------------------------- 2')
                print('[Nodes]#Check if node is up and update node quality')
            check_node_quality()

            if _log_event_calls:
                print('-------------------------------------------------------- 3')
                print('[Nodes]#Prune stale nodes')
            remove_stale_nodes()

            if _log_event_calls:
                print('-------------------------------------------------------- 4')
                print('[Nodes]#Check if nodes know our address')
            verify_our_address_in_nodes()

            # Data
            if _log_event_calls:
                print('******************************************************** 5')
                print('[Data]#Check if data has enough fingers else try to get new ones')
            check_data_fingers()

            if _log_event_calls:
                print('******************************************************** 6')
                print('[Data]#Check if fingers node is up, updated and still carries our data')
            # We split this up so that if we have lots of dummy files but few of our own shared files they get a decent split
            check_finger_quality(False)  # My shared files
            check_finger_quality(True)  # My shared dummy files

            if _log_event_calls:
                print('******************************************************** 7')
                print('[Data]#Prune stale fingers')
            remove_stale_fingers()

            if _log_event_calls:
                print('******************************************************** 8')
                print('[Data]#Refresh stale owned data in nodes sharing older versions')
            refresh_stale_fingers()

            if _log_event_calls:
                print('******************************************************** Done.')
        except Exception as e:
            print('SCHEDULER exception:', traceback.format_exc())
            logging.error(traceback.format_exc())

        # Don't use all CPU
        wait_time = get_configuration_value('scheduler_wait_msec') * 0.001
        time.sleep(wait_time)


class Scheduler:
    def __init__(self):
        print("Starting scheduler:")
        self.continue_to_run = True
        self.wait_time = 1.0

    def forcestop(self):
        print("Shutting down scheduler...")
        self.continue_to_run = False

    def run(self):
        time.sleep(0.2)  # Let the listener start-up and flush its log
        while self.continue_to_run:
            # We're in a thread here (hence the class) so just run once, then see if we should shut down or run again
            run_scheduler(run_continously=False)
            time.sleep(self.wait_time)


def test():
    for i in range(1, 9):
        dq.update_node_quality(1, True)
    for i in range(1, 5):
        dq.update_node_quality(1, False)


# This will only happen if you run the file, not if you import it
if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == '--testrun':
        print('[scheduler.py] Testrun')
        test()
        sys.exit(0)

    if len(sys.argv) > 1 and sys.argv[1] == '--multitestrun':
        print('[scheduler.py] Multitestrun')
        _log_event_calls = True
        run_scheduler()
    else:
        run_scheduler()
