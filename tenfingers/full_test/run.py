# Python part of the test
# results are written to result.txt
# It is run in the ./test10f/ folder where /A/, /B/ and /C/ has been set up
# Just push your modification and run ./run.sh in a shell

#todo, instead of printing, echo information out into the result.txt file

import sys
import os
import time
from subprocess import Popen, PIPE, STDOUT, TimeoutExpired
import sqlite3
from sqlite3 import Error
import time


# To print out and reset after each test so we see how long they take
g_log_start = time.time()

# So we can print "success" or "failure" at the end of the testrun
g_any_error = False

g_database_file = "db/tenfingers.db"


def get_and_reset_elapsed_time():
    global g_log_start
    r = time.time() - g_log_start
    if r < 1:
        r = str(int(r * 100.0) / 100)
    else:
        if r < 10:
            r = str(int(r * 10.0) / 10)
        else:
            r = str(int(r))

    g_log_start = time.time()
    return r + " seconds"


def get_connection(database_file):
    try:
        connection = sqlite3.connect(database_file)
        return connection
    except Error:
        print(Error)

    # Something went wrong
    sys.exit(-3)


# returns the result after a fetch one
# example:
# value = query_one("path to database", "SELECT value FROM configuration_string WHERE name = ?", (name,))
# if not select, then execute
def query_one(database_file, query, extra_data=None, select=True, default=0):
    if not os.path.exists(database_file):
        print("The database file does not exist:", database_file, flush=True)
        return default
    connection = get_connection(database_file)
    cursor = connection.cursor()
    if extra_data is None:
        cursor.execute(query)
    else:
        cursor.execute(query, extra_data)
    if select:
        row = cursor.fetchone()
        if row is None:
            return None

        if len(row) == 1:
            return row[0]
    else:
        # execute:
        connection.commit()

    return None


# Note, on windows echo "abc > file.txt does weird things
# It adds the space before the > and a newline on top
# Yes I have tried echo|set /p="abc" > test.txt and it does not work
# So we accept 1 space + a newline at the end here
def check_file_content(file, expected_content):
    if not os.path.exists(file):
        print("The file does not exist:", file, flush=True)
        return False
    with open(file) as file:
        content = file.read().replace(" \n", "")
        content = content.replace("\n", "")
        if content == expected_content:
            #log_result("File compare: Success.", success=True)
            return True
        log_result("Error comparing: (expected=>) [" + expected_content + "] is not equal to [" + content + "] (<=found)", error=True)
        return False


# error sets color to red, success to green, neither set => normal text
def log_result(text, error=False, success=False):
    time_elapsed = get_and_reset_elapsed_time()

    if error:
        print(text, flush=True)
        # Red
        text = "\\e[31m" + text + "\\e[0m"
        global g_any_error
        g_any_error = True
    if success:
        print(text + '   ' + time_elapsed, flush=True)
        # Green
        text = "\\e[32m" + text + "\\e[0m" + '   ' + time_elapsed

    with open("./result.txt", "a") as file:
        file.write(text + "\n")


# Returns how many nodes this db has (4 max)
def got_nodes(db_file):
    return query_one(db_file, "SELECT COUNT(*) FROM node")


# Runs through the B, C and D folders, then data/1, 2, 3, ... in the search for the file
def count_shared_file(file_name):
    nof = 0
    paths = ['B/data_shared/', 'C/data_shared/', 'D/data_shared_changed/']
    for path in paths:
        for n in range(1, 30):
            p = path + str(n) + '/'
            if os.path.exists(os.path.join(p, file_name)):
                nof = nof + 1
    return nof


def run_command(command, directory):
    # example: command = "cwm --rdf test.rdf --ntriples > test.nt"

    # If shell=True then send in the arguments like a string, for example: "python3 ./10f.py -c a1"
    process = Popen(command, cwd=directory, shell=True, stdout=PIPE, stderr=PIPE)
    output, error = process.communicate()
    return output, error


def get_data_owned_version(name):
    # tenfingers_db_a = os.path.join(test_path, "A/", g_database_file)
    return query_one(tenfingers_db_a, "SELECT data_version FROM data_owned WHERE name = ?", (name,))


log_result("[SUCCESS starting test script] Starting Python part of tenfinger test...", success=True)
print("Starting Python part of tenfinger test...", flush=True)

# Listeners should already be up and running now.

# Get the tests home folder:
out, err = run_command("pwd", "./")
print('PWD:', out, flush=True)
test_path = out.decode().strip('\n') + "/"
if os.name == "nt":  # test_path = 'c/Users/...' change that to 'C:/...'
    drive = test_path[1:2]
    test_path = drive + ":" + test_path[2:]

print("test_path=" + test_path, flush=True)

tenfingers_db_a = os.path.join(test_path, "A/", g_database_file)

print("Make a link of a1.txt in /A/")

# make a link in A for data a1.txt (IPV4) and in C for c2.txt (IPV6)
# Note: it will generate the link where you call it from, so not in ./A
# FROM IPv4
run_command("python3 ./10f.py -c a1", test_path + "A/")
# This link will be used to check stale links, see below, that's why we copy it
run_command("cp a1.10f ../D/olda1link.10f", test_path + "A/")

# FROM IPv6
run_command("python3 ./10f.py -c c2", test_path + "C/")

print("Now download the data in /B/")

# Download a in B (IPv4 <= IPv4)
# Now B has the address of A
run_command("python3 ./10f.py -r ../A/a1.10f", test_path + "B/")

if check_file_content("./B/a1.txt", "a1"):
    log_result("[SUCCESS Download IPv4 <= IPv4: OKAY]", success=True)
else:
    log_result("[#ERROR: IPv4 <= IPv4 DOWNLOAD NOT OK]", error=True)

# Download c in A (IPv4 <= IPv6)
run_command("python3 ./10f.py -r ../C/c2.10f", test_path + "A/")
if check_file_content("./A/c2.txt", "c2"):
    log_result("[SUCCESS Download IPv4 <= IPv6: OKAY]", success=True)
else:
    log_result("[#ERROR: IPv4 <= IPv6 DOWNLOAD NOT OK]", error=True)

# Download a in C (IPv6 <= IPv4)
run_command("python3 ./10f.py -r ../A/a1.10f", test_path + "C/")
if check_file_content("./C/a1.txt", "a1"):
    log_result("[SUCCESS Download IPv6 <= IPv4: OKAY]", success=True)
else:
    log_result("[#ERROR: IPv6 <= IPv4 DOWNLOAD NOT OK]", error=True)

# Download c in D (IPv6 <= IPv6)
run_command("python3 ./10f.py -r ../C/c2.10f", test_path + "D/")
if check_file_content("./D/c2.txt", "c2"):
    log_result("[SUCCESS Download IPv6 <= IPv6: OKAY]", success=True)
else:
    log_result("[#ERROR: IPv6 <= IPv6 DOWNLOAD NOT OK]", error=True)

# Download using backslashes
# Export a link to /C/
run_command("python3 ./10f.py -c c1", test_path + "C/")

# Try to get it with backslashes
run_command("python3 ./10f.py -r ../C/c1.10f", test_path + "D/")
if check_file_content("./D/c1.txt", "c1"):
    log_result("[SUCCESS Download with windows backslashes OKAY]", success=True)
else:
    log_result("[#ERROR: download with windows backslashes NOT OK]", error=True)

# Test downloading with shortname
#print("Test downloading with shortname (\"a1\" instead of \"a1.10f\"")
# Remove old result
run_command("rm -f ./a1.txt", test_path + "B/")
# Download a1 with shortname
run_command("python3 ./10f.py -r ../A/a1", test_path + "B/")
if check_file_content("./B/a1.txt", "a1"):
    log_result("[SUCCESS Downloaded with shortname: OKAY]", success=True)
else:
    log_result("[#ERROR: Downloaded with shortname: NOT OK]", error=True)


# Test the digest

run_command("python3 ./10f.py -c a3", test_path + "A/")  # Make a link
run_command("rm -f a3.txt", test_path + "A/")  # Delete the original file, so that we can try to download it again
# change the digest in the link file
query_one(os.path.join(test_path, "A/", "a3.10f"), "UPDATE information SET data_rsa_pub = '0123456789'", select=False)
# Try to download the a3 file, this should not work as the digest is wrong
run_command("python3 ./10f.py -r ./a3.10f", test_path + "A/")  # Use it
if check_file_content("./A/a3.txt", "a3"):
    log_result("[#ERROR: Download, accepted bad digest: NOT OK]", error=True)
else:
    log_result("[SUCCESS Download, did not accept bad digest: OKAY]", success=True)


# Check changing the name of the shared file/folder can be changed
old_fil_name = "old_name.txt"
new_file_name = "new_name.txt"
run_command('echo "data in old name" > ' + old_fil_name, test_path + "A/")
run_command('echo "data in new name" > ' + new_file_name, test_path + "A/")
run_command("python3 ./10f.py -i name " + old_fil_name, test_path + "A/")
# Get the link
run_command("python3 ./10f.py -c name", test_path + "A/")
# Update the data, AND specifically the name of the file used
run_command("python3 ./10f.py -i name " + new_file_name, test_path + "A/")
# Copy it over to somewhere where the original files do not exist
run_command("cp name.10f ../D/name.10f", test_path + "A/")
# Download file
run_command("python3 ./10f.py name", test_path + "D/")
# Check it is new_name.txt and not old_name.txt

# Cleanup
run_command("python3 ./10f.py -d name", test_path + "A/")

if os.path.isfile(test_path + "D/" + new_file_name):
    log_result("[SUCCESS updating the shared files filename]", success=True)
else:
    if os.path.isfile(test_path + "D/" + old_fil_name):
        log_result("[#Error: updating the shared files filename] the old file name was used", error=True)
    else:
        log_result("[#Error: updating the shared files filename] no file was downloaded", error=True)
# Done


# Check if we can share a tar.gz file:
# a t.tar.gz has been created from a t.txt file and inserted under the name targz in /A/
# Get a link and extract it in say /D/
run_command("python3 ./10f.py -c targz", test_path + "A/")
run_command("cp targz.10f ../D/targz.10f", test_path + "A/")
# Download file
run_command("python3 ./10f.py targz", test_path + "D/")

# The file must be downloaded:
if os.path.isfile(test_path + "D/t.tar.gz"):
    # But not extracted:
    if not os.path.isfile(test_path + "D/t.txt"):
        log_result("[SUCCESS downloading a tar.gz file without treating it like a folder]", success=True)
    else:
        log_result("[#Error downloading a tar.gz file without treating it like a folder] Treated it like a folder by expanding it", error=True)
else:
    log_result(
        "[#Error downloading a tar.gz file without treating it like a folder] Could not download data", error=True)

# Cleanup
run_command("python3 ./10f.py -d targz", test_path + "A/")



###################################################
# Test if when we try to update a file, the comparison with the existing (old) file works
# so it doesn't do anything if they are the same
# Also with tar.gz (which is more complicated as they are not byte-exact copies even if the data is the same)

# Get the versions, should be 1
same_file_version = get_data_owned_version('same')
same_folder_version = get_data_owned_version('same_folder')

# Ask to update
run_command("python3 ./10f.py -i same same.txt", test_path + "A/")
run_command("python3 ./10f.py -i -f same_folder same_folder", test_path + "A/")

# Get the versions, should be the same
new_same_file_version = get_data_owned_version('same')
if same_file_version == new_same_file_version:
    log_result("[SUCCESS File update with same data skipped update OKAY]", success=True)
else:
    log_result("[#ERROR: File update with same data did make new version NOT OK]", error=True)

new_same_folder_version = get_data_owned_version('same_folder')
if same_folder_version == new_same_folder_version:
    log_result("[SUCCESS Folder update with same data skipped update OKAY]", success=True)
else:
    log_result("[#ERROR: Folder update with same data did make new version NOT OK]", error=True)

# Update the file and the folder
run_command('echo "new!" > same.txt', test_path + "A/")
run_command('echo "new!" > same_folder/same.txt', test_path + "A/")

# Ask to update
run_command("python3 ./10f.py -i same same.txt", test_path + "A/")
run_command("python3 ./10f.py -i -f same_folder same_folder", test_path + "A/")

# Get the versions, should be 2
new2_same_file_version = get_data_owned_version('same')
if new_same_file_version < new2_same_file_version:
    log_result("[SUCCESS File update with new data made new version OKAY]", success=True)
else:
    log_result("[#ERROR: File update with new data did not make new version NOT OK]", error=True)

new2_same_folder_version = get_data_owned_version('same_folder')
if new_same_folder_version < new2_same_folder_version:
    log_result("[SUCCESS Folder update with new data made new version OKAY]", success=True)
else:
    log_result("[#ERROR: Folder update with new data did not make new version NOT OK]", error=True)

# Clean up a bit:
run_command("python3 ./10f.py -d same", test_path + "A/")
run_command("python3 ./10f.py -d same_folder", test_path + "A/")

################################################
# Check the data is shared all over the place
# Wait until A gets the address of B (by B reaching out to A)

const_timeout = 180
db_a_file = os.path.join(test_path, "A/", g_database_file)
db_b_file = os.path.join(test_path, "B/", g_database_file)
db_c_file = os.path.join(test_path, "C/", g_database_file)
db_d_file = os.path.join(test_path, "D/", g_database_file)

nodes_a_before = got_nodes(db_a_file)
nodes_b_before = got_nodes(db_b_file)
nodes_c_before = got_nodes(db_c_file)
nodes_d_before = got_nodes(db_d_file)

# Addresses:
# A: 127.0.0.1::2000
# B: 127.0.0.1::3000
# C: 0:0:0:0:0:0:0:1::4000
# D: 0:0:0:0:0:0:0:1::5000
port_a = 2000
port_b = 3000
port_c = 4000
port_d = 5000
print("Wait for IPV4 and IPV6 listeners to have new addresses")
# Waiting for them all to have all takes too long
timeout = const_timeout
success = False
old_output = ""
while timeout > 0 and not success:
    timeout = timeout - 1
    # print("Wait, timeout=" + str(timeout), flush=True)
    # Does the B:s address has made its way to A?
    # todo: all listeners x all

    # Consider nodes to have aquired new nodes if they have:
    # 1) Aquired new nodes
    # 2) Have aquired all other nodes
    a_done = got_nodes(db_a_file) > nodes_a_before or got_nodes(db_a_file) >= 3
    b_done = got_nodes(db_b_file) > nodes_b_before or got_nodes(db_b_file) >= 3
    c_done = got_nodes(db_c_file) > nodes_c_before or got_nodes(db_c_file) >= 3
    d_done = got_nodes(db_d_file) > nodes_d_before or got_nodes(db_d_file) >= 3
    out = "["
    out += str(got_nodes(db_a_file))
    out += str(got_nodes(db_b_file))
    out += str(got_nodes(db_c_file))
    out += str(got_nodes(db_d_file))
    out += "]"
    if out != old_output:
        old_output = out
        print(out, flush=True)

    # If one of the IPV4 and one of the IPV6 are okay, then it is OK
    if (a_done or b_done) and (c_done or d_done):
        success = True
    else:
        time.sleep(1)


print("Node test terminated (results will be printed later).", flush=True)

if success:
    log_result("[SUCCESS both one IPV4 and one IPV6 node knows more addresses, or knows 3]", success=True)
else:
    log_result("[#ERROR: Some nodes never got to know the address to some other nodes, TIMEOUT]", error=True)
    log_result("The nodes (A, B, C, D) (expected result for them all: have more nodes, did not work out)", error=True)
    log_result("BEFORE", error=True)
    log_result("A:" + str(nodes_a_before))
    log_result("B:" + str(nodes_b_before))
    log_result("C:" + str(nodes_c_before))
    log_result("D:" + str(nodes_d_before))
    log_result("AFTER", error=True)
    log_result("A:" + str(got_nodes(db_a_file)))
    log_result("B:" + str(got_nodes(db_b_file)))
    log_result("C:" + str(got_nodes(db_c_file)))
    log_result("D:" + str(got_nodes(db_d_file)))

################################################################
# Check versioning works by making a new version of a1
run_command("echo NewVersion > a1.txt", test_path + "A/")
# Update it
run_command("python3 ./10f.py -i a1 a1.txt", test_path + "A/")
# Check if the other nodes have changed the version (they are up in this scenario and should do it instantly)
# Make a new link
run_command("python3 ./10f.py -c a1", test_path + "A/")

timeout = const_timeout
success = False
version = 0
link_file = os.path.join(test_path, "A/", "a1.10f")
while timeout > 0 and not success:
    timeout = timeout - 1

    # Check it has a newer version
    version = query_one(link_file, "SELECT file_version FROM information WHERE name = ?", ("a1",))
    if version >= 2:
        success = True
    else:
        time.sleep(1)
if success:
    log_result("[SUCCESS creating link: The version was updated in the local link.]", success=True)
else:
    log_result("[#ERROR: creating link of new version: version is " + str(version) + " in the local link, expected to be 2]", error=True)

#

# Download it in another node
run_command("python3 ./10f.py -r ../A/a1.10f", test_path + "C/")

timeout = const_timeout
success = False
while timeout > 0 and not success:
    timeout = timeout - 1

    # Check the version (should be 2), todo: check all of the possible entries
    version = query_one(link_file, "SELECT file_version FROM information WHERE name = ?", ("a1",))

    # Check the data, should be NewVersion
    if check_file_content("./C/a1.txt", "NewVersion"):
        success = True
    else:
        time.sleep(1)
if success:
    log_result("[SUCCESS Downloading new version elsewhere: data is the more recent one.]", success=True)
else:
    log_result("[#ERROR: DOWNLOAD new version elsewhere: data was not updated!]", error=True)

if version == 2:
    log_result("[SUCCESS Downloadinging the new version elsewhere: We downloaded the new version.]", success=True)
else:
    log_result("[#ERROR: DOWNLOAD the new version elsewhere: version is " + str(version) + " in the link, expected to be 2]", error=True)

#

# Now use an old link with version=1 which lingered in ./olda1link.10f
run_command("python3 ./10f.py -r ./olda1link.10f", test_path + "D/")
link_file = os.path.join(test_path, "D/", "olda1link.10f")

timeout = const_timeout
success = False
while timeout > 0 and not success:
    timeout = timeout - 1

    # Check the version (should be 2), todo: check all of the possible entries
    version = query_one(link_file, "SELECT file_version FROM information WHERE name = ?", ("a1",))
    if version == 2:
        success = True
    else:
        time.sleep(1)

if success:
    log_result("[SUCCESS Downloading the new version: The version was updated by the serving node.]", success=True)
else:
    log_result("[#ERROR: DOWNLOAD new version: version is " + str(version) + " in the link, expected to be 2]", error=True)

#

# Check the data, should be NewVersion
if check_file_content("./D/a1.txt", "NewVersion"):
    log_result("[SUCCESS Downloading new version: data is the more recent one.]", success=True)
else:
    log_result("[#ERROR: DOWNLOAD new version: data was not updated!]", error=True)


# Test the folder link
run_command("python3 ./10f.py -c f1", test_path + "A/")
run_command("python3 ./10f.py -r ../A/f1.10f", test_path + "B/")
if check_file_content("./B/base_folder/folder/f1.txt", "f1"):
    log_result("[SUCCESS Folder downloaded and expanded correctly.]", success=True)
else:
    log_result("[#Error: Download or unzipping of folder failed]", error=True)


#

# Check translate file is used

# Use the existing a1 link which haves addresses in both the link and tr file
# but remove all addresses in link file

# Clean up

# Test the Translation files are correctly updated

# Speed up things a bit by deleting a1 anf f1 links
# This will also be used to check nodes eventually delete files if they detect the reciprocal file is no longer shared
shared_a1_files = count_shared_file('a1.aes')
run_command("python3 ./10f.py -d a1", test_path + "A/")

translation_test_folder = "A/"
translation_test_shortname = "a2"
db_a_file = os.path.join(test_path, translation_test_folder, g_database_file)
# Now D:s address will propagate through the network, but more importantly
# the a1_tr and a2_tr files will be updated with A:s new address and updated in the other nodes
# We will use the a2_tr to check things work, as we already have fiddled with a1
# make a link for a2
timeout = const_timeout
success = False
link_file_tr_10f = os.path.join(test_path, translation_test_folder, translation_test_shortname + "_tr.10f")
link_file_tr_atf = os.path.join(test_path, translation_test_folder, translation_test_shortname + "_tr.atf")
link_file_sub_10f = os.path.join(test_path, translation_test_folder, translation_test_shortname + "_sub.10f")
link_file_sub_atf = os.path.join(test_path, translation_test_folder, translation_test_shortname + "_sub.atf")
print('atr = addresses from ' + translation_test_shortname + '_tr.10f file')
print('apltr = addresses in the data/trans/' + translation_test_shortname + '_tr.atf file (for downloading the link payload)')
print('apltrother = addresses in the data/trans/' + translation_test_shortname + '_tr.atf file for downloading the data/trans/' + translation_test_shortname + '_sub.atf file (other_tr_sub_addresses)')
print('asub = addresses from ' + translation_test_shortname + '_sun.10f file')
print('aplsub = addresses in the data/trans/' + translation_test_shortname + '_sub.atf file (for downloading the link payload)')
print('aplsubother = addresses in the data/trans/' + translation_test_shortname + '_sub.atf file for downloading the data/trans/' + translation_test_shortname + '_tr.atf file (other_tr_sub_addresses)', flush=True)

print('   a2.10f (0) =>   DATA          [This is the base link file]')
print('                                            ')
print('   a2_tr.10f (1) =>  a2_tr.atf              ')
print('     a2_tr.atf (2) =>  DATA                 ')
print('     a2_tr.atf (3 other) =>  a2_sub.atf     ')
print('                       ')
print('   a2_sub.10f (4) =>  a2_tr.atf             ')
print('     a2_sub.atf (5) =>  DATA                ')
print('     a2_sub.atf (6 other) =>  a2_tr.atf     ')
print('                       ')
print('   In theory:                          ')
print('       (0) == (2) == (5)     As they point on the base DATA           ')
print('       (1) == (6)   as they both point on a2_tr.atf           ')
print('       (4) == (3)   as they both point on a2_sub.atf            ')
print('atf = Address Translation File                       ')
print('tr = Translation File   (Preventing link decay of the base link file, and of the substitution file)')
print('sub = Substitution File (Preventing link decay of the base link file, and of the translation file)')
print('(x) = addresses')
print('(x other) = addresses pointing on the other _tr.atf or _sub.atf')


while timeout > 0 and not success:
    success_tr = False
    success_sub = False
    # Try to generate the _tr, _sub and then download the _tr.atf, _sub.atf files
    # Remove old ones first
    run_command("rm -f ./" + translation_test_shortname + ".10f", test_path + translation_test_folder)
    run_command("rm -f ./" + translation_test_shortname + "_tr.10f", test_path + translation_test_folder)
    run_command("rm -f ./" + translation_test_shortname + "_sub.10f", test_path + translation_test_folder)
    run_command("rm -f ./" + translation_test_shortname + "_tr.atf", test_path + translation_test_folder)
    run_command("rm -f ./" + translation_test_shortname + "_sub.atf", test_path + translation_test_folder)

    # Export the link files
    run_command("python3 ./10f.py -c " + translation_test_shortname, test_path + translation_test_folder)

    # Then download the link data, thus force the update of the tr & sub files
    run_command("python3 ./10f.py " + translation_test_shortname + ".10f", test_path + translation_test_folder)
    timeout = timeout - 1



    # Check the translation files addresses (.10f link and atf payload)

    addresses_tr = query_one(link_file_tr_10f, "select count(*) from address")
    addresses_payload_tr = query_one(link_file_tr_atf, "select count(*) from address")
    addresses_other_tr = query_one(link_file_tr_atf, "select count(*) from other_tr_sub_address")
    if addresses_tr >= 2 and addresses_payload_tr >= 2 and addresses_other_tr >= 2:
        success_tr = True

    # Check the substitution files addresses (.10f link and atf payload)
    addresses_sub = query_one(link_file_sub_10f, "select count(*) from address")
    addresses_payload_sub = query_one(link_file_sub_atf, "select count(*) from address")
    addresses_other_sub = query_one(link_file_sub_atf, "select count(*) from other_tr_sub_address")

    if addresses_sub >= 2 and addresses_payload_sub >= 2 and addresses_other_sub >= 2:
        success_sub = True

    if success_tr and success_sub:
        success = True
    else:
        time.sleep(1)
    print('atr (1) apltr (2) apltrother (3)   asub (4) aplsub (5) aplsubother (6)',
          addresses_tr, addresses_payload_tr, addresses_other_tr, "",
          addresses_sub, addresses_payload_sub, addresses_other_sub,
          flush=True)

if success:
    log_result("[SUCCESS Translation file generated.]", success=True)
else:
    log_result("[#Error: Translation file wasn't generated, or no one wanted to share it for us.]", error=True)

# Delete all addresses in a2.10f
run_command("python3 ./10f.py -c a2", test_path + "A/")
link_file = os.path.join(test_path, "A/", "a2.10f")
connection = get_connection(link_file)
cursor = connection.cursor()
cursor.execute("DELETE FROM address WHERE 1")
connection.commit()


# Download it in another node
run_command("python3 ./10f.py ../A/a2.10f", test_path + "B/")


# Check the data, should be NewVersion
if check_file_content("./B/a2.txt", "a2"):
    log_result("[SUCCESS Translation files version correctly used when no other addresses existed.]", success=True)
else:
    log_result("[#Error: Download failed to use the translation files addresses]", error=True)


# Check the deleted files
#
#     THIS IS NOT YET IMPLEMENTED, or is it? How do we tell nodes they don't NEED to share our data any more
#    => We do this implicitly by not sharing their reciprocal data (but what if that was a dummy file?)
#
# This should work as a1 was extensively tested for sharing already, so already shared.
# Now we want other nodes to delete a1.aes as we don't want it to be shared anymore
print('Check nodes clean up files when the file is now deleted')
print('Shared at the beginning:', shared_a1_files)
# todo: check if the reciprocal file is deleted too ?
if False:
    timeout = const_timeout
    success = False
    while timeout > 0 and not success:
        timeout = timeout - 1
        # Run through the B, C and D folders, then data/1, 2, 3 in the search for the disappeared data files
        shared_now = count_shared_file('a1.aes')

        if shared_a1_files != shared_now:
            success = True
        else:
            time.sleep(1)

    if success:
        log_result("[SUCCESS Deleted file was deleted from sharing node.]", success=True)
    else:
        log_result("[#Error: Deleted file was not deleted from sharing node]", error=True)
else:
    log_result("[NOT YET IMPLEMENTED: Deleted file was deleted from sharing node.]")

# Check translatiuon files actually does its job
# ==> NEEDS MODIFIED .atf files
# ==> OR change addresses of the listeners (+ relaunch)

# Delete all addresses in a2_sub.10f
link_file = os.path.join(test_path, "A/", "a2_sub.10f")
connection = get_connection(link_file)
cursor = connection.cursor()
cursor.execute("DELETE FROM address WHERE 1")
connection.commit()

# Download the a2 file
run_command("python3 ./10f.py a2.10f", test_path + "A/")

# Now, as long as the a2_tr.atf files "other" addresses have some addresses, they should be in the a2_sub.10f link
# So check we have more than zero addresses in a2_sub.10f address_list
link_file_a2_sub10f = os.path.join(test_path, "A/", "a2_sub.10f")
if query_one(link_file_a2_sub10f, "select count(*) from address") > 0:
    log_result("[SUCCESS atf file updated reciprocal 10f file (_tr.atf:other_tr_sub_addresses => a2_sub.10f:address).]", success=True)
else:
    log_result("[#Error: atf file FAILED to updated reciprocal 10f file (_tr.atf:other_tr_sub_addresses => a2_sub.10f:address)]", error=True)

# Now do it the other way around:
link_file = os.path.join(test_path, "A/", "a2_tr.10f")
connection = get_connection(link_file)
cursor = connection.cursor()
cursor.execute("DELETE FROM address WHERE 1")
connection.commit()

# Download the a2 file
run_command("python3 ./10f.py a2.10f", test_path + "A/")

# Now, as long as the a2_tr.atf files "other' addresses have some addresses, they should be in the a2_sub.10f link
# So check we have more than zero addresses in a2_sub.10f address_list
link_file_a2_tr10f = os.path.join(test_path, "A/", "a2_tr.10f")
if query_one(link_file_a2_tr10f, "select count(*) from address") > 0:
    log_result("[SUCCESS atf file updated reciprocal 10f file (_sub.atf:other_tr_sub_addresses => a2_tr.10f:address).]", success=True)
else:
    log_result("[#Error: atf file FAILED to updated reciprocal 10f file (_tr.atf:other_sub_sub_addresses => a2_tr.10f:address)]", error=True)


# Check that we correctly track nodes serving wrong data:
# To do this we simulate the wrongdoing by changing our base version, without updating its version
# Now all sharing nodes will serve the 'old' data and we will flag them as serving wrong data
# In short: we count the number of errors the scheduler notes, and they should go up as we faked bad data.
def count_integrity_fails(dataname):
    print("count_integrity_fails for:" + dataname)
    number_of_fails = 0
    # Get data_id in data_owned
    db_file = os.path.join(test_path, "D/", g_database_file)
    data_owned_id = query_one(db_file, "SELECT id FROM data_owned WHERE name = ?", (dataname,))
    if data_owned_id:
        print("count_integrity_fails: got data_owned_id:" + str(data_owned_id))
        fingers = query_one(db_file, "select count(*) from finger", ())
        print("count_integrity_fails: nof fingers:" + str(fingers))
        finger_list = []
        # Can obviously be done better. Loops all 16 fingers to get the nodes sharing our data
        for finger_id in range(0, fingers):  # Ugly just get our fingers pointing on our data
            if query_one(db_file, "SELECT id FROM finger WHERE id = ? AND data_owned_id = ?", (finger_id, data_owned_id)):
                finger_list.append(finger_id)

        if len(finger_list) == 0:
            log_result("count_integrity_fails error: no fingers", error=True)

        # Loop all fingers with data_id = that id
        for finger_id in finger_list:

            # and add up data_integrity_fails
            data_integrity_fails = query_one(db_file, "SELECT data_integrity_fails FROM finger WHERE id = ?", (finger_id,))
            print("count_integrity_fails: check fails for finger_id:" + str(finger_id))
            if data_integrity_fails:
                print("count_integrity_fails: they were:" + str(data_integrity_fails))
                number_of_fails += data_integrity_fails
    else:
        log_result("count_integrity_fails error: no data_owned_id", error=True)

    return number_of_fails


# change the file content
print("Change our d1 data to fake all sharers have the wrong data")
with open("D/data_owned_changed/d1.aes", "w") as file:
    file.write('01234567890')

fail_file = "d1"
print("Count all wrongly served data as a reference")
fails = count_integrity_fails(fail_file)
print("Count all wrongly served data as a reference: " + str(fails))
print('Check nodes gets flagged if they serve bad files')
print('Flags (sometimes normally happens when version changes) at the beginning:', fails)
success = False
timeout = const_timeout
while timeout > 0 and not success:
    timeout = timeout - 1
    # Check if we are flagging nodes sharing "d1"
    ###print info ici
    if fails != count_integrity_fails(fail_file):
        success = True
    else:
        time.sleep(1)

if success:
    log_result("[SUCCESS at flagging nodes serving bad data]", success=True)
else:
    log_result("[#Error: at flagging nodes serving bad data]", error=True)


# Verify a3 has been shared by other nodes (check there are fingers for a3)

data_id_for_a3 = query_one(tenfingers_db_a, "SELECT id FROM data_owned WHERE name = ?", ("a3",))
a_finger_for_a3 = 0
success = False
timeout = const_timeout
while timeout > 0 and not success:
    timeout = timeout - 1

    a_finger_for_a3 = query_one(tenfingers_db_a, "SELECT id FROM finger WHERE data_owned_id = ? LIMIT 1", (data_id_for_a3, ))
    if a_finger_for_a3:
        success = True
    else:
        time.sleep(1)
if success:
    log_result("[SUCCESS a3 was shared by at least one other node]", success=True)
else:
    log_result("[#Error: no other node shared a3]", error=True)

# Test deletion of data works as expected

# Note: Previous test must have succeeded for this one to function
shared_data_id = query_one(tenfingers_db_a, "SELECT data_shared_id FROM finger WHERE data_owned_id = ? LIMIT 1", (data_id_for_a3, ))
shared_data_folder = query_one(tenfingers_db_a, "SELECT folder FROM data_shared WHERE id = ? LIMIT 1", (shared_data_id, ))
folder = 'A/data_shared/' + str(shared_data_folder)
if not os.path.isdir(folder):
    log_result("[#Error: folder non existant]" + str(folder), error=True)
# delete a3
run_command("python3 ./10f.py -d a3", test_path + "A/")

# Check a3 is now gone
if query_one(tenfingers_db_a, "SELECT id FROM data_owned WHERE name = ?", ("a3",)):
    log_result("[#Error: when deleting a3, it was not removed from the database.data_owned]" + str(folder), error=True)
else:
    log_result("[SUCCESS a3 correctly deleted from the database.data_owned]", success=True)

# Check the folder is now gone
if os.path.isdir(folder):
    log_result("[#Error: folder was not deleted when deleting a3]" + str(folder), error=True)
else:
    log_result("[SUCCESS when a3 was deleted so was its reciprocally shared data]", success=True)



# By hand:

# Todo: automatize the safe / hidden / secure -afar flag:
# Exported links, _tr and _sub files should be Devoid of emitters IP:PORT address

# take
# python3 ./10f.py
# link.10f <= remove all addresses
# link_tr.10f => download the atf file
# link_sub.10f => download the atf file
# delete the _tr and_sub files
# (copy the atf files)
# Scenario 1:
#  Delete all addresses in link_tr.atf
#  Delete addresses but leav the 'other addresses' in link_sub.atf
# Try to download
# Scenario 2: same but switch the _tr.etf and _sub.atf files (only keep 'other' addresses in link_tr.atf)

# Note: You will need to have the _tr.10f and _sub.10f files or python3 ./10f.pypython3 ./10f.py won't try to use the .atf files
# so clean out the address table in them

# Manually tested, with success as result; branch version1_0

if g_any_error:
    log_result("TESTRUN COMPLETED, THERE WERE ERRORS", error=True)
else:
    log_result("TESTRUN COMPLETED SUCCESSFULLY.")
