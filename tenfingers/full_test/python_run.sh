# This checks out the mater branch, and runs tests directly with the python sources
# It should be the mother of all tests

# All of this is done in a subfolder outside of our versioned version
# If it does not exist, it is created (../../../test10f/work/)
# and a copy of .git is put there

# Todo: clean out all the old tests (including the --testcase from python sources)
# Todo: somehow not generate 4k RSA keys as it's quite slow

date
SECONDS=0
echo "START TESTRUN"

# Generate test folders
echo "[Setup]"
if [[ ! -d ../../../test10f ]]
then
    mkdir ../../../test10f
fi
if [[ ! -d ../../../test10f/work/ ]]
then
    mkdir ../../../test10f/work/
fi

# Go to the test repo
cd ../../../test10f/

# Generate the relaunch all listeners .sh LINUX only unfortunately
if [[ "$OSTYPE" == "linux-gnu"* ]]; then

  echo "kill \$(ps aux | grep './test_[l]istener.py' | awk '{print \$2}')" > stop_all_test_listeners.sh
  chmod 744 ./stop_all_test_listeners.sh
  # Kill any listeners running (Linux) Do this as early as possible so that an old testrun has time to shut down IPV6
  ./stop_all_test_listeners.sh&
fi

# Linux
echo "#!/usr/bin/env bash" > restart_all_listeners.sh
echo 'if [[ "$OSTYPE" == "linux-gnu"* ]]; then' >> restart_all_listeners.sh
echo "  kill \$(ps aux | grep './test_[l]istener.py' | awk '{print \$2}')" >> restart_all_listeners.sh
echo "  cd A" >> restart_all_listeners.sh
#echo "python3 ./test_listener.py&" >> restart_all_listeners.sh
echo "  gnome-terminal -- python3 ./test_listener.py" >> restart_all_listeners.sh

echo "  cd ../B" >> restart_all_listeners.sh
echo "  gnome-terminal -- python3 ./test_listener.py" >> restart_all_listeners.sh
echo "  cd ../C" >> restart_all_listeners.sh
echo "  gnome-terminal -- python3 ./test_listener.py" >> restart_all_listeners.sh
echo "  cd ../D" >> restart_all_listeners.sh
echo "  gnome-terminal -- python3 ./test_listener.py" >> restart_all_listeners.sh
# Windows
echo 'elif [[ "$OSTYPE" == "msys" ]]; then' >> restart_all_listeners.sh
echo "  cd A" >> restart_all_listeners.sh
echo "  start start_listener.bat" >> restart_all_listeners.sh
echo "  cd ../B" >> restart_all_listeners.sh
echo "  start start_listener.bat" >> restart_all_listeners.sh
echo "  cd ../C" >> restart_all_listeners.sh
echo "  start start_listener.bat" >> restart_all_listeners.sh
echo "  cd ../D" >> restart_all_listeners.sh
echo "  start start_listener.bat" >> restart_all_listeners.sh
#end
echo "fi" >> restart_all_listeners.sh

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  chmod 744 ./restart_all_listeners.sh
fi

cd work

# Set up a git repo by lazily copying our working copy
if [[ ! -d .git ]]
then
    cp -r ../../Tenfingers/.git ./.git
fi

echo "[Checkout]"
git fetch
# Get or create a 'test' branch
if [ `git rev-parse --verify test 2>/dev/null` ]
then
    git checkout -B test
else
    git checkout -b test
fi
# Reset to origin/main
git reset --hard origin/main

# Setup compiling environment

echo "$(($SECONDS)) seconds elapsed."

cd ../

# Kill off any lingering listener
# WIN ONLY as their stack is not very good
# WINDOWS: echo "Taskkill /IM \"listener.exe\" /F" > kill_listeners.sh
#echo "killall -e listener" > kill_listeners.sh
#chmod 744 ./kill_listeners.sh
#./kill_listeners.sh > run.log


# Clean up
rm -rf ./A > run.log
rm -rf ./B > run.log
rm -rf ./C > run.log
rm -rf ./D > run.log
if [[ -d A ]]
then
  echo "ERROR CANNOT REMOVE FOLDER A!"
fi
if [[ -d B ]]
then
  echo "ERROR CANNOT REMOVE FOLDER B!"
fi
if [[ -d C ]]
then
  echo "ERROR CANNOT REMOVE FOLDER C!"
fi
if [[ -d D ]]
then
  echo "ERROR CANNOT REMOVE FOLDER D!"
fi

echo "$(($SECONDS)) seconds elapsed."


# Set up 4 folders in /test10f/
mkdir A
mkdir A/db
# Make a folder to test sharing folders
cd A
mkdir base_folder
cd base_folder
mkdir folder
cd ..
cd ..
mkdir B
mkdir B/db
mkdir C
mkdir C/db
mkdir D
mkdir D/db

# deploy the python sources
echo "[Deploy python sources]"

cp ./work/tenfingers/*.py A
cp ./work/tenfingers/*.py B
cp ./work/tenfingers/*.py C
cp ./work/tenfingers/*.py D


echo "$(($SECONDS)) seconds elapsed."

# Copy over cached RSA keys (for speed, generating it's kind of slow if you are in a hurry)
echo "[Copy over cached RSA keys for speed]"
if [[ -d rsa_cache ]]
then
    echo "[Reuse old RSA keys for speed]"
    cp rsa_cache/A_public_key ./A/db/public_key
    cp rsa_cache/A_secret_key ./A/db/secret_key
    cp rsa_cache/B_public_key ./B/db/public_key
    cp rsa_cache/B_secret_key ./B/db/secret_key
    cp rsa_cache/C_public_key ./C/db/public_key
    cp rsa_cache/C_secret_key ./C/db/secret_key
    cp rsa_cache/D_public_key ./D/db/public_key
    cp rsa_cache/D_secret_key ./D/db/secret_key
else
    echo "[Generate new RSA keys]"
fi

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  # do nothing ( : == NOP )
  :
else
  echo 'Create startup .bat files for windows'
  echo 'python ./test_listener.py' > A/start_listener.bat
  echo 'python ./test_listener.py' > B/start_listener.bat
  echo 'python ./test_listener.py' > C/start_listener.bat
  echo 'python ./test_listener.py' > D/start_listener.bat
fi

echo "$(($SECONDS)) seconds elapsed."

# Generate RSA keys (if needed) and set IP:PORTs

echo "[Setup RSA keys for all projects]"
cd A
python3 ./setup.py > setup.log &
cd ../B
python3 ./setup.py > setup.log &
cd ../C
python3 ./setup.py > setup.log &
cd ../D
python3 ./setup.py > setup.log &
cd ../A
wait

echo "$(($SECONDS)) seconds elapsed."


echo "[Setup A]"
python3 ./10f.py -p ip 127.0.0.1 >> setup.log
python3 ./10f.py -p port 2000 >> setup.log
python3 ./10f.py -p scheduler_wait_msec 50 >> setup.log
# Set a "test" variable so thet we can use small quickly generated RSA keys
python3 ./10f.py -p _unsafe_small_rsa_keys 1
echo "[Setup B]"
cd ../B
python3 ./10f.py -p ip 127.0.0.1 >> setup.log
python3 ./10f.py -p port 3000 >> setup.log
python3 ./10f.py -p scheduler_wait_msec 50 >> setup.log
# Set a "test" variable so thet we can use small quickly generated RSA keys
python3 ./10f.py -p _unsafe_small_rsa_keys 1
echo "[Setup C]"
cd ../C
python3 ./10f.py -p ip "0:0:0:0:0:0:0:1" >> setup.log
python3 ./10f.py -p port 4000 >> setup.log
python3 ./10f.py -p scheduler_wait_msec 50 >> setup.log
# Set a "test" variable so thet we can use small quickly generated RSA keys
python3 ./10f.py -p _unsafe_small_rsa_keys 1
echo "[Setup D]"
cd ../D
python3 ./10f.py -p ip "0:0:0:0:0:0:0:1" >> setup.log
python3 ./10f.py -p port 5000 >> setup.log
python3 ./10f.py -p scheduler_wait_msec 50 >> setup.log


python3 ./10f.py -p data_owned_folder 'data_owned_changed' >> setup.log
python3 ./10f.py -p data_owned_translation_folder 'trans_changed' >> setup.log
python3 ./10f.py -p data_shared_folder 'data_shared_changed' >> setup.log
# Set a "test" variable so that we can use small quickly generated RSA keys
python3 ./10f.py -p _unsafe_small_rsa_keys 1
cd ..

# Copy over RSA keys to the cache so that we can reuse them next time we run the tests
if [[ ! -d rsa_cache ]]
then
    mkdir rsa_cache
fi
cp ./A/db/public_key rsa_cache/A_public_key
cp ./A/db/secret_key rsa_cache/A_secret_key
cp ./B/db/public_key rsa_cache/B_public_key
cp ./B/db/secret_key rsa_cache/B_secret_key
cp ./C/db/public_key rsa_cache/C_public_key
cp ./C/db/secret_key rsa_cache/C_secret_key
cp ./D/db/public_key rsa_cache/D_public_key
cp ./D/db/secret_key rsa_cache/D_secret_key




# Start listeners from here before launching the python tests:
#./restart_all_listeners.sh > run.log &
# Rename liteners so that when we can blanketkill them, other listeners will not be stopped

echo "$(($SECONDS)) seconds elapsed."
echo "[Start all the listeners]"
#cd ./A
#python3 ./test_listener.py > listener.log 2> stderr.log&
#cd ../B
#python3 ./test_listener.py > listener.log 2> stderr.log&
#cd ../C
#python3 ./test_listener.py > listener.log 2> stderr.log&
#cd ../D
#python3 ./test_listener.py > listener.log 2> stderr.log&
#cd ..

mv ./A/listener.py ./A/test_listener.py
mv ./B/listener.py ./B/test_listener.py
mv ./C/listener.py ./C/test_listener.py
mv ./D/listener.py ./D/test_listener.py

# Don't use the & here as the listeners create the folders where data is inserted to
./restart_all_listeners.sh





# Insert 3 data in each, a1.txt, a2.txt, .. d3.txt
# Lets do it by ugly cut'n paste code
echo "$(($SECONDS)) seconds elapsed."
echo "[Insert data]"
cd A
echo "a1" > a1.txt
python3 ./10f.py -i a1 a1.txt > insert.log &
echo "a2" > a2.txt
python3 ./10f.py -i a2 a2.txt >> insert.log &
echo "a3" > a3.txt
python3 ./10f.py -i a3 a3.txt >> insert.log &
echo "to compress" > t.txt
tar -czvf t.tar.gz t.txt
python3 ./10f.py -i targz t.tar.gz >> insert.log &
echo "tr" > tr.txt
cd base_folder/folder
echo "f1" > f1.txt
echo "f2" > f2.txt
echo "f3" > f3.txt
cd ../..
python3 ./10f.py -i -f f1 base_folder >> insert.log &

echo "same" > same.txt
python3 ./10f.py -i same same.txt > insert.log &
mkdir same_folder
echo "same" > same_folder/same.txt
python3 ./10f.py -i -f same_folder same_folder > insert.log &

cd ../B
echo "b1" > b1.txt
python3 ./10f.py -i b1 b1.txt > insert.log &
echo "b2" > b2.txt
python3 ./10f.py -i b2 b2.txt >> insert.log &

cd ../C
echo "c1" > c1.txt
python3 ./10f.py -i c1 c1.txt > insert.log &
echo "c2" > c2.txt
python3 ./10f.py -i c2 c2.txt >> insert.log &

cd ../D
# d1 will be changed without updating its version, simulating that all other nodes
# serves wrong files at download.
echo "d1" > d1.txt
python3 ./10f.py -i d1 d1.txt > insert.log &
#echo "d2" > d2.txt
#python3 ./10f.py -i d2 d2.txt >> insert.log &

wait


cd ..

echo "$(($SECONDS)) seconds elapsed."

# Phew...



# Now everything is correctly set up
# We will switch to python here so that we can (more) easily
# manage sub-processes (the listeners)
echo "[Switch to Python]"
rm -f ./result.txt

rm -f ./run_copy.py
cp ../Tenfingers/tenfingers/full_test/run.py run_copy.py

python3 ./run_copy.py

# Kill off the lingering listener.exe files as they won't take orders (thanks windows...)
#./kill_listeners.sh > nul 2>&1
# Kill any listeners running (Linux)
kill $(ps aux | grep './test_[l]istener.py' | awk '{print $2}')

echo "[Back from python.]"

echo "$(($SECONDS)) seconds elapsed."


# Show the python runs result:
echo "-------------------------------------"
echo "----- RESULTS:"

#cat ./result.txt
v=$(<./result.txt)
echo -e "$v"

echo "$(($SECONDS)) seconds elapsed."
#if grep -q ERROR "filename"; then
#    echo "THERE WERE ERRORS!"
#else
#   echo "SUCCESS! (No errors)"
#fi
