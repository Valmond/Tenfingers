# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import os
import sys
import platform

import locator
from storagesizes import get_owned_files_size, get_shared_files_size

try:
    import access as ac
except ImportError:
    print("Error: You need to install Cryptodome:\npip3 install pycryptodomex\n")

from database_query import set_afar_flag, get_wanted_fingers, set_wanted_fingers, get_link_data_id_by_name, get_translation_file_type
import database_query as dq
from helpers import human_readable_file_size
from configuration import get_configuration_value, set_int_configuration_value
import address_registry as ar
import filepath as fp


# Always run in its specific folder
# This makes it work if we launch 10f from another folder
# but has the drawback that the insert file path is no longer relative
# We could figure out where we need to change those paths instead.
# We need to test this with frozen executables and with python, both on Linux and Windows
# Works on Linux Python.
#os.chdir(locator.module_path() + "/")

"""""
Single file / folder / (later list.txt / regex): -> also generates link file!
10f [-i][-f][-r][-n] name filename

-i = insert data (as opposed to download)
-f = folder
-n = no error if linkfile already exists

# Get data, ONLY use the link file
10f name.10f

"""

# Set up the way to call an executable depending on which OS we are on:
executable = '10f'
if platform.system() == 'Windows':
    executable = './10f.exe'


def print_specific_help(specific_help_type):
    if specific_help_type == '-i':
        print('Insert: use to insert new data:')
        print('example:')
        print(executable + ' -i dataname filename')
        print(executable + ' -i -f dataname folder_to_add regex')
        print('Note: an all encompassing regex can be: .*')
        print('-x tries to upload the data right away (default 10 tries)')
        print(executable + ' -i -x dataname filename [x-tries]')
        print(executable + ' -i -f -x dataname folder_to_add regex [x-tries]')
        print('Use the -a flag for afar sharing, it excludes your address from the link, see ' + executable + ' -h -a')
        # add a -s / --secret flag to set hide_my_address to 1 (for main & _tr + _sub entries)?
        # OR make it a specific tag like ./10f name -s / --secret
        # OR both ...  <= Ding ding we have a winner!

    if specific_help_type == '-a':
        print('Afar flag.')
        print('The afar flag omits your IP:PORT in link files concerning a specific data you share.')
        print('You might want to use this flag if you do not want to expose your IP:PORT address of the data shared.')
        print('')
        print('It can be used when you insert data, like this:')
        print(executable + ' -i -a dataname filename')
        print('or it be set at a later time like this:')
        print(executable + ' -a dataname [on/off/1/0]')
        print('It sets / resets the afar flag (on or 1 sets, and off or 0 resets the flag).')
        print('')
        print('When the afar flag is set, it will omit your address in the link file (plus the translation and substitution files)')
        print('when you export them.')
        print('Old link files will still contain your IP:PORT address, re export them if you change the status of the afar flag.')
        print('')
        print('with the afar flag set, you must wait until at least one other node shares your data')
        print('before exporting a link, otherwise it cannot be downloaded.')

    if specific_help_type == '-r':
        print('Download using only addresses in the link file.')
        print('This is useful when there is no link decay (new link, no address changes)')
        print('and you want to download as quickly as possible.')
        print('Without the -r the translation files will first be downloaded and then')
        print('with their addresses, the data will be downloaded.')

    if specific_help_type == '-f':
        print('Folder: Inserts the folder as data.')
    if specific_help_type == '-u':
        print("Force update the data")
        print(executable + ' -u dataname')
    if specific_help_type == '-n':
        print('Overwrites without error if the resulting linkfile exists.')
    if specific_help_type == '-c' or specific_help_type == '-silent':
        print('Create a link')
        print('example:')
        print(executable + ' -c dataname')
        print('use -silent to exclude your own address:')
        print(executable + ' -c -silent dataname')
    if specific_help_type == '-l':
        print('List information')
    if specific_help_type == '-la':
        print('List information')
        print('example for the global database:')
        print('example for the global database:')
        print(executable + ' -la')
        print('example for a specific entry:')
        print(executable + ' -la dataname')
    if specific_help_type == '-s':
        fingers_wanted_our_data = get_configuration_value('fingers_wanted_our_data')
        fingers_wanted_translation_file = get_configuration_value('fingers_wanted_translation_file')
        fingers_wanted_substitution_file = get_configuration_value('fingers_wanted_substitution_file')
        print('Set wanted number of fingers (default for data, translation, substitution: ', fingers_wanted_our_data, fingers_wanted_translation_file, fingers_wanted_substitution_file, ')')
        print('A bigger number equals your data will be (tried to be) shared more')
        print('which makes your computer share more data too.')
        print('A smaller number, less.')
        print('example (our data only):')
        print(executable + ' -s dataname 20')
        print('You can also change the fingers of the translation and substitution files')
        print('higher values are useful for long lasting links in environments where addresses change often')
        print('or zero if you only want to share the data from your computer only.')
        print(executable + ' -s dataname 10 40 5')
    if specific_help_type == '--set_default_fingers':
        fingers_wanted_our_data = get_configuration_value('fingers_wanted_our_data')
        fingers_wanted_translation_file = get_configuration_value('fingers_wanted_translation_file')
        fingers_wanted_substitution_file = get_configuration_value('fingers_wanted_substitution_file')
        print('Set wanted number of default fingers')
        print('Default wanted fingers for data:', fingers_wanted_our_data)
        print('Default wanted fingers for translation files:', fingers_wanted_translation_file)
        print('Default wanted fingers for substitution files:', fingers_wanted_substitution_file)
        print('If the second or third value are set to zero, then link decay will occur over time.')

        ####translation, substitution: ', , , ')')

    if specific_help_type == '-d':
        print('Delete data')
        print('example:')
        print('Delete data:')
        print(executable + ' -d dataname')
    if specific_help_type == '-x':
        print('Tries to upload the data (e.g generate new fingers)')
        print('This is done by asking for sharing other nodes data in exchange.')
        print('example to try ten times (default value) to get new fingers:')
        print(executable + ' -x dataname')
        print('example to try a specific number of times to get new fingers:')
        print(executable + ' -x dataname 20')
        print('Note: the process is random but will not generate more fingers than specified, see -s and -la')
    if specific_help_type == '-p':
        print('Show or update configuration:')
        print('Show actual configuration:')
        print(executable + ' -p')
        print('Update configuration, set port value to 20000:')
        print(executable + ' -p port 20000')
    if specific_help_type == '-ip':
        print('Try to figure out your public IP address:')
        print('Example:')
        print(executable + ' -ip')
        print('Result:')
        print('123.45.67.89')
        print('Hint:')
        print('Set your public ip address like this:')
        print(executable + ' -p ip 123.45.67.89')
    if specific_help_type == '-t' or specific_help_type == '--test':
        print('Check the listener is up and running and connected to the internet')
        print('over our ip:port configuration.')
        print('Example:')
        print(executable + ' -t')
    sys.exit(0)


def print_help():
    print('Tenfingers usage:')
    print('')
    print(executable + ' [-i][-f][-u][-n][-l] name [foldername] [filename][regex]')
    print(executable + ' [-t] linkfile [destfolder]')
    print('')
    print('-i [-f] [-x] = insert data or overwrites existing data (as opposed to download)')
    print('-d name = delete your shared data')
    print('-f = use a folder, not a file')
    print('-c = create a link')
    print('-n = no error if linkfile already exists')
    print('-l [name] = list information')
    print('-s name number_of_fingers_wanted = set how many times you want to exchange shares for this data')
    print('-x name [N] = try to share this data N times (default 10)')
    print('-p [name + value] show or update congfiguration')
    print('-ip try to figure out your public IP address')
    print('-h [-i,-t,-f,-r,-n,-c,-s,-x,-d] specific help')
    print('')
    print('Download data with a link, say link.10f')
    print('./10f link.10f')


if len(sys.argv) == 1:
    print_help()
    sys.exit(0)


cmd = {}
commands = {'-h', '-i', '-f', '-u', '-n', '-c', '-l', '-la', '-s', '-x', '-d', '-p', '-ip', '-t', '--test', '-silent', '-a', '-v',
            '--version', '--set_default_fingers', '-r'}
specific_help = False
specific_help_type = False

# Specific help?  e.g. 10f -h -X  <- where X is the command we want to know more about
if len(sys.argv) == 3 and sys.argv[1] == '-h':
    print('Specific Help:')

# Redo the argv list, just removing any commands and keeping the values, e.g remove those starting with a minus
value = {}
values = 0
for i in range(1, len(sys.argv)):  # Start at 1 to skip the script name
    a = sys.argv[i]
    if a[:1] != '-':  # Not a Command
        value[values] = a
        values += 1

for c in sys.argv:
    if c[:1] == '-':
        if specific_help and not specific_help_type:
            specific_help_type = c

        if c == '-h':  # Specific help:
            specific_help = True
        if c in commands:
            cmd[c] = True
        else:
            print('Unknown command:', c)
            print_help()

if specific_help:
    if specific_help_type == False:
        print_help()
        sys.exit(0)
    else:
        print_specific_help(specific_help_type)
        sys.exit(0)

noerror = '-n' in cmd

if '-v' in cmd or '--version' in cmd:
    from version import get_tenfingers_version
    print('Tenfingers sharing protocol version', get_tenfingers_version())

if '-i' in cmd:
    # executable + ' -i [-x] dataname filename [x-tries]'
    # executable + ' -i -f [-x] dataname folder_to_add [regex] [x-tries]'

    if values < 2:
        print('Error: Insufficient data.')
        print('')
        print_specific_help('-i')

    #for i in range(0, values):
    #    print('Input [' + str(i) + '] =', value[i])

    name = value[0]
    data_to_add = value[1]  # filename for file, regex for folders

    # Workaround for when we launch 10f from a folder that is not the 10f folder
    # Check if we are launching this from another folder:
    source_folder = os.path.normpath(locator.module_path())
    launch_folder = os.path.normpath(os.getcwd())
    if source_folder != launch_folder:
        # Get the folders before we switch to the executables (or python files) folder
        #folder = os.path.join(os.getcwd().replace(".", ""), value[1])
        folder = os.getcwd().replace(".", "") + "/"
        #print("Launched from other folder:", folder)
        #print("Change directory to where 10f.py is:", locator.module_path() + "/")

        # witch to the executables (or python files) folder
        os.chdir(locator.module_path() + "/")
    else:
        folder = "./"  #value[1]

    if data_to_add[:2] == './':  # Remove it as we have the folder
        data_to_add = data_to_add[2:]

    # See if the user wants us to try to share the data instantly
    swicharoo_tries = 0
    if '-x' in cmd:
        swicharoo_tries = 10  # Default
        if '-f' in cmd:
            # Here we must use the regex, or we won't let user decide how many tries (just use the default ".*")
            if values == 3:
                swicharoo_tries = int(value[2])
            else:
                print('when using -f and -x, you must give regex and tries. You can use the default regex ".*", example:')
                print(' -f -x dataname foldername .* 20')
        else:
            if values >= 3:
                swicharoo_tries = int(value[2])
        print('TRIES:', swicharoo_tries)

        # todo: do the same for the trans file too

    from database_query import get_link_data_by_name

    if '-f' in cmd:  # variable length number of argv, regex is optional
        #print(" DEBUG  values=", values)
        # -i -f    dataname folder_to_add [regex]  # 3 to 4 value arguments (regex is optional)
        # -i -f -x dataname folder_to_add [regex] x-tries  # 4 to 5 value arguments (regex is optional)
        swicharoo_tries = 0
        base_folder = './'  #value[1]  # value does not contain the '-?' arguments
        if '-x' in cmd:
            if values == 3:  # no regex
                regex = '.*'
                swicharoo_tries = int(value[2])
            else:
                regex = value[2]
                swicharoo_tries = int(value[3])
        else:
            if values == 2:  # no regex
                regex = '.*'
            else:
                regex = value[2]

        #print('Insert folder:')

        ac.insert_folder(name, folder, data_to_add, regex, swicharoo_tries)
    else:
        #print('Insert file:', name, data_to_add, swicharoo_tries)
        ac.insert_data(name, folder, data_to_add, swicharoo_tries)

    # Check for the afar flag (your IP:PORT shall be excluded from links and _tr/_sub files)
    afar_flag = '-a' in cmd

    # Set the afar flag:
    set_afar_flag(name, afar_flag)

    ###############################################################

else:  # not an insert '-i' so maybe an '-a' update;
    if '-a' in cmd:  # 10f -a name [on/off/1/0]
        if len(sys.argv) == 4:
            name = sys.argv[2]
            on_arg = sys.argv[3]
            afar_flag = on_arg.lower() not in ['off', '0']
            if afar_flag:
                print("Activate afar flag on data [" + str(name) + "]")
            else:
                print("Deactivate afar flag on data [" + str(name) + "]")
            set_afar_flag(name, afar_flag)
        else:
            print_specific_help('-a')

if '-c' in cmd:
    if len(sys.argv) == 3 or len(sys.argv) == 4:
        exclude_our_own_address = '-silent' in cmd
        if exclude_our_own_address:
            name = sys.argv[3]
            print('Create silent link for  DOES NOT YET WORK', name)
        else:
            name = sys.argv[2]
            print('Create link for', name)

        from get_link_from_data import create_and_save_link_database
        create_and_save_link_database(name)
    else:
        print_specific_help('-c')


if '-l' in cmd:
    # Cheap fix for when you launch this from another folder: (the .aes files are not found otherwise)
    os.chdir(locator.module_path() + "/")

    if len(sys.argv) == 2:
        dq.flush_owned_data(True)
    else:
        print_specific_help('-l')



if '-la' in cmd:
    # Cheap fix for when you launch this from another folder: (the .aes files are not found otherwise)
    os.chdir(locator.module_path() + "/")

    print('--------------------')
    if len(sys.argv) == 3:
        print('Show information for the data ['+sys.argv[2]+']')
        # Show the fingers for this entry:
        data_owned_id = dq.get_link_data_id_by_name(sys.argv[2])
        data_version = dq.get_highest_version(data_owned_id)
        print('Actual data version:', data_version)
        dq.flush_fingers(sys.argv[2])
    else:
        print('Show general information:')
        print('')
        # Flush generic data about nodes, data etc.
        our_data_size = get_owned_files_size()
        shared_data_size = get_shared_files_size()
        max_storage_for_other = (get_configuration_value('max_storage_space_other_mb')*1024*1024)

        precision = 100  # so percentage will have some decimals
        part_occupied_sharing_for_others = int((precision*shared_data_size / max_storage_for_other) + 0.5) / precision

        print('# Our Owned data: ', human_readable_file_size(our_data_size))
        dq.flush_owned_data()

        print('')
        print('# Shared data:')
        print('We share:', dq.get_nof_shared_data(), 'files. We use', human_readable_file_size(shared_data_size) + ' of max ' +
              human_readable_file_size(get_configuration_value('max_storage_space_other_mb')*1024*1024) +
              ' [' + str(part_occupied_sharing_for_others) + '%]')


        print('')
        print('# Known nodes:')
        dq.flush_known_nodes()

    print('--------------------')


if '--set_default_fingers' in cmd:  # 10f -s nof_fingers [not_tr_fingers, nof_sub_fingers]
    updated = False
    if len(sys.argv) == 3 or len(sys.argv) == 5:
        nof_fingers = int(sys.argv[2])
        if nof_fingers is not None and nof_fingers >= 0:
            set_int_configuration_value('fingers_wanted_our_data', nof_fingers)
            updated = True

        if len(sys.argv) == 5:  # Set the number of fingers for tr and sub files too
            nof_fingers_tr = int(sys.argv[3])
            nof_fingers_sub = int(sys.argv[4])
            if nof_fingers_tr is not None and nof_fingers_sub is not None and nof_fingers_tr >= 0 and nof_fingers_sub >= 0:
                set_int_configuration_value('fingers_wanted_translation_file', nof_fingers_tr)
                set_int_configuration_value('fingers_wanted_substitution_file', nof_fingers_sub)
                updated = True

    if updated:
        fingers_wanted_our_data = get_configuration_value('fingers_wanted_our_data')
        fingers_wanted_translation_file = get_configuration_value('fingers_wanted_translation_file')
        fingers_wanted_substitution_file = get_configuration_value('fingers_wanted_substitution_file')
        print(f'Updated to {fingers_wanted_our_data} {fingers_wanted_translation_file} {fingers_wanted_substitution_file}')
    else:
        print('Command not understood.')
        print_specific_help('--set_default_fingers')


if '-s' in cmd:  # 10f -s name nof_fingers [not_tr_fingers, nof_sub_fingers]
    # todo: remove the fingers if we no longer need them? Or just let them decay?
    updated = False
    if len(sys.argv) == 4 or len(sys.argv) == 6:
        name = sys.argv[2]
        data_owned_id = get_link_data_id_by_name(name)
        if data_owned_id:  # Check the data exists at all

            id_basename = get_translation_file_type(data_owned_id) == 0

            if id_basename or len(sys.argv) == 4:
                nof_fingers = int(sys.argv[3])
                if nof_fingers is not None and nof_fingers >= 0:
                    set_wanted_fingers(data_owned_id, nof_fingers)
                    updated = True

                if len(sys.argv) == 6:  # Set the number of fingers for tr and sub files too
                    nof_fingers_tr = int(sys.argv[4])
                    nof_fingers_sub = int(sys.argv[5])
                    if nof_fingers_tr is not None and nof_fingers_sub is not None and nof_fingers_tr >= 0 and nof_fingers_sub >= 0:
                        data_owned_id_tr = get_link_data_id_by_name(fp.generate_trans_name(name))
                        data_owned_id_sub = get_link_data_id_by_name(fp.generate_sub_name(name))
                        if data_owned_id_tr is not None and data_owned_id_sub is not None:
                            set_wanted_fingers(data_owned_id_tr, nof_fingers_tr)
                            set_wanted_fingers(data_owned_id_sub, nof_fingers_sub)
                            updated = True
            else:
                print('You cannot update a translation file with several numbers of wanted fingers.')

    if updated:
        print('Updated.')
    else:
        print('Command not understood.')
        print_specific_help('-s')


if '-x' in cmd:  # 10f -x name (tries=10)
    nof_tries = 10
    if len(sys.argv) == 4:
        nof_tries = int(sys.argv[3])

    if len(sys.argv) == 3 or len(sys.argv) == 4:
        name = sys.argv[2]
        from database_query import get_link_data_id_by_name
        from scheduler import check_data_finger
        data_owned_id = get_link_data_id_by_name(name)
        check_data_finger(data_owned_id, nof_tries, True)
    else:
        print_specific_help('-f')

if '-d' in cmd:  # 10f -d [-f] name
    name = None
    if len(sys.argv) == 3:
        name = sys.argv[2]
        if name is not None:
            import access as ac
            ac.delete_data(name)
            # print('Done.')
        else:
            print_specific_help('-d')
    else:
        print_specific_help('-d')


# Force update the data
if '-u' in cmd:
    name = None
    if len(sys.argv) == 3:
        name = sys.argv[2]
        data_owned_id = dq.get_link_data_id_by_name(name)
        if data_owned_id:  # Check the data exists at all
            dq.set_next_data_version(data_owned_id)
            print('Data flagged for update.')
        else:
            print(f'Data {name} not found.')
    else:
        print('Command not understood.')
        print_specific_help('-u')

if '-p' in cmd:
    string_values = ['ip', 'unsecure_nick_name', 'swicharoo_policy', 'exclude_ip_list', 'data_owned_folder', 'data_owned_translation_folder', 'data_shared_folder']
    integer_values = ['port', 'listen_read_bytes', 'max_simultaneous_connections', 'listen_timeout_seconds', 'server_send_package_size',
                      'max_storage_space_other_mb', 'swicharoo_minsize_accept_all', 'swicharoo_file_maxsize_kb', 'scheduler_wait_msec',
                      '_unsafe_small_rsa_keys']

    import database_connection as dc

    if len(sys.argv) == 4:
        name = sys.argv[2]
        value = sys.argv[3]

        print('Update configuration, set: ' + name + ' = ' + value)

        if name in string_values:
            dc.update("UPDATE configuration_string SET value = ? WHERE name = ?", (value, name))
        else:
            if name in integer_values:
                dc.update("UPDATE configuration_int SET value = ? WHERE name = ?", (value, name))
            else:
                print('The value does not exist in the database, use -p to display values.')

    else:
        # Flush the config tables:
        print('------------------------------')
        print('Tenfingers configuration:')
        padding = 29
        for name in string_values:
            value = dc.query_one("SELECT value FROM configuration_string WHERE name = ?", (name,))
            print(name + ' '*(padding - len(name)) + ': "' + str(value) + '"')
        print('')
        for name in integer_values:
            value = dc.query_one("SELECT value FROM configuration_int WHERE name = ?", (name,))
            print(name + ' '*(padding - len(name)) + ': ' + str(value))
        print('------------------------------')


if '-ip' in cmd:
    print('Trying to figure out the public ip address of this machine:', flush=True)
    from helpers import get_public_ip
    ip = get_public_ip()

    if ip != '127.0.0.1':
        print('Your IP address should be:', ip)
        print('Hint:')
        print('Set your public ip address like this:')
        print(executable + ' -p ip', ip)
    else:
        print('There were errors...')


if '-t' in cmd or '--test' in cmd:
    print('Trying to connect to the public ip:port address of this machine.', flush=True)
    from socket_communication import SecureCommunication
    import database_query as dq

    try:
        from Cryptodome.PublicKey import RSA
    except ImportError:
        print("Error: You need to install Cryptodome:\npip3 install pycryptodomex\n")
        exit(-7)

    my_port = get_configuration_value('port')
    my_ip = get_configuration_value('ip')
    _, my_rsa_pub_hex_key = dq.get_rsa_hex_keys()
    my_rsa_pub_key = RSA.importKey(bytes.fromhex(my_rsa_pub_hex_key))

    sc = SecureCommunication(my_ip, my_port, my_rsa_pub_key)
    if sc.is_active():
        print('Success. The listener is running and is accessible on the IP:PORT address ' + my_ip + ':' + str(my_port))
    else:
        print('Connection to ' + my_ip + ':' + str(my_port) + ' failed...')
        print('Your public ip:port must be routed to this PC,')
        print('and for the test to succeed, the listener must be running.')


#
# Download data from a link
if len(cmd) == 0 or '-r' in cmd:
    if len(sys.argv) >= 2:
        skip_tr_sub_files = '-r' in cmd  # Can be used for faster download when link decay is not needed to be taken into account
        name = sys.argv[2] if skip_tr_sub_files else sys.argv[1]
        # Workaround for when we launch 10f from a folder that is not the 10f folder
        # Check if we are launching this from another folder:
        source_folder = os.path.normpath(locator.module_path())
        launch_folder = os.path.normpath(os.getcwd())
        # print("src:", source_folder)
        # print("me:", launch_folder)
        if launch_folder != source_folder:
            # Also change the input files folder
            link_file = os.path.join(os.getcwd().replace(".", ""), name)
            os.chdir(locator.module_path() + "/")
        else:
            link_file = name

        # Download the data
        ac.download(link_file, launch_folder, skip_tr_sub_files)
