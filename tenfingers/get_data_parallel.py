# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import os
import sys
import sqlite3
from sqlite3 import Error
import math
import binascii
import json
import time

# WORK IN PROGRESS

"""
WARNING:
When it works it should be blazing fast, but beware, this is not a fully secure way of downloadig:

1) Any node can spoof your download with a wrong (e.g too high) version to gain full control of what to feed the download
2) Any node can send garbage to break the download (data, file size...)

However, no node can spoof the data itself, it "just won't work"

"""


# This class inserts itself in the database, in the constructor
# This class will read and write in the database
# One instance is a download for a chunk from a specific address
class Slot:
    def __init__(self, database_file, version, address, start_byte, end_byte):
        self.database_file = database_file
        self.downloaders = 0




def create_slot_grid(dbfile, version, addresses, chunks):
    pass
    # Generates this in the db
    # v=1
    # ___c1 c2 c3 c4
    # a1 [ ][ ][ ][ ]
    # a2 [ ][ ][ ][ ]
    # a3 [ ][ ][ ][ ]

# table slot:
#  INTEGER v
#  rsa_pub,ip,port address
#  INTEGER start_byte
#  INTEGER end_byte


"""

Download structure with example:

d.10f = data to download, made up by 4 chunks c1-c4
Addresses: a1, a2, a3

Downloading a specific version

v = 1
___c1 c2 c3 c4
a1 [ ][ ][ ][ ]
a2 [ ][ ][ ][ ]
a3 [ ][ ][ ][ ]

Every slot [ ] has these fields:
 downloaders = DEFAULT 0
 done = F
 

Launch X threads:
Parallel normal: X<= number of addresses
Parallel force: X > number of addresses (so contact a node in parallel too)

For each thread:
Get one empty slot, or if there are no get one with done=F
 => say v1_a1,c1
add 1 to downloaders
Go get the data
 If we get a higher version, say 2, set up a new slot-grid with v=2
Save it off and set done=T


"""


"""
The data to download is loaded in chunks, so first of all we'll need to know the size of the data
This can vary over time, so we'll need to checkwhich version each node has, and restart if it's newer.
Itchy problem: how to prevent version poisoning? E.g an evil node saying it has version 999 and feeds us garbage...

For now:
Split up the data to load in N chunks and populate a small database with one entry each:
chunks: id, start_byte, size (bytes to download), state [IDLE, DOWNLOADING, DONE]

We'll use the spawned threads to try to download any "IDLE", then any "DOWNLOADING" until all are "DONE"

"""

"""
Workflow:

Before we decide to restart the whole download thing:
Threads: N = 8
chunk_size = 100.000 Bytes

1 Make a database with general information (addresses, keys, chunks, ...) + version = 1
2 /Make a Version_1/ folder, where we'll work (if the version is 1)
3 Thread out N threads
4 Wait until all chunks are 'DONE'
5 Merge the files into one
6 do as usual, (e.g untar, uncrypt, copy around ... See get_data_from_link.py which already does this perfectly well)
7 Clean up!

Threads:
1 Get and store local_version from db.version
2 Gelect a semi-random chunk ('IDLE', then 'DOWNLOADING')
3 Check if local_version equals db.version, else go to step 1
4 If no chunk left, assume they are all 'DONE'and bail out
5 Select a random node, if we can't connect, go to 4 (Later: flag it as bad or just select less often)
6 Connect and ask for (link_name&rsa_pub_key) -> version
6 Check node_version vs local_version:
  node_version < local_version -> go to step 4 (Later: flag it as too oldso we won't choose it in step 5)
  node_version > local_version:
        Update db.version
        set all chunks to 'IDLE'
        go to step 1 (ha ha goto)
  node_version == local_version:
    set chunk to 'DOWNLOADING'
    Download the chunk & save it in version_*local_version*/chunk_id.data
    set chunk to 'DONE'
    go to step 1


Any error should make a go to step 4

Version change invalidates the state of all chunks.

Reflexion: if we add (latest known) version to link, we might miss it all, instead of loading an older version


"""



class Download():

    # Downloads a file from a link, uses multiple threads, combines it all in the end
    # if reload is set to True, then we will perform a clean new download
    def __init__(self, link_file, dest_folder, reload=False):
        link_file = link_file.replace('\\', '/')
        dest_folder = dest_folder.replace('\\', '/')

        # Get the data out of the link file
        with open(link_file) as json_data:
            print('get json')
            d = json.load(json_data)
            # todo: save off addresses for later use in swicharoo etc:
            # if 'address_list' in d:
            #    save_off_addresses(d['address_list'])

            # Start the actual downloading
            if not 'address_list' in d or not 'key' in d or not 'iv' in d or not 'data_rsa_pub' in d:
                print("#Error: Missing information in link file, aborting...")
                os.exit(-1)

            self.address_list = d['address_list']

            # this is the key,iv for the encrypted file we want to retrieve
            self.key = bytes.fromhex(d['key'])
            self.iv = bytes.fromhex(d['iv'])
            self.data_rsa_pub = d['data_rsa_pub']

        # Setup
        self.temp_folder = link_file + '_download/'
        if not os.path.exists(self.temp_folder):
            os.makedirs(self.temp_folder)
        self.link_file = link_file
        self.database_name = './' + self.temp_folder + link_file + '_wip.db'

        if reload and os.path.isfile(self.database_name):  # Wipe the database
            os.remove(self.database_name)

        # generate, or regenerate database if needed
        if not os.path.isfile(self.database_name):
            connection = self.get_connection()
            connection.execute('''CREATE TABLE chunks
                                  (id INTEGER PRIMARY KEY AUTOINCREMENT,
                                   start_byte              INTEGER,
                                   size                    INTEGER,
                                   state                   TEXT NULL DEFAULT "IDLE"
                                  );''')

            # Here we share data between threads so one entry only. -1 means it is not initialized yet
            connection.execute('''CREATE TABLE data
                                  (file_size               INTEGER DEFAULT -1,
                                   file_version            INTEGER DEFAULT -1
                                  );''')

            cursor = connection.cursor()
            cursor.execute('INSERT INTO data (file_size, file_version) VALUES(?, ?)', (-1, -1,))
            connection.commit()

        self.insert_chunk(0, 1024)
        self.insert_chunk(1024, 76)

        # Fork off:
        forks = 10
        for i in range(0, forks):
            new_pid = os.fork()
            if new_pid == 0:
                self.fork_main(i)

        time.sleep(8)


    def fork_main(self, nr):
        print("A fork:", nr)
        time.sleep(0.5)

    def query_one(self, query, extra_data=None):
        connection = self.get_connection()
        cursor = connection.cursor()
        if extra_data is None:
            cursor.execute(query)
        else:
            cursor.execute(query, extra_data)
        row = cursor.fetchone()
        if row is None:
            return None

        if len(row) == 1:
            return row[0]
        return None


    def insert_chunk(self, start_byte, size):
        connection = self.get_connection()
        cursor = connection.cursor()
        cursor.execute('INSERT INTO chunks (start_byte, size) VALUES(?, ?)', (start_byte, size, ))
        connection.commit()

    def get_random_chunk(self):
        # Get an IDLE one first, or a DOWNLOADING one if there are no more IDLE. Returns None if no left
        result = self.query_one('SELECT id FROM chunks WHERE state="IDLE" ORDER BY RANDOM() LIMIT 1')
        if result is not None:
            return result
        return self.query_one('SELECT id FROM chunks WHERE state="DOWNLOADING" ORDER BY RANDOM() LIMIT 1')

    def get_connection(self):
        try:
            connection = sqlite3.connect(self.database_name)
            return connection
        except Error:
            print(Error)
        sys.exit(-3)

    # returns the number of chunks left to download or zero if we are done downloading
    def download_chunks_left(self, database_name):
        # Open database

        # Check how many chunks that are not finished
        return 1

    def download_chunk(self, database_name, name, data_rsa_pub, chunk_size):
        # Get a random chunk to download

        # Grab a random node

        # Try to download chunk

        # During download, check if DB either says we're done, or if it doesn't exist anymore, if so just close socket and bail out, we're done

        # If success, save it off and tick the DB entry as 'DONE', else just close socket and return
        pass


    def get_data_parallel(self, address_list, name, data_rsa_pub):  # Note: You do the decrypting!
        # todo: if the version wee get from a node is > our versionm then redo everything with this new version!

        # Get the version + filesize

        # Make a database with entries for N chunks
        chunk_size = 1024 * 1024  # todo: make it configurable
        max_chunks = 100  # this might override chunksize

        # todo: do this
        version = 1
        file_size = 123456

        if chunk_size * max_chunks < file_size:
            chunk_size = int(math.ceil(file_size / max_chunks))

        chunks = int(math.ceil(file_size/chunk_size))
        print("Preparing " + str(chunks) + " chunks for downloading")


        # DB: id, chunkstatus = { 'NOT_STARTED', 'DOWNLOADING', 'DONE'  }


        # For now just do it single threaded until it works:

        while(self.download_chunks_left(self.database_name) > 0):
            self.download_chunk(name, data_rsa_pub, chunk_size)


download = Download('./linuxmusic1.10f', './', True)