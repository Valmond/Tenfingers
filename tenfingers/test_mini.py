import sys,os
import subprocess
import signal
import time

print('Start a killable listener')

if len(sys.argv) != 2:
    print('#error, fulltest.py takes a python path as entry.')
    exit(-100)

python_path = sys.argv[1]

print('Start the listener:')

# Start the listener
proc = subprocess.Popen(python_path + ' listener.py', shell=False)
time.sleep(1.0)

print('Run the tests suite')

# run the tests
print('Setup:')
proc0 = subprocess.call(python_path + ' setup.py --testrun', shell=False, cwd=r'./')
time.sleep(0.2)
print('Generate Database:')
proc1 = subprocess.call(python_path + ' generate_database.py --testrun', shell=False, cwd=r'./')
time.sleep(0.2)
print('Insert Data:')
proc2 = subprocess.call(python_path + ' insert_data.py --testrun', shell=False, cwd=r'./')
time.sleep(0.2)
print('Extract Link for inserted data::')
proc3 = subprocess.call(python_path + ' get_link_from_data.py --testrun', shell=False, cwd=r'./')
time.sleep(0.2)
print('Get data from extracted link:')
proc4 = subprocess.call(python_path + ' get_data_from_link.py --testrun', shell=False, cwd=r'./')
time.sleep(0.2)




# Clean up

proc.kill()
