# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import os.path
import sys
import sqlite3
from sqlite3 import Error

import locator


def get_database_folder():
    return "db/"


def get_database_file_path():
    return os.path.join(locator.module_path(),  get_database_folder(), 'tenfingers.db')


def database_exists():
    return os.path.isfile(get_database_file_path())


# If database_file_path is None, 'tenfingers.db' will be used
# Refact all this into a db class one day
def get_connection(check_if_existing=True, database_file_path=None):

    if database_file_path is None:
        database_file_path = get_database_file_path()
        if check_if_existing and not database_exists():
            print('The database does not exist. Try using generate_database.py')
            sys.exit(-2)
    else:
        database_file_path = os.path.join(locator.module_path(),  database_file_path)
    try:
        connection = sqlite3.connect(database_file_path)
        return connection
    except Error:
        print(Error, flush=True)

    # Something went wrong
    #sys.exit(-3)  LOG IT INSTEAD OF BAILING OUT (Scheduler needs to continue to run...)


# NOTE! Returns the cursor, you do the fetch, connection.commit(), ...
def execute(query, extra_data=None):
    connection = get_connection()
    cursor = connection.cursor()
    if extra_data is None:
        cursor.execute(query)
    else:
        cursor.execute(query, extra_data)
    return cursor


def update(query, extra_data=None):
    connection = get_connection()
    cursor = connection.cursor()
    if extra_data is None:
        cursor.execute(query)
    else:
        cursor.execute(query, extra_data)
    connection.commit()


# use this to insert data
def insert(query, extra_data=None):
    connection = get_connection()
    cursor = connection.cursor()
    if extra_data is None:
        cursor.execute(query)
    else:
        cursor.execute(query, extra_data)
    connection.commit()


# returns the result after a fetch one
def query_one(query, extra_data=None):
    connection = get_connection()
    cursor = connection.cursor()
    if extra_data is None:
        cursor.execute(query)
    else:
        cursor.execute(query, extra_data)
    row = cursor.fetchone()
    if row is None:
        return None

    if len(row) == 1:
        return row[0]
    return None


# returns the result after a fetch all
def query_all(query, extra_data=None):
    connection = get_connection()
    cursor = connection.cursor()
    if extra_data is None:
        cursor.execute(query)
    else:
        cursor.execute(query, extra_data)
    return cursor.fetchall()




