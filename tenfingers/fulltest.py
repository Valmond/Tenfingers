import sys,os
import subprocess
import signal
import time

if len(sys.argv) != 2:
    print('#error, fulltest.py takes a python path as entry.')
    exit(-100)

python_path = sys.argv[1]

print('Start the multi node test:')

if python_path == 'python3':  # Linux
    subprocess.call(['python3', './N1/setup.py'])
    subprocess.call(['python3', './N2/setup.py'])
    # Start the listeners
    proc1 = subprocess.Popen(['python3', './listener.py'], shell=False, cwd=r'./N1')
    proc2 = subprocess.Popen(['python3', './listener.py'], shell=False, cwd=r'./N2')

else:
    subprocess.Popen(python_path , ' ./setup.py', shell=False, cwd=r'./N1')
    subprocess.Popen(python_path , ' ./setup.py', shell=False, cwd=r'./N2')

    # Start the listeners
    proc1 = subprocess.Popen(python_path + ' ./listener.py', shell=False, cwd=r'./N1')
    proc2 = subprocess.Popen(python_path + ' ./listener.py', shell=False, cwd=r'./N2')

print('---Start the testing: (WIP!)')

# Do the testing

# Test links:
# insert data into N1
# insert data into N2
# Get Link1 from N1
# Get Link2 from N2

# Get data from link1
# Get data from link2

# Test Swicharoo:
# Later: ask swicharoo Data1 -> N2
# Delete linkdata in N1
# Get data from link1 (so from N2)

time.sleep(2.0)







# Clean up

proc1.kill()
proc2.kill()
