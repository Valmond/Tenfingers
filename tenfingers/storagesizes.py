# Copyright (C) 2025 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import os
from configuration import get_configuration_value


def get_folder_size(folder):
    total_size = 0
    for dir_path, dir_names, filenames in os.walk(folder):
        for f in filenames:
            fp = os.path.join(dir_path, f)
            # skip if it is symbolic link or if it is our data
            if not os.path.islink(fp):
                if os.path.exists(fp):
                    total_size += os.path.getsize(fp)
    return total_size


#  Size of shared files
def get_shared_files_size():
    return get_folder_size(get_configuration_value("data_shared_folder"))


# Size of owned files + translation files
def get_owned_files_size():
    return get_folder_size(get_configuration_value("data_owned_folder")) + get_folder_size(get_configuration_value("data_owned_translation_folder"))
