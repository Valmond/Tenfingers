# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

# Sort of interface file
# Use this when you want to use the basic functionalities in the tenfingers protocol
# underscore = private functions, use if you know what you are doing

import os

import address_registry as ar
import database_query as dq
import locator

from database_query import get_link_data_id_by_name
from scheduler import check_data_finger
from insert_data import data_exists
from get_data_from_link import get_data_from_database_link, get_link_file_version
from database_query import get_link_data_by_name
from configuration import get_configuration_value
import filepath as fp


def _generate_tr_sub_files(data_name):
    from insert_data import insert_file
    ###############################################################
    # Generate the RSA-address translation and substitute files:
    # Make the file (an empty database file) and add our address to it, except if we want to be silent
    print("Make the address register translation and substitution files:", flush=True)
    ar.create_address_registry_files(data_name)

    # Set the addresses for 'name' in the _tr and_sub files
    ar.set_addresses(data_name, True)
    ar.set_addresses(data_name, False)

    # Todo: when exporting the translate file (-c), remove our address if the -silent keyword is used
    # Insert them into tenfingers.db too:
    trans_folder = get_configuration_value("data_owned_translation_folder")
    insert_file(fp.generate_trans_name(data_name), trans_folder, fp.generate_trans_file_name(data_name), 1)
    insert_file(fp.generate_sub_name(data_name), trans_folder, fp.generate_sub_file_name(data_name), 2)


def get_fingers(data_name, tries):
    if tries > 0:
        data_owned_id = get_link_data_id_by_name(data_name)
        check_data_finger(data_owned_id, tries, True)


# Fails silently if they already exists
def _create_tr_sub_files(data_name, base_folder, tries):
    tr_file_name = fp.generate_trans_name(data_name)
    sub_file_name = fp.generate_sub_name(data_name)
    if not data_exists(tr_file_name):  # If _tr not in the tenfingers.db database, create _tr & _sub files (and address translation files)
        _generate_tr_sub_files(data_name)


# get_finger_tries is how many attempt will be made to share the data.
# This is done right now and has nothing to do with the number of fingers wanted.
def insert_folder(data_name, base_folder, folder_to_add='./', regex='.*', get_finger_tries=0, silent=False):
    from insert_data import insert_folder
    insert_folder(data_name, base_folder, folder_to_add, regex, silent)

    # Avoid going further if the folder didn't exist for inserting
    if get_link_data_by_name(data_name):
        _create_tr_sub_files(data_name, base_folder, get_finger_tries)
        get_fingers(data_name, get_finger_tries)


def insert_data(data_name, folder, file_name, tries=0):
    from insert_data import insert_file
    insert_file(data_name, folder, file_name)
    # Avoid going further if the file didn't exist for inserting
    if get_link_data_by_name(data_name):
        _create_tr_sub_files(data_name, folder, tries)
        get_fingers(data_name, tries)


# Deletes only this specific data
def _delete_one_data(name, translation_folder):
    dq.delete_fingers(name)

    if dq.delete_owned_data(name):
        if translation_folder:
            # Remove the original translation file too
            owned_file_name = os.path.join(locator.module_path(), get_configuration_value("data_owned_translation_folder"), name + fp.get_address_translation_file_extension())
            os.remove(owned_file_name)

        owned_file_name = fp.generate_aes_filepath(name)
        os.remove(owned_file_name)


# Deletes this data plus its tr/sub files too
# folder: either  or 'trans' for translation files
def delete_data(name):
    _delete_one_data(name, False)
    _delete_one_data(fp.generate_trans_name(name), True)
    _delete_one_data(fp.generate_sub_name(name), True)


def _download_translation_files(link_file):
    # Unix style
    link_file = link_file.replace('\\', '/')

    tr_base = fp.generate_trans_name(link_file[:-4])
    sub_base = fp.generate_sub_name(link_file[:-4])
    translation_file = tr_base + ".10f"
    substitution_file = sub_base + ".10f"

    translation_file_result = fp.generate_trans_file_name(link_file[:-4])
    substitution_file_result = fp.generate_sub_file_name(link_file[:-4])

    base_folder = os.path.dirname(os.path.abspath(link_file)).replace('\\', '/')

    # Note:
    # We have a snake-bites-tail event here, so if _sub is newer, we should re--run it for its addresses to download the _tr again
    # as it might have a new version (user workaround: download link twice...)
    # We should just download them both until they are up-to-date with their latest version
    # before we get the base payloads addresses out of them.

    # Loop downloading tr/sub files until there is no better version to get:
    downloaded_trans_version = -1
    downloaded_sub_version = -1
    # while they change at download, continue...
    # Continue trying at Any change (non-existing -> existing / version change)

    changes_detected = True
    while changes_detected:
        changes_detected = False

        # Update the translation file, if it exists
        if os.path.exists(translation_file):
            # print("Try to download the translation file " + str(translation_file_result) + " by using the link file " + str(translation_file))
            # Todo: use the translation_file_result's addresses here (too)
            get_data_from_database_link(translation_file, base_folder, base_addresses=False,
                                        substitution_file_result=substitution_file_result)
            # Check version, and if it has changed
            version = get_link_file_version(translation_file)
            if version > downloaded_trans_version:
                # print("Translation file downloaded version " + str(version), flush=True)
                downloaded_trans_version = version
                changes_detected = True
        else:
            print("Translation file not found:", translation_file)

        # Update the substitution file, if it exists
        if os.path.exists(substitution_file):
            # print("Try to download the substitution file " + str(substitution_file_result) + " by using the link file " + str(substitution_file))
            # Todo: use the substitution_file_result's addresses here (too)
            get_data_from_database_link(substitution_file, base_folder, base_addresses=False,
                                        translation_file_result=translation_file_result)
            # Check version, and if it has changed
            version = get_link_file_version(substitution_file)
            if version > downloaded_sub_version:
                # print("Substitution file downloaded version " + str(version), flush=True)
                downloaded_sub_version = version
                changes_detected = True
        else:
            print("Substitution file not found:", substitution_file)

    # update the addresses in link_sub.10f with the other_tr_sub_address in link_tr.atf
    ar.update_tr_sub_10f_addresses(substitution_file, translation_file_result)

    # update the addresses in link_tr.10f with the other_tr_sub_address in link_sub.atf
    ar.update_tr_sub_10f_addresses(translation_file, substitution_file_result)


def download(link_file, destination_folder='./', skip_tr_sub_files=False):
    # If the link doesn't have the '.10f' extension and the file does not exist, check if there is a name.10f
    if link_file[-4:] != '.10f' and not os.path.exists(link_file):
        if os.path.exists(link_file + '.10f'):
            link_file = link_file + '.10f'  # So we can call 10f with just the (short) name

    translation_file_result = None
    substitution_file_result = None
    if not skip_tr_sub_files:
        # Get the translation files; _tr & _sub so that we can use their addresses
        _download_translation_files(link_file)

        translation_file_result = fp.generate_trans_file_name(link_file[:-4])
        substitution_file_result = fp.generate_sub_file_name(link_file[:-4])

    # Download the data using the .10f file, the _tr.10f & _sub.10f results (the .atf files)
    get_data_from_database_link(link_file, destination_folder, base_addresses=True,
                                translation_file_result=translation_file_result,
                                substitution_file_result=substitution_file_result)


