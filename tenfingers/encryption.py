# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

try:
    from Cryptodome.Cipher import AES
    from Cryptodome import Random
    from Cryptodome.Util import Counter
except ImportError:
    print("Error 6: You need to install Cryptodome:\npip3 install pycryptodomex\n")
    exit(-7)

import math

# How many random bytes we'll add to AES when asked for
s_random_aes_bytes = 8
s_AES_BLOCK_SIZE = 256/8
s_authentication_data_size = 32  # SHA 256 ( RSA ( SHA 256 ( plaintext) ) )

# What gets tucked in front of the aes files data: [random nr][Auth data][encrypted data]
s_aes_file_header_size = s_random_aes_bytes + s_authentication_data_size


# Symmetric encryption/decryption for arbitrary length data
# Makes a random key if not specified
class CipherHelper:
    def __init__(self, aes_key=None, nonce=0):
        if aes_key:
            self.aes_key = aes_key
        else:
            # Make a new fresh AES key for this communication:
            key_bytes = 32
            self.aes_key = Random.new().read(key_bytes)

        self.nonce = int(nonce)  # This sdhould not be needed (the cast)

    def get_nonce(self):
        return self.nonce

    def move_nonce(self, data, randomize):
        size = len(data)
        if randomize:
            size += s_aes_file_header_size
        self.nonce += math.ceil(size/s_AES_BLOCK_SIZE)

    # randomize adds a 8byte random number so that .aes files from the same data won't be the same
    def encrypt(self, plaintext, randomize=False):
        # Create a new Counter object with IV = our never used nonce (for this key).

        ctr = Counter.new(AES.block_size * 8, initial_value=self.nonce)
        # Create AES-CTR cipher.
        aes = AES.new(self.aes_key, AES.MODE_CTR, counter=ctr)

        # Update the IV nonce as the encryption used up a bunch of nonces
        self.move_nonce(plaintext, randomize)

        # Encrypt and return IV and ciphertext.
        if randomize:
            rnd = Random.get_random_bytes(s_random_aes_bytes)
            authentication_data = b'!' * 32
            return aes.encrypt(rnd + authentication_data + plaintext)
        else:
            return aes.encrypt(plaintext)

    # randomize skips the first 8 bytes
    def decrypt(self, encrypted_message, randomized=False):
        # Initialize counter for decryption. iv should be the same as the output of encrypt().
        ctr = Counter.new(AES.block_size * 8, initial_value=self.nonce)
        # Create AES-CTR cipher.
        aes = AES.new(self.aes_key, AES.MODE_CTR, counter=ctr)

        # Update the IV nonce, so that if we're using it for secure communication, we can decrypt the next package
        # Both sender and receiver will use up nonces
        self.move_nonce(encrypted_message, randomized)

        # Decrypt
        if randomized:
            return aes.decrypt(encrypted_message)[s_aes_file_header_size:]
        else:
            return aes.decrypt(encrypted_message)
