# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import os
import io


import os.path
from tarfile import GNU_FORMAT  # To remove those pesky @PaxHeader infos in our tar.gz
import re
from contextlib import contextmanager
import tarfile
import hashlib
from Cryptodome.PublicKey import RSA

import database_connection as dc
import database_query as dq
from encryption import CipherHelper
import locator
import rsa_helper as rh
from helpers import generate_new_header, extract_digest_and_plaintext_from_data_with_header
from configuration import get_configuration_value


# This loads and encrypts data, inserts it into the table 'data_owned'
# stores encrypted data in folder '/data/local/' as it is our owned data.
# Later on, the scheduler will ask other nodes for swicharoo (e.g storing our data at their place, and their data at our place in /data/file number)


def data_exists(data_name):
    connection = dc.get_connection()
    cursor = connection.cursor()
    cursor.execute("SELECT key FROM data_owned WHERE NAME = '" + data_name + "' LIMIT 1")  # todo: secure this
    rows = cursor.fetchall()
    # Does a database entry exists?
    return len(rows) > 0


def chomp(x):
    if x.endswith("\r\n"):
        return x[:-2]
    if x.endswith("\n") or x.endswith("\r"):
        return x[:-1]
    return x


# Useful for comparing tar.gz files
# In memory sha256 checksum of a tar file
def get_tar_checksum(tar_path):
    with tarfile.open(tar_path, "r:gz") as tar:
        checksums = {}
        for member in tar.getmembers():
            if member.isfile():
                file_data = tar.extractfile(member).read()
                checksums[member.name] = hashlib.sha256(file_data).hexdigest()
    return checksums


# Useful for comparing tar.gz files
# In memory sha256 checksum of a tar file
def get_tar_checksum_from_memory(tar_data):
    print('get_tar_checksum_from_memory:', len(tar_data))
    io_bytes = io.BytesIO(tar_data)
    tar = tarfile.open(fileobj=io_bytes, mode='r')
    checksums = {}
    for member in tar.getmembers():
        if member.isfile():
            file_data = tar.extractfile(member).read()
            checksums[member.name] = hashlib.sha256(file_data).hexdigest()
    return checksums


# You check if it's a folder / tar.gz, if the filename is the same etc. Here we check if the DATA is the same
def same_file(new_data, aes_file_name, new_original_file_name, new_is_tar_gz_folder, aes_key, old_used_nonce, silent):
    try:  # If an .aes file has been corrupted, then here we'll have a throw, for example from network_independent_unpack_8bytes_to_size but not only
        # Very verbose print('same:file(', new_data[:25], aes_file_name, is_tar_gz_folder, aes_key, old_used_nonce, silent)
        # Now check if the data is new, otherwise just bail out
        new_data_size = len(new_data)
        #aes_file_size = os.path.getsize(aes_file_name)
        # very verbose print("new_data=", new_data_size, new_data)
        #print("aes_file_size=", aes_file_size)

        with open(aes_file_name, 'rb') as f:  # Possible optimisation for large files: just read and decrypt the header. The variable sized header...
            aes_file_data = f.read()
            if aes_file_data is not None:
                # Make a cipherhelper that will decrypt the old stored data:
                ch_tmp = CipherHelper(aes_key=aes_key, nonce=old_used_nonce)
                old_data_with_header = ch_tmp.decrypt(aes_file_data)
                _, old_data, original_file_name, is_folder = extract_digest_and_plaintext_from_data_with_header(old_data_with_header)

                if new_is_tar_gz_folder == is_folder and new_original_file_name == original_file_name:

                    if new_is_tar_gz_folder:
                        #print('Compare checksums:')
                        checksum_old = get_tar_checksum_from_memory(old_data)
                        checksum_new = get_tar_checksum_from_memory(new_data)
                        if checksum_new == checksum_old:
                            if not silent:
                                print('IN MEMORY tar gz comparison: SAME')
                                print(checksum_old)
                                print(checksum_new)
                            return True
                        else:
                            if not silent:
                                print('IN MEMORY tar gz comparison: DIFFERENT')
                    else:  # Ordinary file
                        if new_data_size == len(old_data):
                            if old_data == new_data:
                                return True
                else:
                    return False
    except:
        pass
    return False


# Inserts new data or updates existing data, does nothing if the data is (exactly) the same.
# address_translation_file are only used when creating new data
# is_tar_gz_folder is needed for when we compare tar.gz files
# Note: If you want to insert data, do not use this function directly (or be sure to know what you are doing), use insert_file and insert_folder instead.
#
# The nonce is a number usable only once for AES encryption
# It can freely be distributed and does not be hidden like the key.
# Its uniqueness is only needed per each specific AES key in CFB or CTR mode,
# but it needs to be unique in every block (encrypted with a specific key),
# not only once for each starting block for example.
# We use AES256-CTR, and it can thus only encrypt a total of around 256EB per key.
# When that amount of data has been transferred, it is no longer safe to use that specific key.
# Encrypt and insert into or update database
def insert_our_data(name, filefolder, file, address_translation_file=0, is_tar_gz_folder=False, silent=False):
    filefolder = filefolder.replace('\\', '/')
    file = file.replace('\\', '/')
    filepath = os.path.join(filefolder, file)
    if not os.path.exists(filepath):
        if not silent:
            print("#Error: cannot find file", filepath)
        return False
    new_data = None
    #print("Read file: " + str(filepath))
    with open(filepath, 'rb') as f:
        new_data = f.read()
    if new_data is None:
        if not silent:
            print('File not found')
        return False
    # Make a 'local' folder if it doesn't already exist
    directory = os.path.join(locator.module_path(), get_configuration_value("data_owned_folder"))
    if not os.path.exists(directory):
        try:
            os.makedirs(directory)
        except:  # This happens when you try to insert multiple data in parallel if you have never inserted any data
            pass

    #####
    # Setup

    # Make the encrypted result files name
    aes_file_name = os.path.join(directory, name + '.aes')
    next_safe_nonce = 0
    # These will either come from the database (when we update data)
    # or be generated when we insert a new data
    file_rsa_pub = None
    file_rsa_priv = None
    data_version_to_use = 1

    #####
    # Data is now loaded into new_data

    update_data = data_exists(name)  # Does a database entry exists?

    # Get a database connection that we will use for selecting data, and then inserting or updating data
    if update_data:
        our_data_information = dq.get_our_data_information(name)
        if not our_data_information:
            print("#Error database problems...")
            return
        data_owned_id = our_data_information[0]
        aes_key = bytes.fromhex(chomp(our_data_information[1]))
        old_used_nonce = int(our_data_information[2])
        next_safe_nonce = int(our_data_information[3])
        data_version_to_use = int(our_data_information[4]) + 1  # +1 for next version

        # Get the file-RSA from the database
        file_rsa_priv_hex = dq.get_rsa_key_by_data_id(data_owned_id, True)
        file_rsa_pub_hex = dq.get_rsa_key_by_data_id(data_owned_id)
        file_rsa_priv = RSA.importKey(bytes.fromhex(file_rsa_priv_hex))
        file_rsa_pub = RSA.importKey(bytes.fromhex(file_rsa_pub_hex))

        # Make the cipher helper that will encrypt our data, starting at an unused nonce:
        ch = CipherHelper(aes_key=aes_key, nonce=next_safe_nonce)

        # Now check if the data is new, otherwise just bail out
        if same_file(new_data, aes_file_name, file, is_tar_gz_folder, aes_key, old_used_nonce, silent):
            # print('DEBUG: they are the SAME, skipping update and bails out.')
            if not silent:
                print('The data of [' + name + '] has not changed, no update was done.')
            return
    else:
        # New file, generate a new set of RSA keys that will be used
        # 1) to make the name + rsapub a unique pair (so nodes will know which one to serve, as they can store several files with the same name)
        # 2) to authenticate the data after downloading it
        if dq.get_is_testrun():
            file_rsa_priv, file_rsa_pub = rh.generate_rsa_keypair(1024)  # Small unsafe, but quick to generate, keys for testing
        else:
            file_rsa_priv, file_rsa_pub = rh.generate_rsa_keypair(4096)
        file_rsa_priv_hex = file_rsa_priv.exportKey('DER').hex()  # refact "key to hex" or something?
        file_rsa_pub_hex = file_rsa_pub.exportKey('DER').hex()
        # Completely new data, make a key with a nonce starting at 0
        ch = CipherHelper()

    # file_rsa_priv and file_rsa_pub are generated and ready to be used for the digest
    # new_data contains the new data in plaintext format

    # Make the digest
    # The '_' is important or "ABC1" + version 1 => "ABC11" is equal to "ABC" + version 11 => "ABC11"
    f = b'1' if is_tar_gz_folder else b'0'
    to_digest = new_data + b'_' + str(data_version_to_use).encode() + b'_' + file.encode() + b'_' + f
    digest = rh.create_digest(to_digest, file_rsa_priv)  # data + version so both are authenticated

    # This is the data to encrypt and save to disk
    # [8 bytes rnd][8 bytes digest size][digest variable size][plaintext (variable size)]
    data_with_header = generate_new_header(digest, file, is_tar_gz_folder) + new_data

    # Encrypt with AES-CTR and store the before and after nonces
    nonce_used = ch.get_nonce()
    encrypted_data = ch.encrypt(data_with_header)
    #encrypted_data = ch.encrypt(new_data, True)
    next_safe_nonce = ch.get_nonce()

    # Store the encrypted data in the data folder so listener can serve it to clients:
    with open(aes_file_name, 'wb') as f:
        f.write(encrypted_data)

    # Insert into or update database
    connection = dc.get_connection()
    cursor = connection.cursor()
    if update_data:
        cursor.execute("UPDATE data_owned SET data_version=data_version+1, iv = ?, next_safe_iv = ? WHERE name = ? ;",
                       (str(nonce_used), str(next_safe_nonce), name))
    else:
        cursor.execute("INSERT INTO data_owned(NAME, address_translation_file, key, iv, next_safe_iv, privkey, pubkey) VALUES(?, ?, ?, ?, ?, ?, ?);",
                       (name, address_translation_file, ch.aes_key.hex(), str(nonce_used), str(next_safe_nonce), file_rsa_priv_hex, file_rsa_pub_hex))
    connection.commit()

    if not silent:
        if update_data:
            print('[' + name + '] updated successfully.')
        else:
            print('New data [' + name + '] inserted successfully.')
    return True


# Accessible as python function:
def insert_file(data_name, folder, file_name, address_translation_file=0, silent=False):
    insert_our_data(data_name, folder, file_name, address_translation_file, silent=silent)


# Easy peasy change dir and back
@contextmanager
def cwd(path):
    oldpwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(oldpwd)


# Accessible as python function:
def insert_folder(data_name, base_folder, folder_to_add, regex, silent=False):
    folder = folder_to_add
    if not silent:
        print('Insert folder:', folder)

    # Go to the base folder so that the tar file won't have them in the filenames
    with cwd(base_folder):
        # get all the files in 'folder' and zip it:
        full_data_name = data_name + '.tar.gz'

        with tarfile.open(full_data_name, "w:gz", format=GNU_FORMAT) as tar_handle:
            for root, _, files in os.walk(folder):
                for file in files:
                    file_path = os.path.join(root, file)
                    if re.search(regex, file_path):
                        if not silent:
                            print('# ADD:', file_path)
                        tar_handle.add(file_path)  #, arcname=os.path.basename(os.path.join(root)))
                    else:
                        if not silent:
                            print('# SKIP:', file_path)

    # name, filefolder, file, rsa_pub_key
    insert_our_data(data_name, base_folder, data_name + '.tar.gz', is_tar_gz_folder=True, silent=silent)
    # delete tar.gz file
    os.remove(os.path.join(base_folder, full_data_name).replace('\\', '/'))

