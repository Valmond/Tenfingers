#!/bin/bash -i

if alias 10f >/dev/null 2>&1; then
  echo "No need to create the alias for 10f as it does already exist."
else
  echo "Create alias for 10f"
  echo "#Tenfingers" >> ~/.bashrc
  echo "alias 10f='python3 $HOME/Tenfingers/tenfingers/10f.py'" >> ~/.bashrc
  source ~/.bashrc
  # As the source ~/.bashrc doesn't always work in skripts, let's make the temporary alias too
  # This doesn't always work, if so restart the terminal
  alias 10f='python3 $HOME/Tenfingers/tenfingers/10f.py'
fi
