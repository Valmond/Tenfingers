# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import os
import sys
from pathlib import Path

try:
    from Cryptodome.PublicKey import RSA
except ImportError:
    print("Error 5: You need to install Cryptodome:\npip3 install pycryptodomex\n")
    exit(-7)

import tarfile
import sqlite3
from sqlite3 import Error
from random import shuffle

from socket_communication import SecureCommunication
import database_query as dq
import database_connection as dc
from encryption import CipherHelper
from rsa_helper import verify_digest
from helpers import extract_digest_and_plaintext_from_data_with_header, network_byte_to_int


# This is the client software that takes a link.10f and gets the according data, decrypts it and saves it to disc


# If there is a database, lets store the addresses there so that we will know more nodes when swicharooing and so
def save_off_addresses(address_list):
    if dc.database_exists():
        for address in address_list:
            if 'rsa_pub' in address and 'ip' in address and 'port' in address:
                rsa_hex_pub_key = address['rsa_pub']
                ip = address['ip']
                port = address['port']

                # Insert nodes from the link, if the database exists so we can download data without a database
                dq.insert_node(ip, port, rsa_hex_pub_key, 'From save_off_addresses')


# Set a new data_version in a link
def update_database_link(database_file_name, new_version, new_nonce):
    try:
        connection = sqlite3.connect(database_file_name)
    except Error:
        print("Could not create the (database) link for updating version:")
        print(Error)
        return False
    cursor = connection.cursor()
    cursor.execute("UPDATE information SET file_version = ?, iv = ? WHERE 1", (new_version, new_nonce))
    connection.commit()
    return True


# Checks a files version
# Returns -1 if file not readable, missing, ...
# Otherwise the version
def get_link_file_version(link_file_path):
    try:
        connection = sqlite3.connect(link_file_path)
    except Error:
        return -1

    cursor = connection.cursor()
    cursor.execute("SELECT file_version from information")
    row = cursor.fetchone()
    if row is None:
        return -1

    if len(row) == 1:
        return row[0]
    return -1


# Gets all the data from the link and returns it, or None if failed
# info, which contains: name, key, iv, data_rsa_pub
# address_list, which is a list of addesses:
# todo: refact this with extract_base_addresses_from_tr_sub_file(), so change address_list to address, pubkey to rsa_pub etc too
def extract_data_from_database_link(database_file_name):
    # print("extract_data_from_database_link", database_file_name, flush=-True)

    if os.path.exists(database_file_name):

        try:
            connection = sqlite3.connect(database_file_name)
        except Error:
            print("Could not create the (database) link:")
            print(Error)
            return None

        cursor = connection.cursor()
        cursor.execute("SELECT name, file_version, key, iv, data_rsa_pub FROM information")
        rows = cursor.fetchall()
        if len(rows) == 0:
            print("Error: No information in link")
            return None
        data = rows[0]
        info = {
            'name': data[0],
            'file_version': data[1],
            'key': str(data[2]),
            'iv': data[3],
            'data_rsa_pub': data[4]  # Used with the filename to make a unique pair (as one node might store several files with the same name)
        }

        address_list = []
        cursor.execute("SELECT rsa_pub, ip, port FROM address")
        rows = cursor.fetchall()
        if len(rows) == 0:
            print("There are no addresses in the link")
            return info, address_list
        for data in rows:  # maybe this is not necessary... but as it should work, lets check that later
            address = {
                'rsa_pub': data[0],
                'ip': data[1],
                'port': data[2]
            }
            address_list.append(address)

        return info, address_list


# Gets all the addresses from the translation/substitution link, returns it, or None if failed
# If base_addresses == True, then get the addresses for the download of the base links payload
# If base_addresses == False, then get the addresses pointing to the Others tr/sub file
def extract_base_addresses_from_tr_sub_file(database_file_name, base_addresses):
    # print("Get data from one of the trans/sub files", database_file_name, flush=True)
    if os.path.exists(database_file_name):

        try:
            connection = sqlite3.connect(database_file_name)
        except Error:
            print("Could not create the (database) link:")
            print(Error)
            return None

        cursor = connection.cursor()
        if base_addresses:
            cursor.execute("SELECT pubkey, ip, port FROM address")
        else:
            cursor.execute("SELECT pubkey, ip, port FROM other_tr_sub_address")

        rows = cursor.fetchall()
        if len(rows) == 0:
            print("There are no addresses in the link")
            return []
        address_list = []
        for data in rows:  # maybe this is not necessary... but as it should work, lets check that later
            address = {
                'rsa_pub': data[0],
                'ip': data[1],
                'port': data[2]
            }
            address_list.append(address)

        return address_list


# Query for: version, file_size, nonce and data
# Note: Only does the query + getting information & data, nothing else
# end_byte=-1 => Download the whole data
# required_version == 0 => Just get the version, file size and nonce
# got_version, got_file_size, got_nonce, got_data = raw_download_data()
# sc = SecureCommunication(other_ip, other_port, RSA.importKey(bytes.fromhex(other_rsa_pub_key)))
def raw_download_data(sc, name, data_rsa_pub, required_version, start_byte=0, end_byte=-1):
    got_version = 0
    got_file_size = 0
    got_nonce = -1
    got_data = None

    partial_download = end_byte >= 0 and end_byte >= start_byte
    # Create the query
    query = 'ONEFINGER:' + name + ',' + data_rsa_pub
    if partial_download:  # Limit what we want to download
        query += ',' + str(start_byte) + ',' + str(end_byte - start_byte + 1)  # rsa in plaintext here as we use it as a name

    success = False

    q = sc.send(query.encode())
    if q:
        r = q.split(b',')
        if len(r) == 3:
            their_version = network_byte_to_int(r[0])
            their_file_size = network_byte_to_int(r[1])
            nonce = r[2]

            read_bytes = (end_byte - start_byte + 1) if partial_download else their_file_size

            print('query_onefinger() got back version, file_size, nonce ' +
                         str(their_version) + ',' + str(their_file_size) + ',' + str(nonce))

            # They are supposed to send back their version, file size and nonce
            if their_version and their_file_size and nonce:
                got_version = their_version
                got_file_size = their_file_size
                got_nonce = nonce

                if their_version == required_version:
                    # Get the data
                    encrypted_data = sc.send(b'GO')
                    if encrypted_data is not None:
                        if len(encrypted_data) == read_bytes:
                            got_data = encrypted_data
                            sc.send(b'THANKS_BYE')
                        else:
                            print('Download data: got incomplete data size', len(encrypted_data), read_bytes)
                    else:
                        print('Download data: got no data')

                else:
                    # We got our version information, bail out
                    sc.send(b'THANKS_BYE')
            else:
                #sc.send(b'THANKS_BYE')
                print('Download data: got mal formatted answer to our query')
        else:
            #sc.send(b'THANKS_BYE')
            print('Download data: got mal formatted answer to our query or wrong number of information back: ' + str(len(r)))

    return got_version, got_file_size, got_nonce, got_data


def _add_translation_file_addresses(address_list, base_addresses, translation_file_result, substitution_file_result):
    # Translation file:
    try:
        if translation_file_result:
            if os.path.exists(translation_file_result):
                address_list_tr = extract_base_addresses_from_tr_sub_file(translation_file_result, base_addresses=base_addresses)
                for address in address_list_tr:
                    # print('Address translation:', address['ip'], address['port'])
                    if address not in address_list:
                        address_list.append(address)
    except:
        pass

    # Substitution file:
    try:
        if substitution_file_result:
            if os.path.exists(substitution_file_result):
                address_list_tr = extract_base_addresses_from_tr_sub_file(substitution_file_result, base_addresses=base_addresses)
                for address in address_list_tr:
                    # print('Address translation:', address['ip'], address['port'])
                    if address not in address_list:
                        address_list.append(address)
    except:
        pass

    return address_list


# Convenience helper function
def _open_socket_for_download(ip, port, rsa_pub_key, handshake_timeout_seconds):
    sc = SecureCommunication(ip, port, rsa_pub_key, handshake_timeout=handshake_timeout_seconds)
    return sc


# Convenience helper function
# Sets up the connection
# Downloads the latest version
# if fail_on_better_version=True then, if the node proposes a better version, we do not download it
def _download_raw_data_from_node(sc, name, data_rsa_pub, required_version, start_byte=0, end_byte=-1, fail_on_better_version=False):
    got_version = 0
    if sc.is_active():
        # Returns all information Except 'got_data' if the version is not the asked for
        got_version, got_file_size, got_nonce, got_data = raw_download_data(sc, name, data_rsa_pub, required_version, start_byte=start_byte, end_byte=end_byte)
        if got_data:  # We got data for the asked for version
            return got_version, got_file_size, got_nonce, got_data
        else:
            if got_version > required_version:  # They got a better version, get it?
                if fail_on_better_version:  # This is eventually convenient when doing parallel downloads
                    return got_version, 0, 0, None
                else:
                    print('Download helper: get better version', got_version)
                    sc.connect()  # Re initialise the connection so that we can now ask for the data (the data with the better version)
                    got_version, got_file_size, got_nonce, got_data = raw_download_data(sc, name, data_rsa_pub, got_version)
                    return got_version, got_file_size, got_nonce, got_data
            else:
                if got_version == required_version:
                    print('#Error, correct version, but no data sent: wanted version:', required_version, ' got version:', got_version)
                else:
                    if got_version == 0:
                        print('#Error, node does no longer share the data (answered with version = 0)')
                    else:
                        print('#Error, stale version, wanted version:', required_version, ' got version:', got_version)

    return got_version, 0, 0, None


def authenticate_downloaded_data(data, key, nonce, version, file_rsa_pub_hex):
    ch = CipherHelper(key, nonce)
    plain_data_with_header = ch.decrypt(data)
    digest, plain_data, original_file_name, is_folder = extract_digest_and_plaintext_from_data_with_header(plain_data_with_header)

    # Verify the digest now
    # The digest is created with the private RSA key for this file
    # taking the data plus the version, like this (see insert_data.py)
    # digest = rh.create_digest(new_data + str(data_version_to_use).encode(), file_rsa_priv)
    f = b'1' if is_folder else b'0'
    message = plain_data + b'_' + str(version).encode() + b'_' + original_file_name.encode() + b'_' + f
    file_rsa_pub = RSA.importKey(bytes.fromhex(file_rsa_pub_hex))

    if verify_digest(message, file_rsa_pub, digest):
        return True, plain_data, original_file_name, is_folder
    return False, None, None, None


# Translation file contains the addresses (too). It is the downloaded data, not the translation 10f file!
# Use only forward slashes here
# base_addresses:
# True  => use the 'address' in the tr/sub files result. You want this if you download the shared payload
# False => use the 'other_tr_sub_address' in the tr/sub files result. You want this if you download the translation files payloads
def get_data_from_database_link(linkfile, dest_folder, base_addresses=True, translation_file_result=None, substitution_file_result=None):
    print('get link', linkfile)

    # Checks the link exists:
    if not os.path.isfile(linkfile):
        print('Error: cannot find the tenfingers link file: [' + str(linkfile) + ']')
        return

    # Get some infos from the link file
    info, address_list = extract_data_from_database_link(linkfile)
    minimum_required_version = info['file_version']

    # get more addresses from the translation files
    address_list = _add_translation_file_addresses(address_list, base_addresses, translation_file_result, substitution_file_result)
    save_off_addresses(address_list)

    # this is the key,iv for the encrypted file we want to retrieve
    key = bytes.fromhex(info['key'])
    iv = int(info['iv'])  # OBSOLETE! Use the node given nonce instead!
    data_rsa_pub = info['data_rsa_pub']

    # Download:
    total_timeout = 0
    tries = 7
    handshake_timeout_seconds = 0.3  # We'll add to this if no address has the time to answer

    while tries > 0:

        shuffle(address_list)

        # Loop all addresses and try to get the data. todo: make this parallel / or at least bail out faster if node is not up.
        for address in address_list:  # todo: make this random access as maybe the first node always keep a stale (but new enough) version.
            if 'rsa_pub' in address and 'ip' in address and 'port' in address:
                rsa_hex_pub_key = address['rsa_pub']
                ip = address['ip']
                port = address['port']
                rsa_pub_key = RSA.importKey(bytes.fromhex(rsa_hex_pub_key))

                print('Download from: ' + str(ip) + str(':') + str(port) + '    timeout=', int(handshake_timeout_seconds * 1000), 'ms')
                total_timeout += handshake_timeout_seconds

                sc = _open_socket_for_download(ip, port, rsa_pub_key, handshake_timeout_seconds)

                # If the communication starts, the timeout will be set to the standard internet 30 seconds timeout

                got_version, got_file_size, got_nonce, got_data = (
                    _download_raw_data_from_node(sc, info['name'], data_rsa_pub, minimum_required_version))

                if got_version > 0:
                    if got_data is not None:

                        authentification_ok, plain_data, original_file_name, is_folder = authenticate_downloaded_data(got_data, key, got_nonce, got_version, info['data_rsa_pub'])

                        #print("AUTHENTIFICATION =", authentification_ok)

                        # if newer version, update best version if authentication is ok

                        if authentification_ok:
                            if dest_folder:
                                if not os.path.exists(dest_folder):
                                    os.makedirs(dest_folder)
                            with open(original_file_name, 'wb') as f:
                                f.write(plain_data)
                                f.close()
                                # print('Success: retrieved data.')
                                if got_version > minimum_required_version:
                                    # print('Update link with new version number: from', minimum_required_version, 'to', their_version)
                                    update_database_link(linkfile, got_version, got_nonce)

                                # was it a folder? If so, decompress it here!
                                if is_folder:
                                    # print('Untar and gunzip')
                                    try:
                                        with tarfile.open(original_file_name, "r:gz") as tar_handle:
                                            directory = dest_folder if dest_folder else './'
                                            tar_handle.extractall(path=directory)  # Will be made here in this folder
                                            tar_handle.close()
                                            os.remove(original_file_name)
                                    except BaseException as error:
                                        print('An exception occurred: {}'.format(error))

                                print('total_timeout used=', total_timeout)
                                return
                        else:
                            print('#Error: Downloaded data corrupt or not emitted by owner.')
                    else:
                        print('SecureCommunication served a stale version, they have version ' + str(got_version))
                else:
                    if got_version == 0:
                        print('SecureCommunication bailed out because node does no longer serve the data')
                    else:
                        print('SecureCommunication Failed')
            else:
                print('#Error: address corrupt.')

        tries -= 1
        handshake_timeout_seconds = (handshake_timeout_seconds * 2) + 0.1  # So if starting at 0.1, 0.3, 0.7 ...

