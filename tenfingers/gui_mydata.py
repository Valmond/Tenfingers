import os
import wx
import wx.lib.mixins.listctrl as listmix
from wx.lib.agw import ultimatelistctrl as ULC

import database_connection as dc
import database_query as dq

# Todo: refresh GUI when we get a new inserted data

#

APPNAME = 'Tenfingers sharing protocol simple GUI'
APPVERSION = '1.0'
MAIN_WIDTH = 800  # Works only for this widget, so fix that using the parent widget
MAIN_HEIGHT = 500



########################################################################
class GfxMyData(wx.Panel, listmix.ColumnSorterMixin):

    # ----------------------------------------------------------------------
    def __init__(self, parent):
        self.parent = parent
        self.setup()

    def setup(self):
        wx.Panel.__init__(self, self.parent, -1, style=wx.WANTS_CHARS, size=(MAIN_WIDTH, MAIN_HEIGHT))

        self.index = 0

        self.list_ctrl = ULC.UltimateListCtrl(self, -1, agwStyle=ULC.ULC_REPORT | ULC.ULC_HAS_VARIABLE_ROW_HEIGHT)
        self.list_ctrl.InsertColumn(0, "Your data", width=240)
        self.list_ctrl.InsertColumn(1, "Info", wx.LIST_FORMAT_RIGHT, width=200)
        self.list_ctrl.InsertColumn(2, "Share")  # re-uploads data to this name

        self.my_data = {}
        my_data_index = 0

        # Get all our data we  might share from tenfingers.db and construct a comprehensive list of tuples
        rows = dc.query_all("SELECT id, name FROM data_owned WHERE address_translation_file = 0")
        for row in rows:
            data_owned_id = row[0]
            data_name = row[1]
            wanted_fingers = dq.get_wanted_fingers(data_owned_id)
            actual_fingers = dq.get_number_of_fingers(data_owned_id)

            data_version = dq.get_owned_data_version(data_owned_id)
            data_info = "Version: " + str(data_version) + "   Shared: " + str(actual_fingers) + "/" + str(wanted_fingers)

            self.my_data[my_data_index] = (data_name, data_info, "")
            my_data_index += 1

        # Generate the list with buttons and information
        items = self.my_data.items()
        index = 0
        # buttons = {}  # insert like 0:button1, 1: button2 ...
        for key, data in items:
            pos = self.list_ctrl.InsertStringItem(index, data[0])
            self.list_ctrl.SetStringItem(index, 1, data[1])
            # self.list_ctrl.SetStringItem(index, 2, data[2])
            button = wx.Button(self.list_ctrl, id=wx.ID_ANY, label="Save Link")
            self.list_ctrl.SetItemWindow(pos, col=2, wnd=button, expand=True)
            self.list_ctrl.SetItemData(index, key)

            # Connect button to function
            # buttons[index] = button
            button.Bind(wx.EVT_BUTTON, self.save_link)
            button.SetName(str(index))  # Store off the index in its name until we figure out how to do it better

            index += 1

        # Now that the list exists we can init the other base class,
        # see wx/lib/mixins/listctrl.py
        self.itemDataMap = self.my_data
        listmix.ColumnSorterMixin.__init__(self, 3)
        self.Bind(wx.EVT_LIST_COL_CLICK, self.OnColClick, self.list_ctrl)  # Doesn't work?

        # Add a "Insert new file data button
        self.insert_button = wx.Button(self, id=wx.ID_ANY, label="Insert/update data", pos=(640, 5))
        self.insert_button.Bind(wx.EVT_BUTTON, self.insert_new_file)


        # Add a "Insert new folder button
        self.insert_folder_button = wx.Button(self, id=wx.ID_ANY, label="Insert/update folder", pos=(640, 40))
        self.insert_folder_button.Bind(wx.EVT_BUTTON, self.insert_new_folder)

        sizer = wx.BoxSizer(wx.VERTICAL)
        #sizer.Add(self.insert_button, 1, wx.MINIMIZE, 5)  # If I add it here it just gets ENORMOUS (expand=True ?)
        sizer.Add(self.list_ctrl, 1, wx.ALL | wx.EXPAND, 5)

        self.SetSizer(sizer)

    #


    def insert_new_file(self, event):
        print("insert_new_file clicked", flush=True)
        # Todo: multi selection, regex selection, select a folder...
        fdlg = wx.FileDialog(None, "Choose a file to insert:", "", "", "*", wx.FD_OPEN)
        if fdlg.ShowModal() == wx.ID_OK:
            file_path = fdlg.GetPath()
            from pathlib import Path
            name = Path(file_path).stem  # eg. test
            name_w_ext = os.path.basename(file_path)  # ex test.tst
            base_folder = os.path.dirname(file_path)
            print("Insert data:", file_path, " with the name", name, "from the base folder", base_folder)
            from database_query import get_link_data_by_name
            if get_link_data_by_name(name):
                print('Info: [' + name + '] already exists, we are updating it.')
            # Insert or update:
            import access as ac
            ac.insert_data(name, base_folder, name_w_ext, 0)  # We could set swicharoo_tries to at least 1 or5 here (make it configurable in parameters.py?)

            # Hardcore regenerate the GUI
            self.setup()

    def insert_new_folder(self, event):
        fdlg = wx.DirDialog(None, "Choose a folder to insert:", "", wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST)
        if fdlg.ShowModal() == wx.ID_OK:
            folder_path = fdlg.GetPath()
            from pathlib import Path
            name = Path(folder_path).stem  # eg. test
            base_folder = os.path.dirname(folder_path)
            print("Insert folder data:", folder_path, " with the name", name, "from the base folder", base_folder)
            from database_query import get_link_data_by_name
            if get_link_data_by_name(name):
                print('Info: [' + name + '] already exists, we are updating it.')
            # Insert or update:
            import access as ac
            swicharoo_tries = 0
            ac.insert_folder(name, base_folder, name, regex='.*', tries=swicharoo_tries)

            # Hardcore regenerate the GUI
            self.setup()

    #

    def save_link(self, event):

        download_id = event.GetEventObject().GetName()  # Yeah I know...
        row = self.my_data[int(download_id)]
        link_name = row[0]
        print("Export", link_name, flush=True)  # [0] = the name of the download

        fdlg = wx.FileDialog(None, "Save link files:", "", link_name, "*.10f", wx.FD_SAVE)

        if fdlg.ShowModal() == wx.ID_OK:
            file_path = fdlg.GetPath()
            print("Export link to:", file_path)

            # Export link, NOTE: inspired by the 10f.py code
            from get_link_from_data import create_and_save_link_database
            create_and_save_link_database(link_name)

            import shutil
            base = file_path[:-4]
            shutil.move("./" + link_name + ".10f", base + ".10f")
            shutil.move("./" + link_name + "_tr.10f", base + "_tr.10f")
            shutil.move("./" + link_name + "_sub.10f", base + "_sub.10f")



    def GetListCtrl(self):
        return self.list_ctrl

    #----------------------------------------------------------------------
    def OnColClick(self, event):
        print("Click!", flush=True)
        pass


########################################################################
class MyForm(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, '%s v%s' % (APPNAME, APPVERSION), size=(MAIN_WIDTH, MAIN_HEIGHT), style=wx.MINIMIZE_BOX | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX | wx.CLIP_CHILDREN)

        # Add a panel so it looks the correct on all platforms
        panel = GfxMyData(self)


# ----------------------------------------------------------------------
# Run the program
if __name__ == "__main__":
    app = wx.App(False)
    frame = MyForm()
    frame.Show()
    app.MainLoop()
