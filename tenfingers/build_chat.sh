# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

echo '------------- Builds a release version of Chat ----------------'

################################################
# Build a release version

mkdir chat_build
mkdir chat_build/py_source

# Copy data
echo '[Copy python scripts]'
cp *.py chat_build/py_source


echo '[Clean up build directories]'

rm -f -r dist/*

echo '[make executables:]'

if [ $(uname -o) == "Msys" ]
then
	cd chat_build
	echo 'Build Windows version'
	../venv/Scripts/pyinstaller.exe py_source/chat_gui.py --clean --onefile
	mv dist/chat_gui.exe ../chat_gui.exe
	../venv/Scripts/pyinstaller.exe py_source/chat.py --clean --onefile
	mv dist/chat.exe ../chat.exe
	wait
else
	echo 'Build Linux version'
	pyinstaller chat_gui.py --clean --onefile
	pyinstaller chat.py --clean --onefile
	wait
fi


rm chat.spec
echo '[Done.]'

