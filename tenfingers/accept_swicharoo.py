# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

from configuration import get_configuration_value
from storagesizes import get_shared_files_size

"""
Figure out if we can/should accept an incoming file
"""


def storage_space_enough_for_swicharoo(their_file_size):
    used_space = get_shared_files_size()
    return used_space + their_file_size <= get_configuration_value('max_storage_space_other_mb')*1024*1024


def auto_accept_small_file_for_swicharoo(their_file_size):
    # todo: Check if we exceed the limit for gracefully stored files!
    return their_file_size <= get_configuration_value('swicharoo_minsize_accept_all') or get_configuration_value('swicharoo_policy') == "GRANT_ALL"


def do_accept_swicharoo(my_file_size, their_file_size):
    # 1: Do we have global space to store the file?
    # 2: Is the file under our Max File size?
    # 3: If the file is really tiny, auto-accept
    # 4: Check which policy to use and see if it fits (e.g MAX_DOUBLE, ... )
    # 5: Default to: Accept file

    # 1: Do we have global space to store the file?
    used_space = get_shared_files_size()
    if not storage_space_enough_for_swicharoo(their_file_size):
        # print('Rejected swicharoo because we are full: used='+ str(used_space) + ' their size='+str(their_file_size) + ' storage space=' + str(get_configuration_value('max_storage_space_other_mb')*1024*1024) )
        return False

    # 2: Is the file under our Max File size?
    if their_file_size > get_configuration_value('swicharoo_file_maxsize_kb')*1024:
        # print('Rejected swicharoo because their file was too big')
        return False

    # 3: If the file is really tiny, auto-accept
    if auto_accept_small_file_for_swicharoo(their_file_size):
        return True

    # 4: Check which policy to use and see if it fits (e.g MAX_DOUBLE, ... )
    if get_configuration_value('swicharoo_policy') == "MAX_DOUBLE":
        return their_file_size <= my_file_size*2

    # 5: Default to: Accept file
    return True
