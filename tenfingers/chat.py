# Copyright (C) 2023 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

# Tenfingers Chat example
#
# On windows, You need to point yopur PATH to a running 10f.exe, on linux, you need to access 10f from the chat folder
#
# Setup is done automatically
#
# -at "Sharing is caring!"  => chat a line of text
# -sh name        => Generate our chat files (there are three), give them to someone else so that they can follow you! (see -au below)
# -au name        => Adds a link file to the chat under the name 'name' (user gets thise files by using the '-sh' command, see above)
# -s user_name => show user_name's chat log
# -af file  => add and share a file in the chat

# NOT YET CODED:


# -l => show all chatters we follow
# -al link.10f  => add a link file (must be a .10f file)  TODO (maybe generate the 10f file automatically?!)
#

import os
import sys
import argparse
import shutil

import access as ac
from get_data_from_link import extract_data_from_database_link

from chatlib import check_setup, add_text_and_share, add_file_as_link_and_share

s_chat_name = 'chat_data'
s_base_dir = './' + s_chat_name  # base dir and name of share


def get_index(user):
    index = 0
    index_file = os.path.join(s_base_dir, user, '_index.txt')
    if os.path.exists(index_file):
        with open(index_file, 'r') as f:
            index = f.readline()
            if index.isdigit():
                index = int(index)
    return index


def set_index(user, index):
    index_file = os.path.join(s_base_dir, user, '_index.txt')
    with open(index_file, 'w') as f:
        f.write(str(index))


# Okay so this func does two things, but it's kind of useful
# gets (and saves off) next free index
def get_next_index(user):
    index = get_index(user) + 1
    set_index(user, index)
    return index



parser = argparse.ArgumentParser(description='Tenfingers chat!')

parser.add_argument('-au', '--adduser', help='Add another chatters scah.10f file so you can see what they are chatting about!')
parser.add_argument('-l', '--list', help='List all added users.')
parser.add_argument('-s', '--show', help='Show my (default) or any chatters chat!')
parser.add_argument('-sh', '--share', help='Make the link so you can share your chat with others! WIP')

parser.add_argument('-af', '--addfile', help='Share a file with your friends!')
#  , default='ShyUser'

parser.add_argument('-at', '--addtext', help='Chat away by adding some text to this command!')
#parser.add_argument('-al', '--add-link', help='Share a tenfinges link with this one!')

parser.add_argument('-al', '--addlink', help="Share a tenfinges link with this one!")

args = parser.parse_args()

#print("--------------------")
#print(args)

#print(args.addlink)

# Always check if we are ready to roll
check_setup()

if len(sys.argv) == 1:
    print("Tenfingers free chat service!")
    print("Chat a line: ./chat.exe -al \"Hello you!\"")
    print("Share a file in the chat: ./chat.exe -af \"./holidayphotos/summerphoto006.jpg\"")
    print("Give your chat-links to someone: ./chat.exe -sh")
    print("Note: give them the three files!")
    print("Add someones chat-link so you can see their chat log: -au theirname.10f")
    print("Check out what someone (that you already added) is chatting about: ./chat.exe theirname")
    print("")
    sys.exit(0)


if args.adduser:
    print("Add user:", args.adduser)
    new_user = args.adduser
    print("Add Link:", new_user)
    # Remove the redundant ./ ??
    new_chatter_dir = os.path.join('./', s_base_dir, 'others', new_user)
    if os.path.isdir(new_chatter_dir):
        print("A chatter named", new_user, "is already present")
        print("If", new_user, "is a new user, rename the three files and try again!")
    else:
        os.makedirs(new_chatter_dir)
        # OBS: The name is translated back to the 's_chat_name' name all chatters use.
        # It was changed so that we can send 'our_name' files at the same time as sending 'other_name'
        shutil.copyfile('./' + new_user + '.10f', os.path.join(new_chatter_dir, s_chat_name + '.10f'))
        shutil.copyfile('./' + new_user + '_tr.10f', os.path.join(new_chatter_dir, s_chat_name + '_tr.10f'))
        shutil.copyfile('./' + new_user + '_sub.10f', os.path.join(new_chatter_dir, s_chat_name + '_sub.10f'))


if args.list:
    print("List:")

if args.show:
    user = args.show
    if args.show == 'me':
        print("todo: show My chat log")
    else:
        print("Chat log of:", user)
        chatter_dir = os.path.join(s_base_dir, 'others', user)
        chat_link = os.path.join(chatter_dir, s_chat_name + '.10f')
        #print(chatter_dir)
        #print(chat_link)
        # todo: download it in situ, refact and use @contextmanager def cwd(path): in insert_data.py
        ac.download(chat_link, chatter_dir)

        index = get_index(os.path.join('others/', user))
        for i in range(1, index + 1):
            # Is it a text file?
            text_file = os.path.join(chatter_dir, str(i) + '.txt')
            if os.path.exists(text_file):
                with open(text_file, "r") as f:
                    print("#" + str(i) + " " + f.read())
            else:
                link_file = os.path.join(chatter_dir, str(i) + '_chat_link.10f').replace('\\', '/')
                if os.path.exists(link_file):
                    info, _ = extract_data_from_database_link(link_file)
                    print("#" + str(i) + " <LINK> [originalfilename]  download with:  ./10f.exe " + link_file + "")
                else:
                    print("#" + str(i) + " <Unknown>")


if args.share:
    print("Share (export my link so you can give it to other people):")
    print("Your name:", args.share)
    print("To make the chat last forever, share the three links:")
    print(args.share + '.10f')
    print(args.share + '_tr.10f')
    print(args.share + '_sub.10f')
    #from chatlib import share_link
    #share_link(args.share)




# Add a text to my chat
if args.addtext:
    print("Add text:", args.addtext)

    add_text_and_share(args.addtext)




if args.addfile:
    add_file_as_link_and_share(args.addfile)




if args.addlink:
    pass


