# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import os
import sys
from sys import exit
import threading
import signal
import socket
import logging
import time
try:
    from Cryptodome.PublicKey import RSA
except ImportError:
    print("Error 4: You need to install Cryptodome:\npip3 install pycryptodomex\n")
    exit(-7)

import datetime
from struct import pack, unpack
import json
import random

from configuration import get_configuration_value
from socket_communication import SecureCommunicationListen
import database_query as dq
import rsa_helper as rh
import locator
from helpers import human_readable_file_size
from accept_swicharoo import auto_accept_small_file_for_swicharoo, storage_space_enough_for_swicharoo
import address_registry as ar
from scheduler import Scheduler
from version import get_tenfingers_version
from ip_version import typeOfIPAddress
from storagesizes import get_shared_files_size
import filepath as fp


"""
the listener serves encrypted data to anyone knowing the name of the data
It stores data in a SQLite database
"""

# Set global flogging:
#logging.basicConfig(
#    filename='listener.log',
#    level=logging.INFO,
#    #format="%(asctime)s %(message)s",
#    format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
#    datefmt='%Y-%m-%dT%H:%M:%S%z'
#)

logging.basicConfig(
     format="{asctime} {levelname}: {message}",
     style="{",
     datefmt='[%Y-%m-%d %H:%M:%S (UTF%z)]',
     level=logging.INFO,
)



logging.info("Logging set up done.")

# Set up logging to console
#console = logging.StreamHandler()
#console.setLevel(logging.DEBUG)
#formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')  # set simple console format
#console.setFormatter(formatter)

#logging.Formatter.formatTime = (lambda self, record, datefmt='%Y-%m-%dT%H:%M:%S%z': datetime.datetime.fromtimestamp(record.created, datetime.timezone.utc).astimezone().isoformat(sep="T",timespec="milliseconds"))

#logging.getLogger('').addHandler(console)  # add the handler to the root logger

logger = logging.getLogger(__name__)
# Logging setup done

# Global shut down boolean
force_shutdown = False


class Listen:
    def __init__(self):
        self.listensocket = None
        self.clientsocket = None

        print('Allocated storage space for other nodes:' + human_readable_file_size(get_configuration_value('max_storage_space_other_mb')*1024*1024))
        print('Max filesize for other nodes:' + human_readable_file_size(get_configuration_value('swicharoo_file_maxsize_kb')*1024))
        print('Always accept files under this size (if we have storage space):' + human_readable_file_size(get_configuration_value('swicharoo_minsize_accept_all')))

    def forcestop(self):
        # logging.info("Shutting down listener...")
        print('forcestop() callsed, shutting down listener...')
        if self.listensocket is not None:
            self.listensocket.close()

    def listenthread_secure(self, client_socket, client_address):
        # logging.info("Recv Message from" + str(client_address[0]))

        prv, pub = dq.get_rsa_hex_keys()
        # logging.info("Recv Message, make secure link:")
        scl = SecureCommunicationListen(client_socket, RSA.importKey(bytes.fromhex(prv)), get_configuration_value('listen_read_bytes'))
        if not scl.is_active():
            # logging.info('Listener could not setup secure link with caller, bailing out...')
            return
        n = scl.recv()

        if not n:
            # logging.info('Recved nothing, close socket and bail out.')
            return

        # All requests take up 10 bytes
        request = n[:10]
        data = n[10:]
        try:
            # What do they want?

            if request == b'AREUALIVE?':  # Just check if we are a Tenfingers node
                # logging.info('Listen thread got AREUALIVE req.')
                scl.send(b'YES')

            # This is the download data request
            if request == b'ONEFINGER:':  # Rest of the request is a name + RSA_PubKey + eventuall: start_byte, read_bytes. If the name corresponds in the database, send back the encrypted file.
                # logging.info('Listen thread got ONEFINGER req.')
                data = data.decode()
                datalist = data.split(',')

                if len(datalist) == 2 or len(datalist) == 4:

                    partial_download = len(datalist) == 4
                    download_start_byte = 0
                    download_size = -1
                    wrong_parameters = False
                    if partial_download:
                        try:
                            download_start_byte = int(datalist[2])
                            download_size = int(datalist[3])
                            # print("Node wants a partial download starting at " + str(download_start_byte) + " with the length of " + str(download_size))
                        except ValueError:
                            wrong_parameters = True

                    if not wrong_parameters:
                        #These two can be either shared data, our our own data
                        name = datalist[0]
                        data_rsa_pub = datalist[1]

                        row = dq.get_link_data_by_name_and_rsapub(name, data_rsa_pub)

                        # logging.info('Request for: [' + name + ']')

                        if row is not None:
                            # logging.info('Found [' + name + '], points on file [' + row[0] + ']')
                            # Get data from DB
                            data_shared_id = row[0]
                            file = row[1]
                            our_version = row[2]
                            our_nonce = row[3]
                            data = None

                            # Is it our data or shared data of someone other
                            #print('Get other folder')
                            others_folder = dq.get_shared_data_folder_by_name_and_rsa_pub(name, data_rsa_pub)
                            owned_data = others_folder == ''
                            if owned_data:  # Did not find it in the stuff we share for others
                                file_folder = get_configuration_value("data_owned_folder")
                            else:
                                file_folder = os.path.join(locator.create_path(get_configuration_value("data_shared_folder")), others_folder)

                            # Send back version + file size + nonce
                            # get the folder that is in the /data/ folder (based on the table 'folder' / or our own local folder 'local'):

                            aes_file_path = file_folder + "/" + file + '.aes'
                            #print('Get file size for:', aes_file_path)
                            file_size = 0
                            try:
                                file_size = os.path.getsize(aes_file_path)
                            except Exception as e:  # If we remove the file manually without changing the database, this cracks up
                                scl.send(b'NotStoring')

                            # todo: if file_size == 0, send back we don't have it any more

                            #print('Generate q')
                            q = str(our_version).encode() + b',' + str(file_size).encode() + b',' + str(our_nonce).encode()
                            #print('Send back q')
                            answer = scl.send(q)
                            #print('Got:' + str(answer))

                            if answer == b'GO':
                                if partial_download:
                                    pass
                                    #print("Node wants: [" + str(file) + "] partially @" + str(download_start_byte) + " + " + str(download_size) + " bytes, we propose version:", our_version, "and they accept it")
                                else:
                                    pass
                                    #print("Node wants: [" + str(file) + "], we propose version:", our_version, "and they accept it")


                                # logging.info('Trying to serve from folder:' + file_folder)

                                if not os.path.isfile(aes_file_path):
                                    scl.send(b'NotStoring')
                                    if owned_data:
                                        logging.warning('Could find data_owned in database, but could not find the file: ' + str(aes_file_path))
                                    else:
                                        logging.warning('Could find data_shared in database, but could not find the file: ' + str(aes_file_path) + ', deleting database entry.')
                                        # todo: We should remove the database entry here, as we don't store the shared data any more
                                        #dq.delete_shared_data(data_shared_id)



                                try:
                                    with open(aes_file_path, 'rb') as f:
                                        if download_size <= 0:  # Download the whole file
                                            data = f.read()
                                        else:
                                            # Download a part of the file:
                                            f.seek(download_start_byte)
                                            data = f.read(download_size)
                                except Exception as e:
                                    logging.error('Tried to open file[' + str(aes_file_path) + '] got exception:' + str(e))

                                if data is None:
                                    # logging.info('[' + name + '] does not point on a file or it was not readable.')
                                    scl.send(b'CannotRead')
                                else:
                                    # logging.info('[' + name + '] corresponds to a file and we loaded it, lets send it back:')

                                    # Send it back (maybe we need to convert the data, it is raw bytes now)
                                    scl.send(data)
                            else:
                                if answer == b'THANKS_BYE':
                                    pass
                                    #print('Node wanted information. Query successful.')
                                else:
                                    pass
                                    # They bailed
                                    #print('Node wants: [', file, '], we proposed version:', our_version, " They just wanted the version.")
                        else:
                            # logging.info('[' + name + '] not found in database.' + rsa_pub)
                            scl.send(b'NotStoring')
                    else:
                        scl.send(b'PartialDownloadParamError')
                else:
                    # logging.info('Malformed request.')
                    scl.send(b'BadRequest')

            # Update request

            if request == b'UPDATEDATA':  # another node asks us to update data we store for them
                #logging.info('Listen thread got UPDATEDATA req.')

                filesize = unpack('>Q', data[:8])[0]  # todo: always use data[:8], so a ',' in the pack(...) won't break things! SO fix it everywhere
                # print('Update data REQ, filesize', filesize)

                arr = data[8:].split(b',')  # todo: always use data[8:], so a ',' in the pack(...) won't break things! SO fix it everywhere

                if len(arr) == 5:
                    other_name = arr[1].decode()
                    other_file_pub_hex_bytes = arr[2]
                    other_file_pub_hex = other_file_pub_hex_bytes.decode()
                    new_version = arr[3].decode()
                    new_nonce = arr[4].decode()

                    # print('Verify connecting node...')

                    # First verify the caller is actually the owner of this file rsa key:
                    random_checksum = b'Digest_this!'+str(random.randint(10000, 99999)).encode()+str(time.time()).encode()
                    # print('Send random verification data:', random_checksum)
                    verification_digest = scl.send(random_checksum)  # caller should now have made a digest off of this checksum with the unique FILE rsa
                    # print('Got verification digest:', verification_digest)

                    other_node_pub_key = RSA.importKey(bytes.fromhex(other_file_pub_hex))
                    # Check caller signed the data correctly (with its private FILE rsa key)
                    # Note: if it is a bogus rsa key pair, then we will not find the data in our database
                    digest_ok = rh.verify_digest(random_checksum, other_node_pub_key, verification_digest)

                    if digest_ok:
                        # print('Digest OK, lets check data size:', filesize)
                        # print('other_name type:', type(other_name))
                        logging.info('Got update request for file '+other_name+' of size:'+str(filesize))

                        success = True
                        if filesize > get_configuration_value('swicharoo_file_maxsize_kb')*1024:
                            logging.info('Update denied, file too big. Size:' + str(filesize))
                            scl.send(b'Too Big!')
                            success = False

                        # Check the version is a number
                        if not new_version:
                            new_version = 1
                        if new_version.isdigit() is False or new_nonce.isdigit() is False:
                            logging.info('Update denied, version or nonce is not a number')
                            scl.send(b'Version not a number.')
                            success = False

                        if success:
                            logging.info('Update to version ' + str(new_version))
                            # Check we have the place to store the file:
                            used_space = get_shared_files_size()
                            if used_space + filesize > get_configuration_value('max_storage_space_other_mb')*1024*1024:
                                logging.info('Update denied, we are out of disc space.')
                                scl.send(b'Out of storage space, sorry.')
                                success = False

                        # TODO: Check the swicharoo doesn't break the swicharoo rules according to the original shared file!
                        # TODO Or else a malicous user can swicharoo a small file, then update it to a much bigger file.
                        if success:
                            pass
                            # Check our swicharoo policy!

                        if success:
                            # print('File size OK.')

                            # Verify we actually store this file for this node!
                            #folder = dq.get_shared_data_folder_by_name_and_rsa_pub(other_name, other_file_pub_hex)  # Here we use the Files rsa key
                            #folder = dq.get_folder(other_file_pub_hex, True)  error here
                            shared_data_id = dq.get_shared_data_id_by_name_and_rsa_pub(other_name,
                                                                                       other_file_pub_hex)  # Here we use the Files rsa key
                            if shared_data_id != 0:
                                # The 'file rsa' key pair is corresponding to a rsa pub key in our database
                                # print('Update request: all good!')

                                # Check the version is actually a newer one (we know the entry exists in the database)
                                old_version = dq.get_shared_data_version(shared_data_id)
                                if int(old_version) < int(new_version):

                                    # logging.info('Update accepted, starting the update.')
                                    new_file_data = scl.send(b'ACCEPTING!')

                                    # todo: check it's not too big for our policy / what they asked for

                                    # Save other nodes data
                                    # Use the files specific folder, not a node-folder any more

                                    # Here we use the Nodes rsa key

                                    # Here we use the File rsa key
                                    folder = dq.get_shared_data_folder_by_name_and_rsa_pub(other_name,
                                                                                           other_file_pub_hex)

                                    file_folder = os.path.join(locator.create_path(get_configuration_value("data_shared_folder")), folder)
                                    print("LISTENER SHARED FOLDER:", file_folder)

                                    if not os.path.exists(file_folder):
                                        os.makedirs(file_folder)

                                    save_name = os.path.join(file_folder, other_name + '.aes')
                                    print("LISTENER SHARED save_name:", save_name)
                                    # logging.info('Save off other file data: ' + save_name)
                                    with open(save_name, 'wb') as f:
                                        f.write(new_file_data)

                                    # Success, update to this new version!
                                    dq.set_shared_data_version(shared_data_id, new_version, new_nonce)

                                    # logging.info('Update done (success).')
                                    logging.info('Update succeeded. shared_data_id=' + str(shared_data_id) +
                                                 ' New version=' + str(new_version) + ' new nonce=' + str(new_nonce))
                                    scl.send(b'OK')
                                else:
                                    logging.info('Update not needed, we are up to date.')
                                    scl.send(b'NO_NEED_TO')

                            else:
                                # print('Update data: we do not store that data here')
                                logging.info('Update failed, we do not store your data (any more)')
                                scl.send(b'DataRemoved')
                        else:
                            logging.info('Update failed.')
                            scl.send(b'Updatefailed')
                    else:
                        logging.info('Update failed, the digest was wrong.')
                        scl.send(b'UNDIGESTABLE')
                else:
                    logging.info('Update denied, malformed request.')
                    scl.send(b'Malformed update request! len=' + str(len(arr)).encode())

            # The classic swicharoo:
            if request == b'SWICHAROO?':  # The good old swicharoo

                # We just got a request for swicharoo with a file size and a pub key
                data_size = data[:8]  # Get the first 8 bytes, who represents the others files size
                tmp = data[8:]  # Remove those first 8 bytes, the rest should be the other_node_pub_key,other_name,...
                split = tmp.decode().split('|')
                if len(split) != 7:
                    #print("#Listener Error in split size, expected 7 got", len(split))
                    client_socket.close()
                    logging.info('Listener: Got badly formed SWICHAROO string')
                    return

                # We now have all the information about the switch, from their side.
                other_node_pub_key = split[0]
                other_name = split[1]
                other_ip = split[2]
                other_port = split[3]
                other_data_version = split[4]
                other_nonce = split[5]
                other_file_specific_rsa_pub = str(split[6])  # As long as it can be stored in a database
                other_file_size = unpack('>Q', data_size)[0]
                #logging.info('Listener: Got a Swicharoo request, other_file_size: ' + str(other_file_size) + ' file name: ' + other_name)

                # Check the version exists and is a number
                if not other_data_version:
                    other_data_version = 1
                if other_data_version.isdigit() is False or other_nonce.isdigit() is False:
                    logging.info('Listener: Swicharoo request turned down because version or nonce is not a number')
                    scl.send(b'VERSIONNAN')
                    client_socket.close()
                    return

                # Find the data we want to send back, but do not select data that the other node already store:
                dummy_data = False
                data_owned_id = dq.get_valid_data_id_needing_finger_for_swicharoo(other_file_size, other_node_pub_key, other_name)
                if data_owned_id <= 0:
                    if not storage_space_enough_for_swicharoo(other_file_size) or not auto_accept_small_file_for_swicharoo(other_file_size):
                        scl.send(b'SRYNODATA.')
                        logging.info('Listener: Swicharoo request turned down because we do not have anything to share that the offer corresponds to and no pro bono')
                        client_socket.close()
                        return
                    # Get or set up a universal small dummy share file
                    dummy_data = True  # this is used for logging purposes only
                    # Make up some node-unique name here
                    name = 'Dummy_' + str(time.time()) + str(random.randint(1000, 9999)) + "_" + other_name  # todo: make this better, datetime + msec + rnd/counter
                    my_origfilename = name
                    # Actually make the dummy data and treat it like any other data
                    nice_string = b'Nice'
                    full_path = locator.create_path(get_configuration_value("data_owned_folder"))
                    if not os.path.exists(full_path):
                        os.makedirs(full_path)
                    with open(os.path.join(full_path, (name + '.aes')), 'wb') as f:
                        f.write(nice_string)
                    my_file_size = len(nice_string)
                    file_specific_rsa_pub = 'dummy'
                    # insert into data_shared
                    data_owned_id = dq.insert_data_owned_dummy(name)
                else:
                    # Get the name and info about our real file:
                    name = dq.get_name_of_owned_data_id(data_owned_id)
                    my_file_size = os.path.getsize(fp.generate_aes_filepath(name))
                    file_specific_rsa_pub = dq.get_rsa_key_by_data_id(data_owned_id)

                #print('#Listener try to share back data_owned_id:', data_owned_id, ' (0 = gracefully share their file)')

                random_checksum = b'Digest_this!' + str(random.randint(10000, 99999)).encode() + str(time.time()).encode()
                query = b'YES_PLEASE' + pack('>Q', my_file_size) + name.encode() + b'|' +\
                        file_specific_rsa_pub.encode() + b'|' + random_checksum
                #   <<        Send Recv           >>
                #logging.info('Listener: Send verification:')
                verification_digest = scl.send(query)
                #logging.info('Listener: got back answer')

                # Check all the different things that have to work out if we should go further:

                if verification_digest == b'U_TOO_BIG!':
                    #print('They declined our acceptance of their swicharoo request because our file is too big.', flush=True)
                    client_socket.close()
                    logging.info('Listener: Swicharoo turned down our file, too big')
                    return

                if verification_digest == b'DATA_AGAIN':
                    logging.info('Listener: They seems to already store our data.')
                    client_socket.close()
                    return

                digest_ok = rh.verify_digest(random_checksum, RSA.importKey(bytes.fromhex(other_node_pub_key)), verification_digest)
                if not digest_ok:
                    scl.send(b'DIGEST_ERR')
                    logging.info('Listener: Caller could not authentify themselves. Bail out.')
                    client_socket.close()
                    return
                #logging.info('Listener: digest ok.')

                if dq.get_link_data_by_name_and_rsapub(other_name, other_node_pub_key) is not None:  # We already store the data
                    scl.send(b'ALREADY_DO')
                    logging.info('Listener: We already store their data.')
                    client_socket.close()
                    return

                # Check if we know this node, and if we don't, insert it into the database
                other_node_id = dq.get_node_id(other_node_pub_key)
                if other_node_id == 0:  # It didn't exist in our database, insert it:
                    logging.info('Listener: Insert new node')
                    dq.insert_node(other_ip, other_port, other_node_pub_key, 'From Listener SWICHAROO')
                    other_node_id = dq.get_node_id(other_node_pub_key)

                dq.set_shared_data_version(other_node_id, other_data_version, other_nonce)

                # Load up our data so we can send it away:
                with open(fp.generate_aes_filepath(name), 'rb') as f:
                    file_data = f.read()
                if file_data is None:
                    scl.send(b'MISCONFIG.')
                    logging.info('#ERROR Listener: Owned data is missing: ' + name + '.aes')
                    scl.close()
                    return

                # All seems okay, proceed with the exchange and send them our data
                #   <<        Send Recv           >>
                other_data = scl.send(file_data)

                # Save other nodes data
                # If we want to store the data in a folder 'per file' instead of a folder per node
                # others_folder = dq.get_shared_data_folder_by_name_and_rsa_pub(other_name, other_file_pub_hex)  # data/1/ for the first data we share, then data/2/, data/3/ ...
                # I now have other: name, origname, data, other files rsa pub. Save off data and update DB
                data_shared_id = dq.insert_data_shared(other_name, other_file_specific_rsa_pub)  # Others data is now stored here for retrieval (by name + pubkey)

                others_folder = dq.get_shared_data_folder_by_name_and_rsa_pub(other_name, other_file_specific_rsa_pub)
                file_folder = os.path.join(get_configuration_value("data_shared_folder"), others_folder) + '/'
                if not os.path.exists(file_folder):
                    os.makedirs(file_folder)

                save_name = os.path.join(str(file_folder), other_name + '.aes').replace('\\', '/')
                #print('Listener: save theirs:', save_name, flush=True)
                #logging.info('Listener: Save off the other file data: ' + str(save_name))
                with open(save_name, 'wb') as f:
                    f.write(other_data)
                #logging.info('Listener: Saved off others data, now store it in database')

                # logging.info('Swicharoo: Insert finger: other_node_id, data_owned_id = ' + str(other_node_id) + str(data_owned_id))
                # Insert a pointer to our data (their node) and their data we share
                dq.insert_finger(other_node_id, data_owned_id, data_shared_id)

                # DONE!
                scl.send(b'OK')
                if dummy_data:
                    logging.info('Swicharoo successful, we used a dummy file.')
                else:
                    logging.info('Swicharoo successful.')

            if request == b'SEND_NODES':  # send back a random node for now  # todo: send back several
                # check if we already have this node, and if not, add it:
                data = data.decode()
                datalist = data.split(',')  # This should be IP/PORT data from caller todo: Escape it!
                if len(datalist) == 3:
                    ip = datalist[0]
                    port = datalist[1]
                    rsa_pub = datalist[2]

                    # This call will silently fail if the node already exists, or it is our pubkey
                    dq.insert_node(ip, port, rsa_pub, 'From Listener SEND_NODES')

                    node_id = dq.get_random_node_id(False, rsa_pub)  # todo: remove the False from the call so we send back only valid nodes
                    if node_id is not None:
                        tmp = dq.get_node_data(node_id)
                        j = {'ip': tmp[0], 'port': tmp[1], 'rsa_pub_key': tmp[2]}
                        # print('Send back node:', tmp[0], tmp[1], tmp[2][:22], flush=True)
                        # print('j:', j)
                        try:
                            r = json.dumps(j)
                        except:
                            print('Could not dumps the dict')
                            r = b'{}'
                        # print('dumps=', r, flush=True)
                        b = r.encode()  # transform to byte object
                        # print('send:', b, flush=True)
                        scl.send(b)
                    else:
                        scl.send(b'{}')
                        #print('We do not have any nodes to share yet!', flush=True)

                else:
                    logging.info('#Warning: SEND_NODES req malformed, len(datalist) =' + str(len(datalist)))

                # print('SEND NODES Done.', flush=True)

            # The node informs us they have changed address
            if request == b'NEWADDRESS':  #
                # check if we already have this node, and if not, add it:
                data = data.decode()
                datalist = data.split(',')  # This should be IP/PORT data from caller todo: Escape it!
                if len(datalist) == 3:
                    #print("NEWADDRESS: new request", flush=True)
                    other_ip = datalist[0]
                    other_port = datalist[1]
                    other_node_pub_key = datalist[2]
                    print("NEWADDRESS: new request other_ip=" + str(other_ip) + " other_port=" + str(other_port), flush=True)

                    # Cheap sanity check
                    if len(other_ip) < 64 and len(other_port) < 10:

                        # Update the node
                        #print("NEWADDRESS: update", flush=True)
                        # Verify it is the right node (with their rsa pub + digest)

                        random_checksum = b'Digest_this!' + str(random.randint(10000, 99999)).encode() + str(time.time()).encode()
                        # query = b'YES_PLEASE' + pack('>Q', my_file_size) + name.encode() + b'|' + my_origfilename.encode() + b'|' + random_checksum
                        #print("NEWADDRESS: send digest test and get back result", flush=True)
                        verification_digest = scl.send(random_checksum)
                        #print("NEWADDRESS: check result", flush=True)
                        digest_ok = rh.verify_digest(random_checksum, RSA.importKey(bytes.fromhex(other_node_pub_key)), verification_digest)

                        if digest_ok:
                            #print("NEWADDRESS: Digest ok", flush=True)

                            # We now know the caller is the righteous holder of the public key, so just update its address
                            #print("NEWADDRESS: get node id", flush=True)
                            other_node_id = dq.get_node_id_from_rsapubkey(other_node_pub_key)
                            if other_node_id:
                                print("NEWADDRESS: update", flush=True)
                                # Update the nodes address
                                dq.update_node_address(other_node_id, other_ip, other_port)
                                scl.send(b'OK')

                                # Check if we use that node to share any of our base_data, if so:
                                # Update the corresponding TR&SUB files to our data

                                # SELECT data_owned_id FROM finger WHERE node_id = ? (other_node_id,)
                                r = dq.get_all_data_node_shares(other_node_id)
                                # Now we have all the data that are affected by this address change
                                # we need to update different files depending on what they are (base file, translation file, substitution file)
                                # Luckily we have a function doing exactly that for us, so just loop the data and call it
                                for data_owned_id in r:
                                    ar.refresh_data_addresses(data_owned_id)

                            else:
                                print("NEWADDRESS: found no node...", flush=True)
                                scl.send(b'Your address not found')
                        else:
                            print("NEWADDRESS: Digest wrong", flush=True)
                            scl.send(b'Digerst error')
                    else:
                        print("NEWADDRESS: request IP or PORT are not valid", flush=True)
                        scl.send(b'Error NEWADDRESS request IP or PORT are not valid')
                else:
                    # Makformed request
                    print("NEWADDRESS: Malformed request", flush=True)
                    scl.send(b'Error Malformed NEWADDRESS request')

            # Close the socket
            client_socket.close()

        except NameError as error:
            logging.info("[NameError] Message not understood: NameError=" + str(error))
        except:
            logging.info("[Uncaught Exception (1/3)] : " + str(sys.exc_info()[0]))
            logging.info("[Uncaught Exception (2/3)] More information: request: " + str(request))
            logging.info("[Uncaught Exception (3/3)] More information: data[:50]: " + str(data[:50]))
            scl.send(b'GeneralError')
        # logging.info('Listen thread done.')

        if scl.is_active():
            scl.close()

    def run(self):
        print('Starting Listener: thread start')
        try:
            # Here we could/should(?) list all network interfaces and their supported protocols...
            ip = get_configuration_value('ip')
            ipversion = typeOfIPAddress(ip)
            if ipversion == 'Invalid':
                self.listensocket = None
                print('#Warning: tried to connect to an invalid ip address:', ip)
                sys.stdout.flush()
                return

            print('IP version detected:', ipversion, '(' + str(ip) + ')')
            sys.stdout.flush()
            # Connect to other node

            # self.listensocket.bind((str(socket.INADDR_ANY), self.port))
            if ipversion == 'IPv4':
                self.listensocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                # To prevent [Errno 98] Address already in use if socket is abandoned and in the TIME_WAIT state
                self.listensocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                self.listensocket.bind(('', get_configuration_value('port')))
            else:
                self.listensocket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
                # To prevent [Errno 98] Address already in use if socket is abandoned and in the TIME_WAIT state
                self.listensocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                self.listensocket.bind(('::', get_configuration_value('port')))

            # Set a timeout to the accept() so that we can shut down gracefully (or actually at all without pkill)
            #self.listensocket.settimeout(2)

            self.listensocket.listen(get_configuration_value('max_simultaneous_connections'))
            self.listensocket.settimeout(get_configuration_value('listen_timeout_seconds'))

        except socket.error as err:
            print('Failed to create listen socket: ', err)
            logging.info('Failed to create listen socket')
            exit()

        # log
        logging.info('Listen on port: ' + str(get_configuration_value('port')))

        global force_shutdown
        while not force_shutdown:

            got_connection = False
            while not force_shutdown and got_connection is False:
                try:
                    # establish connection
                    client_socket, client_address = self.listensocket.accept()
                    # Pass through the while:
                    got_connection = True
                    # Make a new thread that will deal with the communication:
                    if not force_shutdown:
                        # Read request:
                        client_socket.settimeout(60)
                        # Start new thread
                        threading.Thread(target=self.listenthread_secure, args=(client_socket, client_address)).start()

                except socket.error as err:
                    print("listen exception", err)
                    pass


version = get_tenfingers_version()

dt = datetime.datetime.now()
microseconds = dt.microsecond
microseconds = int(microseconds / 10000)
dt = dt.replace(microsecond=0)
dt = str(dt.isoformat()) + "." + str(microseconds)

logging.info('Tenfingers listener startup: V' + version + ' start time: ' + dt)
logging.info('Use --noscheduler to disable scheduler')
logging.info('Listen configuration: Public IP=' + get_configuration_value('ip') + ' Listen port=' + str(get_configuration_value('port')))
logging.info('Share pool maximum size: ' + human_readable_file_size(get_configuration_value('max_storage_space_other_mb') * 1024 * 1024))
logging.info('Maximum share file size: ' + human_readable_file_size(get_configuration_value('swicharoo_file_maxsize_kb') * 1024))
logging.info('Anything under ' + human_readable_file_size(get_configuration_value('swicharoo_minsize_accept_all')) + ' is shared pro bono')

logging.info("Scheduler CPU alleviation rest time (msec):" + str(get_configuration_value('scheduler_wait_msec')))

print('Generate folder if they do not exist')
ar.check_gen_folders()

run_scheduler = True
if len(sys.argv) > 1 and sys.argv[1] == '--noscheduler':
    logging.info('Scheduler will not be started (--noscheduler)')
    run_scheduler = False


time.sleep(0.1)  # So logging will have time to write


# Set up a function that will be called at SIGINT so that a CTRL+C will shut down all threads and exit correctly:
def signal_handler(sig, frame):
    logging.info('Received SIGINT or SIGTERM, shutting down threads.')
    global force_shutdown
    force_shutdown = True
    listener.forcestop()
    sys.exit(0)


# Set up the SIGINT handler
logging.info("Set up the SIGINT handler")
signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)


# set up a thread with the scheduler
scheduler = None
scheduler_thread = None
if run_scheduler:
    scheduler = Scheduler()
    scheduler_thread = threading.Thread(name='Scheduler', target=scheduler.run)
    scheduler_thread.start()
    time.sleep(0.1)  # So logging will have time to write


# Create thread and start up:
listener = Listen()
listener_thread = threading.Thread(name='Listener', target=listener.run)  # l.run2, args=("abc",))

logging.info("start listener thread:")
listener_thread.start()
