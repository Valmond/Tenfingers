import os
import subprocess

import wx
import wx.xrc
import wx.aui


from gui_mydata import GfxMyData
from gui_othersdata import GfxOthersData
from gui_listener import GfxNode
from gui_parameters import GfxParameters


class Notifier:
    @staticmethod
    def notify(message, icon="info"):  # icon can be error, or an icon file
        subprocess.run(
            ["/usr/bin/notify-send", "--icon=" + icon, message])


# Based on the example from:
# https://webtech-training.blogspot.com/2016/02/arrange-tabs-in-layouts-in-wxpython.html


class MainForm(wx.Frame):
    def __init__(self, parent):

        self.m_mgr = None
        # First of all, we need to have a working Tenfingers set  up because the GUI will query it for information and stuff.
        if not self.is_listener_setup():
            if not self.do_the_setup():

                subprocess.run(["/usr/bin/notify-send", "--icon=error", "Tenfingers setup.py cannot run properly, bailing out :-/ ..."])
                subprocess.run(["/usr/bin/notify-send", "--icon=error", "Check if you have the rights to execute it."])
                exit(-1)

        # Construct az name with the base folder where we are. eg.:  desktop/tenfingers/bin/  -> "bin"
        base_folder = os.path.basename(os.path.normpath(os.path.basename(os.path.normpath(os.path.realpath('./')))))

        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=u"Tenfingers (" + base_folder + ")", pos=wx.DefaultPosition,
                          size=wx.Size(900, 600), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        self.m_mgr = wx.aui.AuiManager()
        self.m_mgr.SetManagedWindow(self)
        self.m_mgr.SetFlags(wx.aui.AUI_MGR_DEFAULT)
        self.m_auinotebook1 = wx.aui.AuiNotebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                 wx.aui.AUI_NB_DEFAULT_STYLE)
        self.m_mgr.AddPane(self.m_auinotebook1, wx.aui.AuiPaneInfo().Left().CaptionVisible(False).PinButton(
            True).Dock().Resizable().FloatingSize(wx.DefaultSize).CentrePane())
        self.m_panel1 = wx.Panel(self.m_auinotebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_auinotebook1.AddPage(self.m_panel1, u"My data", False)
        self.m_panel2 = wx.Panel(self.m_auinotebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_auinotebook1.AddPage(self.m_panel2, u"Others data", False)
        self.m_panel3 = wx.Panel(self.m_auinotebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_auinotebook1.AddPage(self.m_panel3, u"Node", False)
        self.m_panel4 = wx.Panel(self.m_auinotebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.m_auinotebook1.AddPage(self.m_panel4, u"Parameters", False)

        # Make that fun layout
        #self.m_auinotebook1.Split(0,wx.LEFT)
        #self.m_auinotebook1.Split(1,wx.LEFT)
        #self.m_auinotebook1.Split(2,wx.BOTTOM)

        self.gfx_mydata = GfxMyData(self.m_panel1)
        self.gfx_others_data = GfxOthersData(self.m_panel2)
        self.gfx_node = GfxNode(self.m_panel3)
        self.gfx_parameters = GfxParameters(self.m_panel4)

        self.m_mgr.Update()
        self.Centre(wx.BOTH)

    def __del__(self):
        if self.m_mgr is not None:
            self.m_mgr.UnInit()

    @staticmethod
    def is_listener_setup():  # First time, so that we can propose (or just run it automatically) the setup run
        return os.path.isfile("./db/public_key") and os.path.isfile("./db/secret_key") and os.path.isfile("./db/tenfingers.db")

    def do_the_setup(self):
        subprocess.run(["/usr/bin/notify-send", "--icon=info", "Tenfingers setup running, please wait a little bit ..."])  # We can use our icon here
        # Do the setup...
        # WARNING this runs the setup check whatever you do. It's okay but ugly (it's there for the frozen code)
        from setup import setup
        # Run  it again to see all is OK
        setup()
        print("Setup done.", flush=True)
        return self.is_listener_setup()  # Returns True on success


if __name__ == "__main__":
    app = wx.App(False)
    frame = MainForm(None)
    frame.Show()
    app.MainLoop()
