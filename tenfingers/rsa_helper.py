# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

try:
    from Cryptodome.PublicKey import RSA
    from Cryptodome import Random
    from Cryptodome.Cipher import PKCS1_OAEP
    from Cryptodome.Signature import PKCS1_v1_5
    from Cryptodome.Hash import SHA512
except ImportError:
    print("Error 3: You need to install Cryptodome:\npip3 install pycryptodomex\n")
    exit(-7)

import hashlib
import database_query as dq


# Saves an RSA key file as an encoded file
def save_key(filename, key):
    try:
        f = open(filename, 'wb')
        f.write(key.exportKey('DER'))
        f.close()
    except IOError as err:
        print("Exception raised at save key file", err)
        return False
    return True


# Loads an RSA key file from an encoded file
def load_key(filename):
    try:
        with open(filename, 'rb') as content_file:
            content = content_file.read()
            return RSA.importKey(content)
    except IOError as err:
        print("Exception raised at load key file", err)
    return None


# Gets the unique RSA keypair from the database or None if not existing. This function returns fully functional keys!
def get_rsa_keypair(noerror=False):  # Returns prv, pub
    prv, pub = dq.get_rsa_hex_keys()
    if prv is not None:
        return RSA.importKey(bytes.fromhex(prv)), RSA.importKey(bytes.fromhex(pub))
    else:
        if noerror:
            return None
        print('#Error: RSA Keypair not found in database!')
        exit(-7)


#


# Generates a RSA keypair
def generate_rsa_keypair(keysize):
    print("Generating RSA Key pair, please wait...")
    random_generator = Random.new().read
    prv = RSA.generate(keysize, random_generator)
    # prv = RSA.generate(keysize, 65537, callback=lambda x, y, z: None)
    pub = prv.publickey()
    return prv, pub


# Returns a short byte string
def _sha(message):
    if not isinstance(message, bytes):
        message = message.encode()
    return hashlib.sha256(message).hexdigest().encode()


# Returns a block of raw bytes
def raw_sha256(data):
    return bytes.fromhex(hashlib.sha256(data).hexdigest())


# creates a digest, extracts the number and returns a hex version of it (of type bytes so we can easily send it over the network)
def create_digest(message, rsa_prv_key):
    if not isinstance(message, bytes):
        print('Warning: create_digest was called with non-bytes variable: auto encodes it to bytes')
        message = message.encode()

    signer = PKCS1_v1_5.new(rsa_prv_key)
    digest = SHA512.new()
    digest.update(message)
    return signer.sign(digest)


# digest must be in hex-numbers byte form
def verify_digest(message, rsa_pub_key, signature):
    signer = PKCS1_v1_5.new(rsa_pub_key)
    digest = SHA512.new()
    digest.update(message)
    return signer.verify(digest, signature)


# creates a digest, extracts the number and returns a hex version of it (of type bytes so we can easily send it over the network)
def create_digest_old(message, rsa_prv_key):
    if not isinstance(message, bytes):
        print('Warning: create_digest was called with non-bytes variable: auto encodes it to bytes')
        message = message.encode()
    message = _sha(message)
    d = rsa_prv_key.sign(message, '')
    d0 = d[0]
    h = hex(d0)
    b = h.encode()
    return b


# digest must be in hex-numbers byte form
def verify_digest_old(message, rsa_pub_key, digest):
    # Check if the key id str, bytes, real key (we need a real key)
    message = _sha(message)
    h = digest.decode()
    d0 = int(h, 16)
    d = (d0,)
    return rsa_pub_key.verify(message, d)


# use this to validate / create digests for addresses
def _make_address_string(ip, port, pub_key):  # Just concats the data so we can use create digest and verify easily
    if not isinstance(ip, bytes):
        ip = ip.encode()
    if not isinstance(port, bytes):
        port = port.encode()
    if not isinstance(pub_key, bytes):
        pub_key = pub_key.encode()
    return ip + b'_' + port + b'_' + pub_key + b'_end'


# Use this to create a universal digest for an address
def create_address_digest(rsa_prv_key, ip, port, pub_key):
    return create_digest(_make_address_string(ip, port, pub_key), rsa_prv_key)


# Use this to validate an address
def verify_address_digest(bmessage, digest, pub_key):
    return verify_digest(bmessage, pub_key, digest)


# Calculates and writes the auth data in the data ( byte 8..39, 32 bytes)
#def insert_authentication_data(data, rsa_priv):
#    auth = _create_32b_authentification_signature(data, rsa_priv)
#    # len should be 32
#    if not len(auth) == s_authentication_data_size:
#        print('#ERROR: size of authentication data is not', s_authentication_data_size, ' it is ', len(auth))
#    for i in range(0, s_authentication_data_size):
#        data[s_random_aes_bytes + i] = auth[i]


# makes a string that can be used to autentificate a specific data
# Used to ensure a node have delivered the originally published data
# This is only for when emitting data, as it encrypts with the private key
# data_version so a node cannot try to maliciously serve an old dataset masquerading as a new one
def _old__create_authentification_signature(data, data_version, rsa_priv):
    if len(data) < 32:
        pass  # Must pad in some secret way?
    # Make an SHA so that we do not need to encrypt a large data with RSA
    raw_sha = raw_sha256(data + str(data_version).encode())
    # Encrypt
    print('Generate an auth-string with ', rsa_priv)
    #dig = encrypt(raw_sha, rsa_priv)
    #print('Generated a digest. len=', len(dig))
    #return dig


# Check if the data has been signed off with the rsa_priv key by checking it with its pub key
def _old__verify_authentification_signature(data, data_version, rsa_pub, signature):
    # Generate the digest, exactly like how we generated it when
    raw_sha = raw_sha256(data + str(data_version).encode())  # This is wat was

    # Uncrypt
    auth_string = ''

    return auth_string == raw_sha















