# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import os
import sys
import database_query as dq
from configuration import get_configuration_value
import sqlite3
from sqlite3 import Error
import shutil
import filepath as fp


"""
Creates a link file from existing data (that was created with the insert_data.py)
"""


def print_help():
    print('Usage:')
    print('')
    print('python3 get_link_from_data.py dataname')
    print('python3 get_link_from_data.py --testrun')

    sys.exit()


# Returns the connection so we don't need to open it again, or None if it fails
def generate_link_database(database_file_name):
    # delete any old db
    if os.path.isfile(database_file_name):
        os.remove(database_file_name)

    # Generate the database
    try:
        connection = sqlite3.connect(database_file_name)
    except Error:
        print("Could not create the (database) link:")
        print(Error)
        return None

    connection.execute('''CREATE TABLE information
             (
              name                  TEXT,
              file_version          INTEGER DEFAULT 1,
              key                   TEXT,
              iv                    INTEGER,
              data_rsa_pub          TEXT
               );''')

    # The addresses where we'll try to fetch the data
    # address_list.append({'rsa_pub': pub, 'ip': ip, 'port': port})
    connection.execute('''CREATE TABLE address
             (
              rsa_pub       TEXT,
              ip            TEXT,
              port          TEXT
             );''')

    return connection


def set_information_in_database(connection, name, version, aes_key, aes_iv, data_rsa_pub_key):
    cursor = connection.cursor()
    cursor.execute('INSERT INTO information (name, file_version, key, iv, data_rsa_pub) VALUES(?, ?, ?, ?, ?)', (name, version, aes_key, aes_iv, data_rsa_pub_key))
    connection.commit()


def add_address_to_database(connection, rsa_pub_key, ip, port):
    cursor = connection.cursor()
    cursor.execute('INSERT INTO address (rsa_pub, ip, port) VALUES(?, ?, ?)', (rsa_pub_key, ip, port))
    connection.commit()


# Takes 'Name' entry from global database and generates a link named 'name'.10f which contains
# This link is a SQLite database and contains two tables;
# One for global information: IP, PORT, NAME, ORIGFILENAME, AES&IV, RSA_PUB
# (AES is stored in 64 bytes hex string)
# One address entry for each finger storing the data we are generating the link for.
# If version == 0 then the latest version for this data will be used
# if exclude_our_address == True then we will not add our own address to the link, only other nodes links
# Fingers are all the fingers which addresses we shall add to the .10f database
# if move_to_folder is a folder, the tenfingers file will be moved there, otherwise it is created in the tenfingers source folder
def _create_and_save_link_database(name, exclude_our_address, fingers, move_to_folder='', silent=False):
    row = dq.get_link_data_by_name(name)
    key_pair = dq.get_rsa_hex_keys()

    if row is None or key_pair is None:
        if not silent and row is None:
            print('#Error: Link not found in datbase:', name)
        if not silent and key_pair is None:
            print('#Error: RSA keys not found in datbase.')
        return

    # Get data id from global database:
    data_owned_id = dq.get_link_data_id_by_name(name)

    # get the latest data_version
    data_version = dq.get_owned_data_version(data_owned_id)

    connection = generate_link_database(name + '.10f')
    if connection is None:
        return

    prv, pub = key_pair

    # Update the database link with 'my' address information
    if not exclude_our_address:
        add_address_to_database(connection, pub, get_configuration_value('ip'), get_configuration_value('port'))

    # print("Fingers:", len(fingers), flush=True)

    fingers_len = len(fingers)
    for n in range(0, fingers_len):
        finger_id = fingers[n]
        # add to the address list:
        node_id, _ = dq.get_finger_data(finger_id)
        if node_id != 0:
            # get rsa_pub, ip, port from node and add it to the address list
            node_data = dq.get_node_data(node_id)
            # Update the link with address information
            add_address_to_database(connection, node_data[2], node_data[0], node_data[1])

    # data_rsa_pub is, together with the name, the unique identifier for this data
    data_rsa_pub = dq.get_rsa_key_by_data_id(data_owned_id)

    # Update the link with information
    set_information_in_database(connection, name, data_version, row[1], row[2], data_rsa_pub)

    tf_name = name + '.10f'
    if not silent:
        print('Link created:', tf_name)

    if len(move_to_folder) and os.path.isdir(move_to_folder):
        shutil.move(os.path.join("./", tf_name), os.path.join(move_to_folder, tf_name))


# We now generate all the tenfingers files
# if move_to_folder is a folder, the tenfingers file will be moved there, otherwise it is created in the tenfingers source folder
def create_and_save_link_database(data_name, move_to_folder='', silent=False):
    exclude_our_address = dq.get_afar_flag_from_name(data_name)

    # Get data id from global database:
    data_owned_id = dq.get_link_data_id_by_name(data_name)
    translation_data_id = dq.get_link_data_id_by_name(fp.generate_trans_name(data_name))
    substitution_data_id = dq.get_link_data_id_by_name(fp.generate_sub_name(data_name))

    # Get a list of finger ids for the data we want to share and for the addresslist we want to share
    data_fingers = dq.get_all_fingers(data_owned_id)
    translation_fingers = dq.get_all_fingers(translation_data_id)
    substitution_fingers = dq.get_all_fingers(substitution_data_id)
    if not silent and len(data_fingers) == 0:
        print('Warning: This data have not yet been uploaded elsewhere! If your PC is shut down, your link will not function until other nodes pick up the slack.')

    if not silent:
        print("Nodes sharing the data:", data_fingers)
        print("Nodes sharing the address translation file:", translation_fingers)

    # Make the data link
    _create_and_save_link_database(data_name, exclude_our_address, data_fingers, move_to_folder, silent)
    if len(translation_fingers) == 0 and len(substitution_fingers) == 0 and exclude_our_address:
        if not silent:
            print('Warning: Translation address list has not been uploaded elsewhere. Your link will expire if nodes change addresses.')
    else:
        # Make the address translator link, which contains the list of addresses used in the data link
        _create_and_save_link_database(fp.generate_trans_name(data_name), exclude_our_address, translation_fingers, move_to_folder, silent)
        # Make the address substitution link, which contains the list of addresses used in the data link
        _create_and_save_link_database(fp.generate_sub_name(data_name), exclude_our_address, substitution_fingers, move_to_folder, silent)
