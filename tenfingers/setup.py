# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

# Run this to setup.
# Will not harm existing installations
# Generates RSA keypair, database etc.

import sys
import platform

import rsa_helper as rh
import generate_database as gdb
import database_connection as dc
import database_query as dq


def setup():
    print('[setup.py]')
    print('Welcome to the 1-2-10 finger setup!')

    # gen database?
    if not dc.database_exists():
        print('setup: generate database')
        gdb.create_software_database()

    # RSA keys exist?
    r = rh.get_rsa_keypair(True)
    if r is None:
        # Is there a public_key/secret_key file keypair? If so use them
        textkey_prv = None
        textkey_pub = None
        try:
            with open('./db/secret_key', 'r') as f:
                textkey_prv = f.read()
            with open('./db/public_key', 'r') as f:
                textkey_pub = f.read()
        except FileNotFoundError:
            pass

        if textkey_prv is None or textkey_pub is None:
            print('setup: generate RSA keys')
            # Generate them:
            prv, pub = rh.generate_rsa_keypair(4096)
            textkey_prv = prv.exportKey('DER').hex()
            textkey_pub = pub.exportKey('DER').hex()
            # insert in database
            dq.set_rsa_hex_keys(textkey_prv, textkey_pub)
            # Save off copy (this can be used for test systems, or for commanding our files from a distance) a backup
            with open('db/secret_key', 'w') as f:
                f.write(textkey_prv)
            with open('db/public_key', 'w') as f:
                f.write(textkey_pub)
        else:
            print('setup: reuse existing RSA keys')
            dq.set_rsa_hex_keys(textkey_prv, textkey_pub)

    else:
        print('setup: load RSA keys')
        prv, pub = r

    # Try to get our public ip address
    from helpers import get_public_ip
    my_public_ip = get_public_ip()

    executable = './10f'
    if platform.system() == 'Windows':
        executable = './10f.exe'

    dc.update("UPDATE configuration_string SET value = ? WHERE name = ?", (my_public_ip, 'ip'))

    from configuration import get_configuration_value
    print('IMPORTANT NOTICE!')
    print('Your public ip address was set to:', my_public_ip)
    print('Your public port was set to:', get_configuration_value('port'))

    print('You need to verify if this is the ip:port you want to use,')
    print('and route the incoming traffic to this local PC!')
    print('To change ip or port, use these commands:')

    print(executable + ' -p ip 123.1.2.3')
    print(executable + ' -p port 20000')

    print('')
    print('You can check the routing by starting the listener and run:')
    print(executable + ' -test')


setup()






























