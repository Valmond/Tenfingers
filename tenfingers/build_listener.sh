# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

echo '------------- Build the listener ----------------'

################################################
# Build a listener only

rm dist/listener
rm dist/listener.exe

# Clean up the package folder
rm -f -r ./tenfingers_release/*
mkdir tenfingers_release
mkdir tenfingers_release/py_source

# Copy data
echo '[Copy python scripts]'
cp *.py tenfingers_release/py_source
echo '[Copy pducumentation]'
cp "../instructions/install.txt" tenfingers_release
cp "../instructions/linux commandline install.txt" tenfingers_release
cp "../instructions/troubleshooting.txt" tenfingers_release
echo '[Copy copyright]'
cp ../COPYING tenfingers_release


echo '[Clean up build directories]'

rm -f -r dist/*

echo '[make executables:]'

if [ $(uname -o) == "Msys" ]
then
	echo 'Build Windows version'
	venv/Scripts/pyinstaller.exe listener.py --clean --onefile
else
	echo 'Build Linux version'
	pyinstaller listener.py --clean --onefile
fi


#pyinstaller setup.py --clean --onefile
#pyinstaller 10f.py --clean --onefile

rm listener.spec
rm setup.spec
rm 10f.spec

mkdir tenfingers_release/bin
cp -f -u dist/listener* ./

echo '[Done.]'

