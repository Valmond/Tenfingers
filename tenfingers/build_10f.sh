# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

echo '------------- Build the command line interface ----------------'

################################################
# Build a listener only

rm dist/10f
rm dist/10f.exe
rm 10f
rm 10f.exe

# Clean up the package folder
rm -f -r ./tenfingers_release/*
mkdir tenfingers_release
mkdir tenfingers_release/py_source


echo '[Clean up build directories]'

rm -f -r dist/*

echo '[make executables:]'

if [ $(uname -o) == "Msys" ]
then
	echo 'Build Windows version'
	venv/Scripts/pyinstaller.exe 10f.py --clean --onefile
else
	echo 'Build Linux version'
	pyinstaller 10f.py --clean --onefile
fi

rm listener.spec
rm setup.spec
rm 10f.spec

mkdir tenfingers_release/bin
cp -f -u dist/10f* ./

echo '[Done.]'

