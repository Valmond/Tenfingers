import sys,os
import subprocess
import signal
import time


print('Run the tests suite (Listener must run in bash: python3 listener.py)')



# run the tests
print('#Setup:')
exec(open("./setup.py --testrun").read())

print('#Generate Database:')
exec(open("./generate_database.py --testrun").read())

print('#Insert Data:')
os.system("python3 insert_data.py --testrun")

print('#Extract Link for inserted data::')
os.system("python3 get_link_from_data.py --testrun")

print('#Get data from extracted link:')
os.system("python3 get_data_from_link.py --testrun")


