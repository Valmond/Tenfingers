# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import database_connection as dc

"""
This helper function serves configuration data
The data is stored in the database
"""


def get_configuration_value(name, default=None):
    # If the 10f is stand alone, there might be no database
    if not dc.database_exists():
        return default

    value = dc.query_one("SELECT value FROM configuration_string WHERE name = ?", (name,))
    if value is not None:
        return value
    else:
        value = dc.query_one("SELECT value FROM configuration_int WHERE name = ?", (name,))
        if value is not None:
            return int(value)
        else:
            print('#WARNING, could not find configuration value, uses default value[', default, '], entry missing=', name)
            return default


def set_int_configuration_value(name, value):
    dc.update("UPDATE configuration_int SET value = ? WHERE name = ?", (value, name))


# Gets all the 'exclude_ip_list' regex from the 'configuration_string' table
def get_exclude_ip_list():
    rows = dc.query_all("SELECT value FROM configuration_string WHERE name = ?", ('exclude_ip_list',))
    r = []
    for data in rows:  # maybe this is not necessary... but as it should work, lets check that later
        r.append(data[0])
    return r
