# Copyright (C) 2023 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)



# -l => show all chatters we follow
# -al link.10f  => add a link file (must be a .10f file)  TODO (maybe generate the 10f file automatically?!)
#

import os
import sys
import argparse
import shutil
import tarfile
from contextlib import contextmanager

import access as ac
from get_data_from_link import extract_data_from_database_link

s_chat_name = 'chat_data'
s_base_dir = './' + s_chat_name  # base dir and name of share


def get_base_dir():
    return s_base_dir


# If not done, do setup
def check_setup():
    if not os.path.isdir(s_base_dir):
        # make a /chat folder
        os.makedirs(s_base_dir)
        os.makedirs(s_base_dir + '/me')
        # in it, we will make a txt file for each tect chat, starting from 1
        # a link will get a folder, I guess, as it vcontains more stuff
        ac.insert_folder(s_base_dir, './', s_base_dir + '/me')

        #
        # make an 'others' folder
        os.makedirs(s_base_dir + '/others')
        # it will contain a folder for each 'other' address you added
        #  as it will have the chat.10f, and expand 1.txt, 2.txt. /3/ etc
        # Make out chat link (to change with other chatters)
        # ./10f -i -f ./ chat


def get_index(user):
    index = 0
    index_file = os.path.join(s_base_dir, user, '_index.txt')
    if os.path.exists(index_file):
        with open(index_file, 'r') as f:
            index = f.readline()
            if index.isdigit():
                index = int(index)
    return index


def set_index(user, index):
    index_file = os.path.join(s_base_dir, user, '_index.txt')
    with open(index_file, 'w') as f:
        f.write(str(index))


# Okay so this func does two things, but it's kind of useful
# gets (and saves off) next free index
def get_next_index(user):
    index = get_index(user) + 1
    set_index(user, index)
    return index


# If not done, do setup
def check_setup():
    if not os.path.isdir(s_base_dir):
        # make a /chat folder
        os.makedirs(s_base_dir)
        os.makedirs(s_base_dir + '/me')
        # in it, we will make a txt file for each tect chat, starting from 1
        # a link will get a folder, I guess, as it vcontains more stuff
        ac.insert_folder(s_base_dir, './', s_base_dir + '/me')

        #
        # make an 'others' folder
        os.makedirs(s_base_dir + '/others')
        # it will contain a folder for each 'other' address you added
        #  as it will have the chat.10f, and expand 1.txt, 2.txt. /3/ etc
        # Make out chat link (to change with other chatters)
        # ./10f -i -f ./ chat


def add_text_and_share(text):
    # Get next index
    index = get_next_index('me')
    file_name = s_base_dir + '/me/' + str(index) + '.txt'
    with open(file_name, 'w') as f:
        f.write(str(text))
    update_my_chat_log()


def add_file_as_link_and_share(file_path_name):
    file_path_name = file_path_name.replace('\\', '/')
    file_name = os.path.basename(file_path_name)
    base_path = file_path_name[:-(len(file_name)+1)]
    print("Add a file link for:", file_path_name)
    print(file_name)
    print(base_path)

    # Get next index
    index = get_next_index('me')

    chat_item_name = str(index) + '_chat_link'

    # Insert a new 10f file
    ac.insert_data(chat_item_name, base_path, file_name)

    # export its links
    from get_link_from_data import create_and_save_link_database
    create_and_save_link_database(chat_item_name)

    # moce/rename them to the
    chatter_dir = os.path.join(s_base_dir, 'me')
    shutil.move('./' + chat_item_name + '.10f', os.path.join(chatter_dir, chat_item_name + '.10f'))
    shutil.move('./' + chat_item_name + '_tr.10f', os.path.join(chatter_dir, chat_item_name + '_tr.10f'))
    shutil.move('./' + chat_item_name + '_sub.10f', os.path.join(chatter_dir, chat_item_name + '_sub.10f'))

    # add them to the chat log (all 3!)
    update_my_chat_log()


# Reloads the chat logs 10f file
def update_my_chat_log():
    ac.insert_folder(s_chat_name, os.path.join(s_base_dir, 'me'), './')


# exports our chat tenfilger file and renames it accordingly
def save_share_link(file_name):
    from get_link_from_data import create_and_save_link_database
    create_and_save_link_database(s_chat_name)

    # Files to generqte, and tar and then remove
    filenames = ['./' + s_chat_name + '.10f', './' + s_chat_name + '_tr.10f', './' + s_chat_name + '_sub.10f']

    source_dir = './'
    with tarfile.open(file_name, "w:gz") as tar:
        #tar.add(source_dir, arcname=os.path.basename(source_dir))
        for filename in filenames:
            tar.add(filename)
            os.remove(filename)


# Easy peasy change dir and back
@contextmanager
def cwd(path):
    oldpwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(oldpwd)


# Get the chat log for a user: (does not work for 'me')
# returns a dict of numbers and their chat line/link
def get_chat_log(user):
    r = {}
    idx = 1
    if user == 'me':
        chatter_dir = os.path.join(s_base_dir, 'me')
        index = get_index('me')
    else:
        chatter_dir = os.path.join(s_base_dir, 'others', user)
        chat_link = os.path.join(chatter_dir, s_chat_name + '.10f')
        # todo: download it in situ, refact and use @contextmanager def cwd(path)  ??? works already ???
        ac.download(chat_link, chatter_dir)
        index = get_index(os.path.join('others/', user))

    for i in range(1, index + 1):
        # Is it a text file?
        text_file = os.path.join(chatter_dir, str(i) + '.txt')
        if os.path.exists(text_file):
            with open(text_file, "r") as f:
                t = f.read()
        else:
            link_file = os.path.join(chatter_dir, str(i) + '_chat_link.10f').replace('\\', '/')
            if os.path.exists(link_file):
                info, _ = extract_data_from_database_link(link_file)
                t = "<LINK> [originalfilename]  download with:  ./10f.exe " + link_file
            else:
                t = "<Unknown>"

        print(t)
        r[idx] = t
        idx += 1

    return r
