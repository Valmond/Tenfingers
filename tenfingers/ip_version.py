# helper telling if a string is a valid IPv4, IPv6 or invalid internet address

from ipaddress import ip_address, IPv4Address
  
def typeOfIPAddress(IP: str) -> str:
    try:
        return "IPv4" if type(ip_address(IP)) is IPv4Address else "IPv6"
    except ValueError:
        return "Invalid"
