# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

echo '------------- Builds a release version ----------------'

################################################
# Build a release version

# remove any old installers:
mv tenfingers*.tar.gz _old_installs

# Clean up the package folder
rm -f -r ./tenfingers_release/*
mkdir tenfingers_release
mkdir tenfingers_release/py_source

# Copy data
echo '[Copy python scripts]'
cp *.py tenfingers_release/py_source
echo '[Copy pducumentation]'
cp "../instructions/install.txt" tenfingers_release
# cp "../instructions/usage instructions.txt" tenfingers_release  TOO OLD FOR NOW
cp "../instructions/linux commandline install.txt" tenfingers_release
cp "../instructions/troubleshooting.txt" tenfingers_release
echo '[Copy copyright]'
cp ../COPYING tenfingers_release


echo '[Clean up build directories]'

rm -f -r dist/*

echo '[make executables:]'

if [ $(uname -o) == "Msys" ]
then
	echo 'Build Windows version'
	# If there are weird problems, just add --clean to one of those: (and remove the & as you can't run in parallel then)
	# or just delete C:\Users\USERNAME\AppData\Roaming\pyinstaller\*
    # Change this to point to where pyinstaller has been installed (to install: pip3 install pyinstaller)
	C:/Python312/Scripts/pyinstaller.exe 10f.py --onefile&
	C:/Python312/Scripts/pyinstaller.exe listener.py --onefile&
	C:/Python312/Scripts/pyinstaller.exe setup.py --onefile&
	# Wait for the subprocesses to end
	wait
else
	echo 'Build Linux version'
	pyinstaller 10f.py --clean --onefile
	pyinstaller listener.py --clean --onefile
	pyinstaller setup.py --clean --onefile
	# Wait for the subprocesses to end
	wait
fi


rm listener.spec
rm setup.spec
rm 10f.spec

mkdir tenfingers_release/bin
cp -f -u dist/* tenfingers_release/bin

cp "../instructions/Windows Install.odt" tenfingers_release

rm -f tenfingers.tar.gz
tar -cvzf tenfingers.tar.gz tenfingers_release/*

# Add todays date
if [ $(uname -o) == "Msys" ]
then
    mv tenfingers.tar.gz tenfingers_Windows_$(date +%Y%m%d-%Hh%Mm).tar.gz
else
    mv tenfingers.tar.gz tenfingers_Linux_$(date +%Y%m%d-%Hh%Mm).tar.gz
fi



echo '[Done.]'

