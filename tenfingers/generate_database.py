# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)
import os.path
import sys
from pathlib import Path

import database_connection as dc

"""
Creates the SQLite database and its associated tables
"""


# This database holds all the Nodes and all the swicharoo packages and the fingers pointing on our packages
def create_software_database():
    if dc.database_exists():
        print('The database already exists.')
        exit(-1)

    if not os.path.exists(dc.get_database_file_path()):
        Path(dc.get_database_folder()).mkdir(parents=True, exist_ok=True)

    print('Creating database:')

    connection = dc.get_connection(False)
    if connection is not None:
        print('Creating tables:')
        
        # Table for keeping track of other nodes: (unix time is seconds since 1970, 1jan 00:00:00)
        # our_ip and our_port is what the node think is our address, (like our_ip_according_to_this_node but that name is kind of long)
        # this is used when we change IP and/or PORT, so that we know we went off the radar on them.
        # The scheduler will figure that out and send our new IP:PORT to them.
        connection.execute('''CREATE TABLE node
                 (id INTEGER PRIMARY KEY AUTOINCREMENT,
                 ip         CHAR,
                 port       CHAR,
                 pubkey     CHAR,
                 connection_tries INTEGER DEFAULT 0,
                 connection_fails INTEGER DEFAULT 0,
                 last_connection_unixtime INTEGER DEFAULT 0,
                 our_ip         CHAR DEFAULT 'unknown',
                 our_port       CHAR DEFAULT 'unknown',
                 rem        CHAR
                  );''')
                  
        # Create two tables that will hold the data for fingers.
        # These tables will hold all our files we share, plus all files we share fo other users.

        # Create a table that will hold data we want to share
        # data_version is 1 for the original data, when refreshing, increment with 1 so fingers get stale and scheduler will send new data to sharing nodes
        # address_translation_file is for, you know it! Used so that we can update them automatically when our, or any other concerned node address changes
        # Note: there are two translation files pointing on each other (plus containing the addresses of the base link)
        # _tr = 1
        # _sub = 2
        #  should point on another data_owned file (so we know how to deal with stuff)  ?? Right or wrong?
        # iv is the nonce we used when encrypting the data,
        # next_safe_iv is the next safe nonce to use with this specific key
        connection.execute('''CREATE TABLE data_owned
                 (id INTEGER PRIMARY KEY AUTOINCREMENT,
                 name               CHAR,
                 address_translation_file CHAR DEFAULT 0,
                 key                CHAR,
                 iv                 INTEGER DEFAULT 0,
                 next_safe_iv       INTEGER DEFAULT 0,
                 privkey            CHAR,
                 pubkey             CHAR,
                 data_version       INTEGER DEFAULT 1,
                 hide_my_address    INTEGER DEFAULT 0,
                 wanted_fingers     INTEGER DEFAULT 10,
                 unique (name)
                  );''')

        # Create a table that holds data that other nodes wants us to share
        # PRIMARY KEY = ROWID, a 64-uint
        connection.execute('''CREATE TABLE data_shared
                 (id INTEGER PRIMARY KEY AUTOINCREMENT,
                 name               CHAR,
                 pubkey             CHAR,
                 data_version INTEGER DEFAULT 1,
                 iv                 INTEGER DEFAULT 0,
                 folder             INTEGER,
                 unique (name, pubkey)
                  );''')

        # Table for keeping track of who is keeping and sharing my files, points on 'node'(the node keeping the data) & 'data_owned' (my data)
        # data_shared_id is what we share for the other node (so we can delete that data when deleting a finger)
        # Typically we would like to have 10 entries for each entry in the finger table that points on a local file (our file) in 'data'
        # data_version is for checking the data_owned is not stale
        # the tries/fails are for:
        # connection: just a connection to the node
        # data_integrity: checks they have the right version + the actual data (demands a small sample)
        # WARNING: This will fail when the node doesn't have the right version! So don't prune over agressively
        connection.execute('''CREATE TABLE finger
                 (id INTEGER PRIMARY KEY AUTOINCREMENT,
                 node_id         INTEGER DEFAULT 0,
                 data_owned_id   INTEGER DEFAULT 0,
                 data_shared_id  INTEGER DEFAULT 0,
                 data_version INTEGER DEFAULT 1,
                 connection_tries INTEGER DEFAULT 0,
                 connection_fails INTEGER DEFAULT 0,
                 data_integrity_tries INTEGER DEFAULT 0,
                 data_integrity_fails INTEGER DEFAULT 0,
                 last_connection_unixtime INTEGER DEFAULT 0
                  );''')

        # My RSA keypair (just one entry, for now)
        connection.execute('''CREATE TABLE rsa
                 (id INTEGER PRIMARY KEY AUTOINCREMENT,
                 privkey    CHAR,
                 pubkey     CHAR
                  );''')

        # Configuration data
        connection.execute('''CREATE TABLE configuration_string
                 (name    CHAR,
                 value     CHAR
                  );''')

        connection.execute('''CREATE TABLE configuration_int
                 (name    CHAR,
                 value     INTEGER
                  );''')


        print('Database created.')

        set_database_default_values()
        print('Default configuration values inserted, edit with -s.')

    else:
        print('There were errors.')


def set_database_default_values():
    if not dc.database_exists():
        print('The database does not yet exists.')
        return

    connection = dc.get_connection(False)
    if connection is not None:
        # Insert default values:
        cursor = connection.cursor()

        # Truncate tables:
        cursor.execute("DELETE FROM configuration_string")
        cursor.execute("DELETE FROM configuration_int")

        # Configuration strings:
        cursor.execute("INSERT INTO configuration_string ('name', 'value') VALUES ('ip', '127.0.0.1')")
        cursor.execute("INSERT INTO configuration_string ('name', 'value') VALUES ('unsecure_nick_name', 'Alice')")
        cursor.execute("INSERT INTO configuration_string ('name', 'value') VALUES ('swicharoo_policy', 'GRANT_ALL')")
        # folder to hold data:
        cursor.execute("INSERT INTO configuration_string ('name', 'value') VALUES ('data_owned_folder', 'data_owned')")  # Where my data will be stored
        cursor.execute("INSERT INTO configuration_string ('name', 'value') VALUES ('data_owned_translation_folder', 'translation_files')")  # Translation and Substitution files
        cursor.execute("INSERT INTO configuration_string ('name', 'value') VALUES ('data_shared_folder', 'data_shared')")  # What we share for others

        # Exception to the rule: We can have several of these exclude regex:
        cursor.execute("INSERT INTO configuration_string ('name', 'value') VALUES ('exclude_ip_list', 'nothing')")


        # Configuration integers

        # Listener socket
        cursor.execute("INSERT INTO configuration_int ('name', 'value') VALUES ('port', 46000)")
        cursor.execute("INSERT INTO configuration_int ('name', 'value') VALUES ('listen_read_bytes', 10000)")
        cursor.execute("INSERT INTO configuration_int ('name', 'value') VALUES ('max_simultaneous_connections', 40)")
        cursor.execute("INSERT INTO configuration_int ('name', 'value') VALUES ('listen_timeout_seconds', 30)")
        cursor.execute("INSERT INTO configuration_int ('name', 'value') VALUES ('server_send_package_size', 32768)")
        # Sizes
        cursor.execute("INSERT INTO configuration_int ('name', 'value') VALUES ('max_storage_space_other_mb', 16384)")
        cursor.execute("INSERT INTO configuration_int ('name', 'value') VALUES ('swicharoo_minsize_accept_all', 65536)")
        cursor.execute("INSERT INTO configuration_int ('name', 'value') VALUES ('swicharoo_file_maxsize_kb', 1048576)")
        # Number of fingers wanted
        cursor.execute("INSERT INTO configuration_int ('name', 'value') VALUES ('fingers_wanted_our_data', 10)")
        cursor.execute("INSERT INTO configuration_int ('name', 'value') VALUES ('fingers_wanted_translation_file', 12)")
        cursor.execute("INSERT INTO configuration_int ('name', 'value') VALUES ('fingers_wanted_substitution_file', 6)")
        # How long time we wait between the different scheduler jobs
        cursor.execute("INSERT INTO configuration_int ('name', 'value') VALUES ('scheduler_wait_msec', 10000)")

        # Only for when we run tests
        cursor.execute("INSERT INTO configuration_int ('name', 'value') VALUES ('_unsafe_small_rsa_keys', 0)")

        connection.commit()
    else:
        print('There were errors.')


if len(sys.argv) > 1 and sys.argv[1] == '--generate':
    import database_query as dq
    if not dc.database_exists():
        create_software_database()
