# Copyright (C) 2020 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import sys
import errno
import socket
from struct import pack, unpack

try:
    from Cryptodome.Cipher import PKCS1_OAEP
except ImportError:
    print("Error 1: You need to install Cryptodome:\npip3 install pycryptodomex\n")
    exit(-7)

from configuration import get_configuration_value
import binascii

import time
import struct

from encryption import CipherHelper
from ip_version import typeOfIPAddress

'''
This package sends a *complete* data package, or fails.
To do this, the size of the data to send is added to the package, so that
when receiving, we know if we got all the data or just a bit.
'''

'''
SecureCommunication

First package from N1 to N2:
send to N2: N2_RSAPUBKEY('GREETINGS!'+AES,IV)   # AES&IV from N1

N2 decrypts msg, if comprehensible (e.g it starts with 'GREETINGS!'), extract AES keypair
N2 responds with a, at least 32 bytes, meassage: 'FOR THE PEOPLE'+DATETIME or some random junk
N1 now knows N2 is authentic (or it is u to N2 to be) and now we can communicate securely

todo: N2 needs to authenticate N1

'''


# Class for initiating secure communication with another node
# Connects to a node with a GREETINGS! query, encrypted with the nodes public rsa key
# Also an AES keypair is sent over which now will be used for communication
class SecureCommunication:
    use_extensive_logging = False

    def __init__(self, ip, port, other_rsa_pub_key, handshake_timeout=5):
        self.ip = ip
        self.port = port
        self.other_rsa_pub_key = other_rsa_pub_key
        self.handshake_timeout = handshake_timeout

        self.ch = None

        self.connect()

    def connect(self):

        self.ch = CipherHelper()
        ip_version = typeOfIPAddress(self.ip)
        if ip_version == 'Invalid':
            self.socket = None
            print('#Warning: tried to connect to an invalid ip address:', self.ip)
            sys.stdout.flush()
            return

        # print('IP version detected:', ip_version, '(' + str(ip) + ')')
        sys.stdout.flush()
        # Connect to other node
        if ip_version == 'IPv4':
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if ip_version == 'IPv6':
            self.socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM, 0)

        self.socket.settimeout(self.handshake_timeout)
        # print("Socket timeout:", self.socket.gettimeout())
        # print("Try to open a secure connection to:",str(ip) + ':' + str(port))
        try:
            if ip_version == 'IPv4':
                self.socket.connect((self.ip, int(self.port)))
            if ip_version == 'IPv6':
                self.socket.connect((self.ip, int(self.port), 0, 0))
        except Exception as e:
            #print(e, flush=True)
            # This error happens when the node is just not up at all:
            # print('There was a problem when trying to connect to ' + str(ip) + ':' + str(port) + ' :', e)
            self.socket = None
            return

        query = 'GREETINGS!' + self.ch.aes_key.hex() + ',' + str(self.ch.get_nonce())

        # crypt with rsa pubkey from other node:
        cipher = PKCS1_OAEP.new(self.other_rsa_pub_key)
        rsa_query = cipher.encrypt(query.encode())  # encode to bytes

        # Send RSA encrypted message
        if socket_secure_send(self.socket, rsa_query, extensive_logging=self.use_extensive_logging):
            answer = socket_secure_recv(self.socket, 0, extensive_logging=self.use_extensive_logging)
            if answer is not None:
                # If everything went well, the other node have encrypted the message with AES
                message = self.ch.decrypt(answer)
                if message != b'HI_MINDOKI':
                    print('SC connection refused, handshake was wrong when trying to talk to ' + str(self.ip) + ':' + str(self.port))
                    if message:
                        print('They answered:', message)
                        print('The message is of type:', type(message))
                    self.socket = None
                else:
                    # If the communication starts, the timeout will be set to the standard internet 30 seconds timeout
                    self.socket.settimeout(30)
                    #print('SC Securely connected to other node.')
            else:
                print('SC Unknown answer / error: answer is None')

                self.socket = None
        else:
            self.socket = None

    # True if link is open and working
    def is_active(self):
        return self.socket is not None

    # Sends a message and returns the recv
    def send(self, msg):
        if not self.is_active():
            return None

        if socket_secure_send(self.socket, self.ch.encrypt(msg), extensive_logging=self.use_extensive_logging):
            data = socket_secure_recv(self.socket, 0, extensive_logging=self.use_extensive_logging)
            if data is not None:
                return self.ch.decrypt(data)
            else:
                pass
                #print('Could not recv after request was sent')
        else:
            print('Could not send request')
        return None

    def close(self):
        if self.socket is not None:
            self.socket.close()
            self.socket = None


# This is the pair class with SecureCommunication but at the listening part
# Instantiate one when someone tries to connect to the listener
# It will get the AES key pair etc.
class SecureCommunicationListen:
    use_extensive_logging = False

    def __init__(self, socket, my_rsa_priv_key, max_read_size):
        try:

            if self.use_extensive_logging:
                print('SCL -- Starting listening --', flush=True)

            self.aes_key = None
            self.aes_iv = None
            self.socket = socket

            # print('SSL: recv incoming data')
            incoming = socket_secure_recv(self.socket, 800)  # Recv at least 1 RSA block (AES (key+iv) + 10 char handshake + padding)
            # Reading finalized

            # Avoid catching normal network errors:
            if incoming is not None:
                # decrypt with our private RSA key
                try:
                    cipher = PKCS1_OAEP.new(my_rsa_priv_key)
                    message = cipher.decrypt(incoming)
                except ValueError:
                    # We come here if the decryption didn't work, it happens if the node contacting us
                    # does not have use our RSA Pub Key to encrypt its opening sequence.
                    # Possible reasons:
                    # It's not a tenfingers node but some prober/pink-machine
                    # We have changed RSA keys
                    # We use an address someone else had befor (so with another RSA keypair)
                    # print("#Error: SCL could not connect to peer, RSA decryption error:", ValueError)
                    if self.use_extensive_logging:
                        print('SSL: the data was not encrypted with our pub key or something went wrong, abort.')
                        print('SSL: incoming=', incoming, flush=True)
                        print('SSL: ValueError=', str(ValueError), flush=True)
                    socket_secure_send(self.socket, b'BAD PUBLIC KEY FOR THIS IP:PORT.')
                    self.socket.close()
                    self.socket = None
                    return
            else:
                if self.use_extensive_logging:
                    print('SCL Handshake failed.')
                self.socket.close()
                self.socket = None
                return

            # Split up the query in requirement and payload data
            query = message[:10]  # Query is always 10 bytes
            data = message[10:]  # the payload

            # check for the initial greeting/handshake command:
            if query != b'GREETINGS!':
                if self.use_extensive_logging:
                    print('SCL Did not get greeting message, closing down link')
                self.socket.close()
                self.socket = None
                return
            if self.use_extensive_logging:
                print('SCL Got greeting message!', flush=True)

            # Payload should contain AES key and initialisation vector:
            tmp = data.split(b',')

            if len(tmp) != 2:  # error
                self.socket.close()
                self.socket = None
                return

            key = tmp[0]
            iv = tmp[1]

            if not iv.decode().isnumeric():
                # print("---------------------------------------> ", iv, type(iv), iv.decode())
                self.socket.close()
                self.socket = None
                return

            # split out AES, key is in bytes, IV is an int
            self.ch = CipherHelper(aes_key=binascii.unhexlify(key), nonce=int(iv.decode()))

            if self.use_extensive_logging:
                print('SCL Splitted out AES key & iv', flush=True)

            # return OK message, encrypted with AES
            rmsg = self.ch.encrypt('HI_MINDOKI'.encode())

            # send back handshake ACK AES(HI_MINDOKI)
            socket_secure_send(self.socket, rmsg)
            if self.use_extensive_logging:
                print('SCL -- Done, returning. --', flush=True)

        except socket.error as err_msg:
            print('SCL caught exception in constructor:', flush=True)
            print(err_msg, flush=True)
            self.socket = None

    # True if link is open and working
    def is_active(self):
        return self.socket is not None

    # Get queries and data from caller
    def recv(self):
        try:
            if not self.is_active():
                return None
            data = socket_secure_recv(self.socket, 0)
            if data is not None:
                return self.ch.decrypt(data)
            else:
                # print('SCL Could not recv after request was sent')
                return None
        except socket.error as err_msg:
            print('SCL caught exception in recv:', flush=True)
            print(err_msg, flush=True)
            self.socket = None
            return None

    # Send msg and data back to caller
    def send(self, msg):
        try:
            if not self.is_active():
                return None

            if socket_secure_send(self.socket, self.ch.encrypt(msg)):
                data = socket_secure_recv(self.socket, 0)
                if data is not None:
                    return self.ch.decrypt(data)
                else:
                    pass
                    #print('Listener: Could not recv after request was sent')
            else:
                print('Listener: Could not send request')
            return None
        except socket.error as err_msg:
            print('SCL caught exception in send:', flush=True)
            print(err_msg, flush=True)
            self.socket = None
            return None

    def close(self):
        try:
            if self.socket is not None:
                self.socket.close()
                self.socket = None
        except socket.error as err_msg:
            print('SCL caught exception in close:', flush=True)
            print(err_msg, flush=True)
            self.socket = None


# Helpers for sending a specific sized package:


# Encodes a long long (8bytes) for the size of the rest of the data
# Then sends it by packages
# Sends msg as a sting, or opens the file if file 1= None and reads it and sends it off by chunks (So we won't fill up all the RAM)
def socket_secure_send(s, msg, file=None, extensive_logging=False):
    if extensive_logging:
        print("SSS -- socket_secure_send starting --", flush=True)
        print('SSS send message')

    # As we all know, data sent over the internet might be split up, so send how many
    # bytes we'll try to push through here. An unsigned 8Byte integer should do the trick
    # hopefully for the foreseeable future:
    datasize = len(msg)
    lengthdata = pack('>Q', datasize)
    # Send a long long, little endian encoded information about the msg size
    try:
        # Send size of data to come
        if extensive_logging:
            print('SSS Send length data', len(lengthdata),' should be 8, extracted datasize to receive=', datasize)
        s.send(lengthdata)

        # Send off the actual data
        if extensive_logging:
            print("SSS Send data of size", lengthdata)

        send_by_packages = True

        if extensive_logging:
            print('SSS --- Send data by packages Start ---')

        # send by packets
        max_packet_size = get_configuration_value('server_send_package_size', 32768)
        data_size = len(msg)
        left_to_send = data_size
        sent = 0
        while left_to_send > 0:
            package_size = min(max_packet_size, left_to_send)
            if extensive_logging:
                print('SSS bytes left to send', left_to_send, ', send a data package of max size', package_size)
                #print(struct.unpack("B" * 7 + "I" * 24, s.getsockopt(socket.SOL_TCP, socket.TCP_INFO, 104)))

            # What? Didn't we already send the size of the data earlier?
            # We sure did, but this is so when someone is downloading your 1.5TB of
            # knitting recipes and videos, it won't choke the OS:s small buffers!
            # We wouldn't want that right? So here we'll use a smaller 32bits unsigned integer,
            # but beware, buffers can be small so don't use numbers too big!
            lengthdata = pack('>L', package_size)
            if extensive_logging:
                print('SSS send length data:', package_size)
            s.send(lengthdata)
            if extensive_logging:
                print('SSS length data sent.')

            # Now send a chunk of the data
            data_to_send = msg[sent:sent+package_size]
            data_to_send_len = len(data_to_send)
            if extensive_logging:
                print('SSS send(): len=' + str(data_to_send_len))
            a = s.send(data_to_send)
            if extensive_logging:
                print('SSS send(' + str(len(data_to_send))+') Done, returned:', a)

            sent += package_size
            left_to_send -= package_size

            # And wait for the little Ack!
            if extensive_logging:
                print('SSS wait for Chunk-ACK...')
            ack = secure_recv(s, 1)  # todo check the ack = b'1' or something
            if extensive_logging and ack:
                print('SSS Got Chunk-ACK:', ack)
        if extensive_logging:
            print('SSS --- Send data by packages Done ---')

        # Check if the data went through, other socket sends b'0' for success and others for error
        if extensive_logging:
            print("SSS recv ACK")

        if extensive_logging:
            print('SSS wait for (global) ACK...')
        ack = secure_recv(s, 1)
        if extensive_logging and ack:
            print("SSS Got ACK:", ack)
            print("SSS -- Done --", flush=True)

        return ack == b'0'
    except socket.error as error:
        if extensive_logging:
            print("SSS There was a problem in socket_secure_send:", str(error))

    return False


def read_chunk(s, chunk_size):
    try:
        chunk = b''
        while chunk_size > 0:
            part = secure_recv(s, chunk_size)  # try to recv the missing data in the chunk
            if part is None:
                s.close()
                return None

            chunk += part  # Do append instead
            chunk_size -= len(part)

        return chunk
    except socket.error as err_msg:
        print('SCL caught exception in read_chunk:', flush=True)
        print(err_msg, flush=True)
        return b''


# This one just catches pesky network errors instead of crashing the thread
def secure_recv(s, byte_len):
    try:
        b = s.recv(byte_len)
        return b
    except socket.error as err_msg:
        # print('SCL secure_recv caught exception:', err_msg, flush=True)
        return None


# Receive data in packages
# Recv:s in memory, or to a file if file != None
def socket_secure_recv(s, max_read_size, file=None, extensive_logging=False):
    # Recv the long long size data
    if extensive_logging:
        print("SSR -- socket_secure_recv starting --", flush=True)
        print("SSR Recv length data")

    try:
        # Do it raw so that we can get some extra information in case of error
        tmp = s.recv(8)
    except socket.error as e:
        err = e.args[0]
        if err == errno.EAGAIN or err == errno.EWOULDBLOCK:
            if extensive_logging:
                if err == errno.EAGAIN:
                    print('SSR no (size) data avaliable: errno.EAGAIN')
                if err == errno.EWOULDBLOCK:
                    print('SSR no (size) data avaliable: errno.EWOULDBLOCK')

            # No data available
            return None
        else:
            # Real error:
            if extensive_logging:
                print("SSR error: ", e)

            return None

    if len(tmp) != 8:
        if extensive_logging:
            print('SSR Warning: Socket probably closed on other side: Could not read size-variable from incoming stream, len=', len(tmp))
        return None

    (to_read, ) = unpack('>Q', tmp)

    if extensive_logging:
        print("SSR Recv length data, expected size to read:", to_read)

    # Log progress if file is bigger then 512KB
    log_progress = to_read >= 512 * 1024
    old_percent = -1
    start_time_sec = time.time()

    # We are now supposed to read to_read bytes of data, bail out if too big
    if max_read_size != 0 and to_read > max_read_size:
        if extensive_logging:
            print("SSR Wrong size / too much data, max_read_size=", max_read_size, " to_read=", to_read, " max_read_size=", max_read_size)

        s.send(b'1')
        return None
    data = []
    data_length = 0
    read = to_read

    if extensive_logging:
        print('SSR --- Receive data Start ---')

    while read > 0:
        # Recv a chunk:

        chunk_size_data = secure_recv(s, 4)  # A 4 bit unsigned int for chunk size
        if not chunk_size_data:
            if extensive_logging:
                print('SSR Warning: Socket probably closed on other side: Could not get shunk size data')
            return None

        if len(chunk_size_data) != 4:
            if extensive_logging:
                print('SSR Warning: Socket probably closed on other side: Could not get shunk size data')
            return None

        (chunk_size,) = unpack('>L', chunk_size_data)
        if extensive_logging:
            print('SSR -- Read chunk  (chunk size=' + str(chunk_size) + ') --')

        part = read_chunk(s, chunk_size)
        if part is None:
            if extensive_logging:
                print('SSR Warning: part is empty, returning None.')
            return None

        #data = data + part
        data_length += len(part)
        data.append(part)

        # Show the download progress (if over 16kb)
        if log_progress:
            already_read = data_length
            left_to_read = to_read - already_read
            percent = 100 - int((left_to_read * 100) / to_read)
            if old_percent != percent:
                time_sec = time.time()
                duration_sec = (time_sec - start_time_sec)
                if duration_sec < 1:
                    duration_sec = 1
                kilobytespersec = (already_read / duration_sec) / 1024

                time_left = 0
                if kilobytespersec > 0.0:
                    time_left = left_to_read / (kilobytespersec * 1024.0)

                # Round off a bit for readibility:
                kilobytespersec = int(kilobytespersec)
                time_left = int(time_left * 10.0) / 10.0

                msg = 'Downloading File [' + str(percent) + '%] at ' + str(kilobytespersec) + ' kByte/sec\t ' + str(time_left) + ' secs left \r'

                print(msg, end="", flush=True)
                #print('Downloading File [%d%%] at kByte/sec\r' % percent, end="", flush=True)
                old_percent = percent

            # send ACK
        if extensive_logging:
            print('SSR -- All chunks read Done --')
            print('SSR Send chunk-Ack:')
        s.send(b'0')

        read = to_read - data_length

        # Show the download progress (if over 16kb)
        if log_progress:
            percent = 100 - int((read*100) / to_read)
            if old_percent != percent:
                print('Downloading File [%d%%]\r'%percent, end="", flush=True)
                old_percent = percent

        if log_progress:
            if read <= 0:
                time_sec = time.time()
                duration_sec = int(time_sec - start_time_sec)
                print('Downloading File done [100%] in around ' + str(duration_sec) + ' seconds\t\t\t\t\t\t'); ## /t to erase last line
            #else:
            #    print('Error downloading file...')

        if extensive_logging:
            print('SSR --- Receive data Done ---')

    # Reading finalized
    if extensive_logging:
        print("SSR Send ACK")

    try:
        s.send(b'0')
    except socket.error as err_msg:
        print('SCL Caught error as sending final ACK (other node probably just bailed):', flush=True)
        print(err_msg, flush=True)

    # build back the raw data object and return it:
    if log_progress:
        print('SSR Build back the binary data from the chunks')

    if extensive_logging:
        print("SSR -- Done --", flush=True)
    return b''.join(data)
