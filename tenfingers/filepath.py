# Copyright (C) 2025 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import os

import locator
from configuration import get_configuration_value


def get_address_translation_file_extension():
    return ".atf"


def generate_aes_relative_path(name):
    return os.path.join(get_configuration_value("data_owned_folder"), (name + '.aes'))


def generate_aes_filepath(name):
    return os.path.join(locator.create_path(get_configuration_value("data_owned_folder")), (name + '.aes'))


# Get the data name for an address translation file corresponding to a data_name
def generate_trans_name(data_name):
    return data_name + "_tr"


# Get the data name for an address translation file corresponding to a data_name
def generate_sub_name(data_name):
    return data_name + "_sub"


def generate_trans_file_name(data_name):
    return generate_trans_name(data_name) + get_address_translation_file_extension()


def generate_sub_file_name(data_name):
    return generate_sub_name(data_name) + get_address_translation_file_extension()


# Get the full path and filename to an address translation file
def get_trans_file_path(data_name):
    return os.path.join(get_configuration_value("data_owned_translation_folder"), generate_trans_file_name(data_name))


# Get the full path and filename to an address translation file
def get_sub_file_path(data_name):
    return os.path.join(get_configuration_value("data_owned_translation_folder"), generate_sub_file_name(data_name))
