# Copyright (C) 2025 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)


import os
import database_connection as dc
import database_query as dq
from pathlib import Path
from configuration import get_configuration_value
import filepath as fp


def _gen_folder_if_not_existing(folder):
    if not os.path.exists(folder):
        Path(folder).mkdir(parents=True, exist_ok=True)


def check_gen_folders():
    # Check if the folders are OK
    _gen_folder_if_not_existing(get_configuration_value("data_owned_folder"))
    _gen_folder_if_not_existing(get_configuration_value("data_owned_translation_folder"))
    _gen_folder_if_not_existing(get_configuration_value("data_shared_folder"))
    _gen_folder_if_not_existing(dc.get_database_folder())


def create_tr_sub_database_file(name):
    try:
        connection = dc.get_connection(False, name)
        # This is the base address list, used for downloading the base payload,
        # the data from the actual 10f link
        connection.execute('''CREATE TABLE address
                 (id INTEGER PRIMARY KEY AUTOINCREMENT,
                 pubkey     CHAR,
                 ip         CHAR,
                 port       CHAR
                  );''')
        connection.commit()

        # This is the address list for the Other tr/sub file
        connection.execute('''CREATE TABLE other_tr_sub_address
                 (id INTEGER PRIMARY KEY AUTOINCREMENT,
                 pubkey     CHAR,
                 ip         CHAR,
                 port       CHAR
                  );''')
        connection.commit()
    except:
        # This happens if the files are already there
        pass


def create_address_registry_files(data_name):
    # print("Create an address registry translation file:", trans_file_path, flush=True)
    trans_file_path = fp.get_trans_file_path(data_name)
    create_tr_sub_database_file(trans_file_path)

    # print("Create an address registry substitution file:", sub_file_path, flush=True)
    sub_file_path = fp.get_sub_file_path(data_name)
    create_tr_sub_database_file(sub_file_path)


# dict of addresses for this data_name
# call it with generate_trans_name(data_name) or generate_sub_name(data_name)
# to get the addresses pointing to those two links instead of the base datas links
def get_all_addresses(data_name):
    # todo: flag silent translate in the db for that (-afar) to work

    # get the nodes, having fingers pointing on our data
    data_owned_id = dq.get_link_data_id_by_name(data_name)
    afar = dq.get_afar_flag_from_id(data_owned_id)
    data_addresses = dq.get_address_list_as_dict(data_owned_id, afar)
    return data_addresses


# Gets the addresses out of a file
# Use other_addresses=True to get the other_tr_sub_addresses from a translation file
def load_addresses(file_path_name, other_addresses=False):
    address_list = {}
    if os.path.exists(file_path_name):
        connection = dc.get_connection(False, file_path_name)
        cursor = connection.cursor()
        if other_addresses:
            cursor.execute("SELECT pubkey, ip, port FROM other_tr_sub_address")
        else:
            cursor.execute("SELECT pubkey, ip, port FROM address")

        rows = cursor.fetchall()
        for row in rows:
            pubkey = row[0]
            ip = row[1]
            port = row[2]
            address_list[pubkey] = {'ip': ip, 'port': port}
    else:
        print("load_addresses() could not find file:", file_path_name, flush=True)

    return address_list


# To use with the atf files
# Updates the address part in the 'tr_sub_file_name' file
# by using the other_tr_sub_address in the 'atf_file' file
# Note: the atf file must already have been downloaded
def update_tr_sub_10f_addresses(tr_sub_file_name, atf_file):
    if dc.database_exists():
        #print("Check files exist", tr_sub_file_name, atf_file)

        if os.path.exists(tr_sub_file_name) and os.path.exists(atf_file):
            #print("Files exist")

            other_addresses = load_addresses(atf_file, other_addresses=True)
            if len(other_addresses):
                # Update the db
                #print("Connect to tr/sub file")
                connection = dc.get_connection(False, tr_sub_file_name)
                if connection:
                    #print("Connected")
                    connection.execute('''DELETE FROM address''')
                    connection.commit()

                    # Insert the new_addresses in trans / sub file
                    for rsa in other_addresses:
                        ip = other_addresses[rsa]['ip']
                        port = other_addresses[rsa]['port']
                        #print("Insert ip/port", ip, port)
                        connection.execute('''INSERT INTO address (rsa_pub, ip, port) VALUES (?, ?, ?)''',
                                           (rsa, ip, port))
                        connection.commit()


# Watch out, is called with the translation file name
# other_addresses is for the addresses pointing between the _tr and _sub files
def get_addresses_from_translation_file(data_name):
    trans_file_path = fp.get_trans_file_path(data_name)
    address_list = {}
    if os.path.exists(trans_file_path):
        connection = dc.get_connection(False, trans_file_path)
        cursor = connection.cursor()
        cursor.execute("SELECT pubkey, ip, port FROM address")

        rows = cursor.fetchall()
        for row in rows:
            pubkey = row[0]
            ip = row[1]
            port = row[2]
            address_list[pubkey] = {'ip': ip, 'port': port}

    return address_list


# todo: refact this by using load_addresses() instead
# Call with full filepath name
def get_other_addresses_from_tr_sub_file(file_path_name):
    address_list = {}
    if os.path.exists(file_path_name):
        connection = dc.get_connection(False, file_path_name)
        cursor = connection.cursor()
        cursor.execute("SELECT pubkey, ip, port FROM other_tr_sub_address")

        rows = cursor.fetchall()
        for row in rows:
            pubkey = row[0]
            ip = row[1]
            port = row[2]
            address_list[pubkey] = {'ip': ip, 'port': port}

    return address_list


# Reset an address translation file with its addresses, you should update it as the file have changed! (like ./10f.exe -u)
# Note: YOU must update version of the file, this is a raw, no checks update.
# if use_tr_file==False, then it updates the sub file
def set_addresses(data_name, use_tr_file):
    if use_tr_file:
        tr_sub_file_name = fp.get_trans_file_path(data_name)  # the .atf file
        other_name = fp.generate_sub_name(data_name)
    else:
        tr_sub_file_name = fp.get_sub_file_path(data_name)
        other_name = fp.generate_trans_name(data_name)

    if os.path.exists(tr_sub_file_name):
        # Get all addresses from the base data
        new_addresses = get_all_addresses(data_name)
        other_addresses = get_all_addresses(other_name)

        # Clean out trans file
        # print("Clean out address from", tr_sub_file_name, flush=True)
        connection = dc.get_connection(False, tr_sub_file_name)
        if connection:
            connection.execute('''DELETE FROM address''')
            connection.commit()
            connection.execute('''DELETE FROM other_tr_sub_address''')
            connection.commit()

            # Insert the new_addresses in trans file
            for rsa in new_addresses:
                ip = new_addresses[rsa]['ip']
                port = new_addresses[rsa]['port']
                connection.execute('''INSERT INTO address (pubkey, ip, port) VALUES (?, ?, ?)''', (rsa, ip, port))
                connection.commit()

            for rsa in other_addresses:
                ip = other_addresses[rsa]['ip']
                port = other_addresses[rsa]['port']
                connection.execute('''INSERT INTO other_tr_sub_address (pubkey, ip, port) VALUES (?, ?, ?)''', (rsa, ip, port))
                connection.commit()


# Updates tr & sub files according to what kind of data this is (base_data, TR, SUB)
def refresh_data_addresses(data_owned_id):
    # If this is a base_data, update the corresponding TR&SUB files, else update the 'other' SUB/TR file
    from insert_data import insert_our_data

    # Always update both _tr.atf and _sub.atf as they are always impacted:
    # if the base addresses changes, both carry those addresses
    # if say a TR files addresses affected, then it is affected, and so is the corresponding SUB file in the sub.other addresses
    # Same thing for SUB vs TR

    data_type = dq.get_translation_file_type(data_owned_id)
    name = dq.get_name_of_owned_data_id(data_owned_id)

    base_name = ''
    if data_type == 0:  #
        base_name = name

    if data_type == 1:  # _tr
        base_name = name[:-len(fp.generate_trans_name(''))]

    if data_type == 2:  # _sub
        base_name = name[:-len(fp.generate_sub_name(''))]

    # update_trans_file:
    set_addresses(base_name, use_tr_file=True)
    # The (translation) file to reencrypt
    trans_name = fp.generate_trans_name(base_name)
    trans_file_name = fp.generate_trans_file_name(base_name)  # name => name_tr.atf

    insert_our_data(trans_name, get_configuration_value("data_owned_translation_folder"), trans_file_name)  # is actually create OR update ...

    # update_sub_file:
    set_addresses(base_name, use_tr_file=False)
    # The (substitute) file to reencrypt
    sub_name = fp.generate_sub_name(base_name)
    sub_file_name = fp.generate_sub_file_name(base_name)  # name => name_sub.atf

    insert_our_data(sub_name, get_configuration_value("data_owned_translation_folder"), sub_file_name)  # is actually create OR update ...
