# Copyright (C) 2024 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

import os


def get_tenfingers_version():
    version = 'unknown'
    version_file = './version'
    if os.path.exists(version_file):
        with open(version_file, 'r') as f:
            version = f.read()
    return version
