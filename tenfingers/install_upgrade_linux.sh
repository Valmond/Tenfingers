# Copyright (C) 2024 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

# This script installs and sets up Tenfingers on Linux Mint.

# Check if pycryptodomex is installed
if [ "$(pip3 list | grep pycryptodomex)" ]; then
  echo "PyCryptodomeX is installed."
else
  echo "PyCryptodomeX is not installed, installing..."
  pip3 install pycryptodomex
  # To uninstall:
  # pip3 uninstall pycryptodomex
fi


# Check there is not a listener already running
if [ "$(ps aux | grep './[l]istener.py')" ]; then
  echo "A running listener is detected, we need to stop it..."
  kill $(ps aux | grep './[l]istener.py' | awk '{print $2}')
else
  echo "No running listener detected. Continuing with the install/upgrade."
fi


# Add the listener to crontab so that after each reboot, it starts
echo "Setup restart on reboot in crontab"
cmd="@reboot cd ~/Tenfingers/tenfingers && python3 ./listener.py&"
(crontab -l ; echo $cmd) | sort - | uniq - | crontab -
# To remove the command from the crontab:
# ( crontab -l | grep -v -F "$croncmd" ) | crontab -
# Or edit manually
# crontab -e


dir="$HOME/Tenfingers"
if [ -d "$dir" ]; then
  if [ -z "$( ls -A $dir )" ]; then
    echo "$dir exists but is empty, go ahead"
  else
    cd $dir
    git fetch > /dev/null
    if [ -z "$( git diff main origin/main )" ]; then
      echo "Actual Tenfingers version: $(<tenfingers/version )"
      echo "No changes detected, you are already running the latest branch."
    else
      echo "$dir is not empty. Updating ..."
      echo "A newer branch is detected, updating..."
      git reset --hard origin/main > git_reset.log
      echo "Actual Tenfingers version: $(<tenfingers/version )"
      echo "Tenfingers updated, you can now start the listener."
    fi
  fi
else
  echo "Setup..."
  # Make a folder for Tenfingers
  mkdir $dir
  cd $HOME
  # Get the git repositorys content
  git clone https://codeberg.org/Valmond/Tenfingers.git

  cd Tenfingers/tenfingers
  python3 ./setup.py
  # Note: we must run this in interactive mode or the alias will not be detected
  ./add_10f_alias.sh
  # Reload the aliases. You might have to relaunch the terminal for the aliases to function.
  source ~/.bashrc
fi


echo "Start the listener"
# Start the listener in another terminal, so that it works if we launch the script with a mouse double click too
cd ~/Tenfingers/tenfingers && gnome-terminal -- python3 ./listener.py&

# Sleep some time while the terminal starts
sleep 1
# Copyright (C) 2024 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

# This script installs and sets up Tenfingers on Linux Mint.

# Check if pycryptodomex is installed
if [ "$(pip3 list | grep pycryptodomex)" ]; then
  echo "PyCryptodomeX is installed."
else
  echo "PyCryptodomeX is not installed, installing..."
  pip3 install pycryptodomex
  # To uninstall:
  # pip3 uninstall pycryptodomex
fi


# Check there is not a listener already running
if [ "$(ps aux | grep './[l]istener.py')" ]; then
  echo "A running listener is detected, we need to stop it..."
  kill $(ps aux | grep './[l]istener.py' | awk '{print $2}')
else
  echo "No running listener detected. Continuing with the install/upgrade."
fi


# Add the listener to crontab so that after each reboot, it starts
echo "Setup restart on reboot in crontab"
cmd="@reboot cd ~/Tenfingers/tenfingers && python3 ./listener.py&"
(crontab -l ; echo $cmd) | sort - | uniq - | crontab -
# To remove the command from the crontab:
# ( crontab -l | grep -v -F "$croncmd" ) | crontab -
# Or edit manually
# crontab -e


dir="$HOME/Tenfingers"
if [ -d "$dir" ]; then
  if [ -z "$( ls -A $dir )" ]; then
    echo "$dir exists but is empty, go ahead"
  else
    cd $dir
    git fetch > /dev/null
    if [ -z "$( git diff main origin/main )" ]; then
      echo "Actual Tenfingers version: $(<tenfingers/version )"
      echo "No changes detected, you are already running the latest branch."
    else
      echo "$dir is not empty. Updating ..."
      echo "A newer branch is detected, updating..."
      git reset --hard origin/main > git_reset.log
      echo "Actual Tenfingers version: $(<tenfingers/version )"
      echo "Tenfingers updated, you can now start the listener."
    fi
  fi
else
  echo "Setup..."
  # Make a folder for Tenfingers
  mkdir $dir
  cd $HOME
  # Get the git repositorys content
  git clone https://codeberg.org/Valmond/Tenfingers.git

  cd Tenfingers/tenfingers
  python3 ./setup.py
  # Note: we must run this in interactive mode or the alias will not be detected
  ./add_10f_alias.sh
  # Reload the aliases. You might have to relaunch the terminal for the aliases to function.
  source ~/.bashrc
fi


echo "Start the listener"
# Start the listener in another terminal, so that it works if we launch the script with a mouse double click too
cd ~/Tenfingers/tenfingers && gnome-terminal -- python3 ./listener.py&

# Sleep some time while the terminal starts
sleep 1

