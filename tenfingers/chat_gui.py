# Copyright (C) 2023 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# # This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

# Tenfingers chat example, this time with a handy GUI

import os
import tarfile
from tkinter import *
from tkinter import ttk
from tkinter.filedialog import askopenfilename, asksaveasfilename
from tkinter import messagebox

from chatlib import check_setup, add_text_and_share, add_file_as_link_and_share, save_share_link, get_base_dir, get_chat_log

# 'me' is always the first
def get_all_users():
    users = ['me']
    others_dir = './chat_data/others/'
    tuples = [x[0] for x in os.walk(others_dir)]
    for d in tuples:
        if d != others_dir:
            user = os.path.basename(d)
            users.append(user)


    #users = ['tab1', 'tab2']
    #users = ['me', 'loulou', 'alice', 'bob']
    return users


class App(Tk):
    def __init__(self):
        super(App, self).__init__()

        self.title("Tenfingers Chat")
        self.minsize(700, 490)
        # self.wm_iconbitmap("icon.ico")

        # If needed, make folders, make the 10f we share etc
        check_setup()

        # Save off the tab widgets, by key=user name NOT VERY USED AT All
        self.tab = {}
        # Save off the text widgets, by key=user name
        self.text = {}

        self.active_tab_name = ""

        self.tab_control = ttk.Notebook(self)

        # Make a tab for each user including the special user, 'me'
        #for user in get_all_users():

        # Bind tab activation to a function so we can know what to refresh/show...
        self.tab_control.bind('<<NotebookTabChanged>>', self.on_tab_change)

        # Add text fields etx to the tabs
        for user in get_all_users():

            if user == 'me':
                tab = self._add_tab(user)
                # Warning, here we do it all manually as the 'me' user is actually us and needs special treatments...
                label_frame = LabelFrame(tab, text="Sharing is caring")
                label_frame.grid(column=0, row=0, padx=8, pady=4)

                # row 0
                label = Label(label_frame, text="What is on your mind:")
                label.grid(column=0, row=0, sticky='W')

                self.share_text_edit = Entry(label_frame, width=20)
                self.share_text_edit.grid(column=1, row=0)

                self.chat_button = Button(label_frame, text='Chat', width=12, command=lambda: self.chat_text())
                self.chat_button.grid(column=2, row=0, sticky='W')

                # row 1
                label2 = Label(label_frame, text="Share a file:")
                label2.grid(column=0, row=1)
                #self.share_file_edit = Entry(label_frame, width=20)
                #self.share_file_edit.grid(column=1, row=1)
                self.share_file_button = Button(label_frame, text='Share', width=12, command=lambda: self.chat_file())
                self.share_file_button.grid(column=2, row=1, sticky='W')

                # row 2
                text = Text(label_frame, state='disabled', width=44, height=10)
                text.grid(row=2, column=0, columnspan=4)
                text.insert('1.0', 'This is a Text widget demo')
                self.text[user] = text

                # Adding users and exporting 'me' as a shareable user address package
                label_frame_user = LabelFrame(tab, text="Handling users:")
                label_frame_user.grid(column=0, row=8, padx=8, pady=4)

                # Row 0
                label2 = Label(label_frame_user, text="Share my chat with others:")
                label2.grid(column=0, row=0)
                self.chat_button = Button(label_frame_user, text='Share my address', width=12, command=lambda: self.save_my_address())
                self.chat_button.grid(column=2, row=0, sticky='W')

                # Row 1
                label2 = Label(label_frame_user, text="Import other users to the chat:")
                label2.grid(column=0, row=3)
                self.chat_button = Button(label_frame_user, text='Add new address', width=12, command=lambda: self.add_new_user())
                self.chat_button.grid(column=2, row=3, sticky='W')

            else:
                self.add_user(user)
        self.tab_control.pack(expand=1, fill="both")

    def add_new_user(self):
        files = [('Chat Files', '*.chat')]
        file = askopenfilename(filetypes=files, defaultextension=files, title='Save chat file so we can share it with other chatters!')
        if len(file):
            user = os.path.splitext(os.path.basename(file))[0]
            print("Add user:", file)
            if user in self.tab:
                print("The user already exists")
            else:
                self.add_user(user)
                base_dir = get_base_dir()
                user_dir = os.path.join(base_dir, 'others', user)
                os.makedirs(user_dir)
                # untar file:
                if tarfile.is_tarfile(file):
                    with tarfile.open(file) as f:
                        f.extractall(path=user_dir)
                    messagebox.showinfo("User added successfully!")
                    return True
        messagebox.showerror("The user could not be added...")


    @staticmethod
    def save_my_address():
        files = [('Chat Files', '*.chat')]
        file = asksaveasfilename(filetypes=files, defaultextension=files, title='Save chat file so we can share it with other chatters!')
        if len(file):
            print("save my address as:", file)
            save_share_link(file)

    # Don't use except if you want to construct the tab manually
    def _add_tab(self, user):
        tab = ttk.Frame(self.tab_control)
        self.tab_control.add(tab, text=user)
        return tab

    def add_user(self, user):
        tab = self._add_tab(user)
        label_frame = LabelFrame(tab, text="Sharing is caring")
        label_frame.grid(column=0, row=0, padx=8, pady=4)
        # row 0
        text = Text(label_frame, width=84, height=18, wrap='word')
        text.grid(row=0, column=0, columnspan=3)

        #label_frame.pack(expand=True)

        #text.insert('1.0', '')
        #text.insert('1.1', 'This is a Text widget demo\n Yolo\n Yolo\n Yolo\n Yolo\n Yolo\n Yolo\n Yolo\n Yolo\n Yolo\n Yolo\n Yolo\n Yolo\n Yolo\n Yolo\n Yolo\n Yolo')
        text.insert('0.0', 'Chat messages\n')

        text['state'] = 'disabled'
        self.text[user] = text

        # row 1
        self.chat_button = Button(label_frame, text='Refresh', width=12, command=lambda: self.refresh())
        self.chat_button.grid(row=1, column=1, sticky='W')

    @staticmethod
    def make_user_file(user, file_name):
        if user == 'me':
            return os.path.join("chat_data/me", file_name)
        else:
            return os.path.join("chat_data/others/", user, file_name)

    def chat_text(self):
        text_to_pubbblish = self.share_text_edit.get()
        user = self.active_tab_name
        print("CHAT text", text_to_pubbblish)
        text = self.text[user]
        text['state'] = 'normal'
        text.insert('1.0', 'This is a Text widget demo')
        text.insert('3.4', 'This is a Text widget demo')
        # get all text from 0,1 to end:  text_content = self.me_text.get('1.0', 'end')
        text.delete('1.0', "end")
        text.insert('1.0', text_to_pubbblish)
        text['state'] = 'disabled'

        # Save off the file
        add_text_and_share(text_to_pubbblish)

        self.refresh()

    def chat_file(self):
        Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing
        file = askopenfilename()  # show an "Open" dialog box and return the path to the selected file
        print("CHAT file", file)
        add_file_as_link_and_share(file)
        self.refresh()

    def refresh(self):
        user = self.active_tab_name
        print("Refresh", user)
        # todo:
        cl = get_chat_log(user)
        print(cl)

        ## loop and set the text
        text = self.text[user]
        text['state'] = 'normal'
        #text['state'].pack(expand=True)
        # get all text from 0,1 to end:  text_content = self.me_text.get('1.0', 'end')
        text.delete('1.0', "end")

        for idx in range(1, len(cl) + 1):
            t = "#" + str(idx) + " " + cl[idx]
            text.insert('end', t + '\n')

        text.insert('end', '...')

        text['state'] = 'disabled'

    # Used to keep track on which tab is open
    def on_tab_change(self, event):
        self.active_tab_name = event.widget.tab('current')['text']
        print("Tab changed", self.active_tab_name)

    def _write_index(self, user, idx):
        index_file_path = self.make_user_file(user, "_index.txt")
        with open(index_file_path, 'w') as f:
            f.write(str(idx))

    def get_file_name_type(self, user, idx):
        pass   # todo:  return txt, 10f, unknown ...


    # delete I guiss?
    def refresh_tab_text(self, user):
        pass   # todo:  load up all the textx and display in this tabs text widget
        # loop 1 .. index
        # check file type, and display accordingly



# Start it up
app = App()
while True:
    app.update_idletasks()
    app.update()



