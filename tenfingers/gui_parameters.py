import sys
import wx
import wx.lib.mixins.listctrl as listmix
from wx.lib.agw import ultimatelistctrl as ULC

import database_connection as dc
import database_query as dq





APPNAME = 'Tenfingers sharing protocol simple GUI'
APPVERSION = '1.0'
MAIN_WIDTH = 800
MAIN_HEIGHT = 500
TEXT_FONT_HEIGHT = 24


class GfxParameters(wx.Panel, listmix.ColumnSorterMixin):
    def __init__(self, parent):
        self.parameters = {}
        self.text_control = {}
        self.information_text = None
        self.list_ctrl = None

        # Setup
        self.parent = parent
        self.setup()

    def setup(self):
        wx.Panel.__init__(self, self.parent, -1, style=wx.WANTS_CHARS, size=(MAIN_WIDTH, MAIN_HEIGHT))

        self.information_text = wx.StaticText(self.parent, label="...")

        self.parameters = {}
        self.text_control = {}

        self.list_ctrl = ULC.UltimateListCtrl(self, -1, agwStyle=ULC.ULC_REPORT | ULC.ULC_HAS_VARIABLE_ROW_HEIGHT)  # | ULC.ULC_EDIT_LABELS)
        self.list_ctrl.InsertColumn(0, "Name", width=240)
        self.list_ctrl.InsertColumn(1, "Value", wx.LIST_FORMAT_RIGHT, width=140)
        self.list_ctrl.InsertColumn(2, "Save it")

        self.parameters = self.get_parameters()  # returns a numbered dict containing tuples
        items = self.parameters.items()
        index = 0
        for key, data in items:
            parameter_name = data[0]
            parameter_value = str(data[1])
            parameter_is_int = data[2]

            #pos = self.list_ctrl.InsertStringItem(index, parameter_name)

###
            pos = self.list_ctrl.InsertStringItem(sys.maxsize, parameter_name)
            textctrl = wx.TextCtrl(self.list_ctrl, -1, parameter_value, size=(200, TEXT_FONT_HEIGHT))
            self.list_ctrl.SetItemWindow(pos, 1, textctrl, expand=True)
            self.text_control[index] = textctrl

###
            #self.list_ctrl.SetStringItem(index, 1, parameter_value)  # old non editable text field

            button = wx.Button(self.list_ctrl, id=wx.ID_ANY, label="Save", size=(120, TEXT_FONT_HEIGHT))
            self.list_ctrl.SetItemWindow(pos, col=2, wnd=button, expand=True)
            self.list_ctrl.SetItemData(index, key)
            button.Bind(wx.EVT_BUTTON, self.update_parameter)
            button.SetName(str(index))  # Store off the index in its name until we figure out how to do it better

            # self.all_other_data[index] = (data[0], "")  # Save the file
            index += 1

        # Now that the list exists we can init the other base class,
        # see wx/lib/mixins/listctrl.py

        #listmix.ColumnSorterMixin.__init__(self, 3)
        #self.Bind(wx.EVT_LIST_COL_CLICK, self.OnColClick, self.list_ctrl)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.list_ctrl, 1, wx.ALL | wx.EXPAND, 5)
        # sizer.Add(self.information_text, 0, wx.ALL | wx.EXPAND, 5)
        self.SetSizer(sizer)

    @staticmethod
    def get_parameters():  # (Name, Value, Is_Int)
        r = {}
        index = 0
        rows = dc.query_all("SELECT name, value FROM configuration_string")
        for row in rows:  # maybe this is not necessary... but as it should work, lets check that later
            name = row[0]
            value = row[1]
            r[index] = (name, value, False)  # If you add information here, also do it when you reconstruct the tuple in update_parameter()
            index += 1
        rows = dc.query_all("SELECT name, value FROM configuration_int")
        for row in rows:  # maybe this is not necessary... but as it should work, lets check that later
            name = row[0]
            value = row[1]
            r[index] = (name, value, True)  # If you add information here, also do it when you reconstruct the tuple in update_parameter()
            index += 1

        return r

    def update_parameter(self, event):
        parameter_id = int(event.GetEventObject().GetName())  # Yeah I know...
        row = self.parameters[parameter_id]
        name = row[0]
        last_value = row[1]
        is_integer = row[2]
        textctrl = self.text_control[parameter_id]
        new_value = textctrl.Value
        from gui_main import Notifier
        if is_integer and not new_value.isdigit():
            Notifier.notify("The content of [" + name + "] must be an integer, not [" + new_value + "]")
            textctrl.Value = str(last_value)
            return
        if len(new_value) == 0:
            Notifier.notify("The content of [" + name + "] cannot be empty.")
            textctrl.Value = str(last_value)
            return

        print("update parameter", name, new_value, flush=True)
        if new_value != "":
            if is_integer:
                dc.update("UPDATE configuration_int SET value = ? WHERE name = ?", (str(new_value), name))
            else:
                dc.update("UPDATE configuration_string SET value = ? WHERE name = ?", (new_value, name))
            self.parameters[parameter_id] = (row[0], new_value, row[2])  # Update "last_value" by recreating the tuple
            Notifier.notify("The content of [" + name + "] was set to " + new_value)
        else:
            pass
            # Pop warning it can't be empty

    #