Trouble shooting guide

Use Python 3.6 or better

#In scheduler.py:
"No nodes exist"

This means your node is not connected to any other node just yet.
To fix this, you'll need to use a link, get a fresh link somewhere, say link.10f, and use it in a shell.
To do this, copy the link to where the python scripts live, open a shell there and type this (without the $):
$ python3 get_data_from_link.py link.10f

This will connect to nodes and try to download the data link.10f points to, but also store away the node addresses.
If the nodes exist, you're on! Otherwise your link is too old and you have to hunt a fresher one down.

// Limitations:
For now, there is a 15MB hard limit for file exchange, so no files bigger than 15MB can be used.

// Linux & Windows:

If there are any python errors talking about Crypto something, try this:
Note: pycryptodome conflicts (maybe not always?) with pycrypto so we'll need to remove it if installed.

(No ./ on windows, so just pip bla bla)
./pip uninstall Crypto
./pip uninstall pycryptodome
./pip uninstall pycrypto
./pip install pycrypto
(in /Scripts/ folder of your Python 3.x folder)


// Windows:

Uninstall windows and install Linux!
No just kidding, here are some known caveats though:

# Error when installing pycrypto: "Build tools for MSVC 14" needed:
Don't go to the link proposed (or do it anyway because you are a rebel!)
You can find them here (as of 2019), and yes you can use the 2019 version (skip the C# parts when installing if you want to)!
https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2019

Install them and reboot 6 times. No just kidding, come back!

You do have to reboot once though, bummer.

Now get back to the python folder, in the windows cmd (win+r -> type cmd, enter) and rerun the
pip install pycrypto
NOTE: You should possibly run the cmd line as administrator


If it goes downhill from here with this errors (Python 3.6 and later):

> Running setup.py install for pycrypto ... error
> ERROR: Command errored out with exit status 1:
(It continues for some pages, all in blood red)

Then it's probably because #include<stdint.h> is removed from some python package.
(thank you stackoverflow: https://stackoverflow.com/questions/41843266/microsoft-windows-python-3-6-pycrypto-installation-error )
So the solution is to: force the VC compiler to include stdint.h. A solution as inelegant as hacky, but what the hell, lets get going:


You will need to hunt down two files and update the commands below, vcvars140.bat and stdint.h (both in the BuildTools directory)

Now lets go ahead!

cd "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\Common7\Tools\vsdevcmd\ext\vcvars\"
vcvars140.bat amd64
cd \
set CL=-FI"C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Tools\MSVC\14.23.28105\include\stdint.h"

Ready? Lets run the pip again!
pip install pycrypto

What a wonderful operative system!
Nah, it's okay. At least to play games on (ok, ok I'll stop!)


# Error:
ImportError: No module named 'winrandom'

Problem is solved by editing string in crypto\Random\OSRNG\nt.py:
(for example: C:\Python35\Lib\site-packages\Crypto\Random\OSRNG\nt.py)

import winrandom

to

from . import winrandom


#Error
AttributeError: module 'time' has no attribute 'clock'

time.clock is deprecated in later python distributions, but pycrypt is not aware of that so lets change it.
Open the file:
C:\Python38\lib\site-packages\Crypto\Random\_UserFriendlyRNG.py
and change this line
t = time.clock()
to
t = time.perf_counter()



// General network:

// Forward your public internet address to the script

Say your public IP is 11.22.33.44, as is tradition

Your PC running the script is on a local address, say 192.168.0.1 (usually it's only the last number that changes like 192.168.0.X, sometimes its 192.162.1.X)
You can get this one by firing up the commanbdline and type:
ifconfig

It will spit out a bunch of text, on the top you might find something like this:
inet 192.168.0.13  netmask 255.255.255.0  broadcast 192.168.0.255

Here my local ip address is 192.168.0.13

Choose a port, (over 1024), say 10000

Now open the file 'config.json' and change the ip and port to your local address (here my is 192.168.0.13, and the port is 10000)
(you can leave the rest for now)
and save it.

Restart the listener script





