# Prerequisite

You need to **either** use Python, **or** the Frozen executables, and their prerequisites are as follows:


## Python: Python3 and CryptodomeX

You need Python 3.6 or better plus the library pycryptodomex.

To install Python, see install recommendations for your specific OS.

If you don't know if python is installed or which version is installed, use:

```
python3 --version
```

When you have Python 3.6 or better installed, you need to install CryptodomeX.

Just run:

```
pip3 install pycryptodomex
```

## Frozen executables: No specific prerequisites.

If you can't or don't want to use Python, then you can use the frozen executables instead. 

This does not need python (or CryptoDome, it's baked in in the executables), you just run the executables as is.

> The Linux executables has been built on a Linux Mint 21 setup, if you have trouble on another Linux OS, use the python version.

Just use the pre-frozen executables for your specific operative system.

Note: On Windows, newly frozen executables sometimes get flagged as dangerous files. Just don't use them.



## Next step

[Install Tenfingers](/manualinstall.html) 


![](img/logo.png)
