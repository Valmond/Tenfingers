# Installing/setup Tenfingers manually

## Get the tenfingers files

You may use python or frozen executables:


### Linux Python:

You can get the latest version from git:

These commands will create a tenfingers folder in your HOME folder and download the python files into it.

```
# Go to your users home folder (you can put it anywhere, it's just convenient to have it here)
cd $HOME
# Make a fonder for Tenfingers
mkdir tenfingers
cd tenfingers
# Get the git repositorys content
git clone https://codeberg.org/Valmond/Tenfingers.git
# Get only the python files
mv Tenfingers/tenfingers/*.py ./
mv Tenfingers/tenfingers/*.sh ./
# Remove the rest
rm Tenfingers -rf
```

Run the setup:

```
python3 ./setup.py
```


### Windows Python:

Download the latest version [here](http://www.mindoki.com/10f/Windows/).

Extract it and go to `/tenfingers_release/py_source` and get the python files.

Put them in any folder you want, or use them where they already are.

Run the setup:

```
python3 setup.py
```


### Linux or Windows frozen executables:

Download the latest version for your OS from tenfingers.org

[Download for Linux](http://www.mindoki.com/10f/Linux/)

[Download for Windows](http://www.mindoki.com/10f/Windows/)

Extract it and go to `/tenfingers_release/bin` and get all the executables.

Put them in any folder you want, or use them where they are.

Run the setup:

Linux:

```
./setup
```

Windows:

```
setup.exe
```

### Note for Linux and Windows Frozen executable users:

> **Further on the examples will be for Python on Linux, so you might need to adjust them a little.**

So for example, in an example for Linux with Python:

```
python3 ./10f.py test ...
```

it becomes, on Linux (without python):

```
./10f test ...
```

and on Windows (without Python):

```
10f.exe test ...
```

and on Windows *with* Python:

```
python 10f.py test ...
```



## Forwarding internet to your PC and start the node


You now need to forward a port from internet to your PC, for example the default port 46000.

This is usually made in the software of your internet proveders "box", or your router.

> This is the complicated part, get this up and running and everything else should be quite simple! Good luck.

You will also need to have your public IP address, say for example your public IP is 173.77.77.77

The setup tries to figure out your public IP address, check if it got it right:

```
python3 ./10f.py -p
```
This will show all the configurations, check `ip` is set to "173.77.77.77"

If you need to change is:

```
python3 ./10f.py -p ip 173.77.77.77
```

In the same way, if you don't use the default 46000 port, but say port 8080

```
python3 ./10f.py -p port 8080
```

> You need to restart the listener after IP or PORT changes.

**When the listener is running** ([see below](#run-the-listener)), **but not before**, you can check if the configuration is working:



```
python3 ./10f.py -test

```



### Run the listener

You might want to use another shell to run the listener but anyway you start it like this:

```
python3 ./listener.py
```
On Linux que can setup the listener to run automatically byy adding it to the crontab:

```
crontab -e
```

then add the line:

```
@reboot cd ~/Tenfingers/tenfingers && python3 ./listener.py&
```

Where ``` ~/Tenfingers/tenfingers ``` is where the python files are located.

Now the Tenfingers listener will start automatically after each reboot.

You can check if the listener is tunning using this command:

```
lsof -i tcp:46000
```

### Bootstrap the node swarm

If you have all set up, and the listener running, you still need to give it access to the tenfingers network.

[Download a test link](https://www.mindoki.com/10f/testlink.10f)

and then use 10f to download a small smample file from the network:

Linux:

```
./10f ./testlink.10f
```

Linux python:

```
python3 ./10f.py ./testlink.10f
```

Windows:

```
10f.exe testlink.10f
```

Windows python

```
python3 10f.py testlink.10f
```




## Next step

Now that you are all set (felicitations, you got it all up and running!), see the next step *Usage* for how to share data: [Usage](/usage.html) 


![](img/logo.png)
