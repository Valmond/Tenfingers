

# Usage

Sharing starts with adding some data to the tenfingers system, and then create a link that you can distribute.

Someone else that now has your link, can download the data.

You can then update the data, and the user can download the new data.

The examples are for Linux, for windows it is almost the same, just use ```10f.bat``` instead of ```10f```.

## Sharing a file or a folder



Share a file, for example image.jpg

```
10f -i my_stuff_to_share image.jpg
```

`my_stuff_to_share` is the name of the data shared, it is used to extract the link files, update the data and so on. It is just an example and you can share many many different things under different names.

To check out what you have already shared:

```
10f -l
```

> **Note:** the names ending with _tr and _sub are the translation and substitution files, they are internal files so don't touch them!

> **Note:** The number of 'fingers' is how many times your data is shared by other tenfinger nodes.


To share a folder, say ./images/ just add -f

```
10f -i -f my_stuff_to_share images
```

If you only want to share .jpg files, you can add a regex:

```
10f -i -f my_stuff_to_share images .\.jpg
```

Get the link so that you can share it with someone:

```
10f -c my_stuff_to_share
```

Now you should have three files, the link files:

```
my_stuff_to_share.10f

my_stuff_to_share_tr.10f

my_stuff_to_share_sub.10f
```

Those together will allow anyone to download the latest shared images (or what ever you share).


## Updating data

To update the data you are sharing, you just use the same syntax as creating it, so say you do not longer want to share 'image.jpg' but image2.jpg
you can update the `my_stuff_to_share` link like this:

```
10f -i my_stuff_to_share another_image.jpg
```

If you changed the image.jpg itself, say it's an art in progress, just reuse the exact same syntax as when you shared it the first time:

```
10f -i my_stuff_to_share image.jpg
```

You can of course freely decide to share a folder or any other data instead!


> **Note**: Sharing and updates sometimes takes some time.


## Using a link file and download the data from it:

So you just got `my_stuff_to_share.10f` from a friend or foe, to check it out just:

```
10f my_stuff_to_share
```

and it will be downloaded.


# Help

There is an inbuilt help that you can use like this:

```
10f -h
```

And if you want some help with a specific command, like inserting for example ( -i )

```
10f -h -i
```

will give you some pointers.



![](img/logo.png)

