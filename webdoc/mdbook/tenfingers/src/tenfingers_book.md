# Sharing your life with Tenfingers Book

## Creating your book: (Linux)

> Note: this is just an example, feel free to change anyting to your convenience

> Note: this is not optimized at all and merely an example for what can be done using the tenfingers sharing protocol.

Create a folder that will contain your lifes story, say /home/loulou/Desktop/TenfingersBook/my_book

In there, create a folder of todays date: 20240930

In there put a file: what i did today.txt and fill it with your thoughts.
Maybe you'd like to add a photo?

Go to the base folder (here : /home/loulou/Desktop/TenfingersBook/my_book )
and run this script:

> Note: you need to change the TENFINGERS_SRC_DIR variable so it points to where 10f.py lives

```
#!/usr/bin/bash
# Modify these:
# Where you can find the 10f.py file (and all the other python files)
TENFINGERS_SRC_DIR=${HOME}/tenfingers
# The name of the book you want to create
BOOK_NAME="my_book"
# Where your book is (this folder should contain folders, who themselves contains files)
BOOK_DIR=${HOME}"/Desktop/TenfingersBook"

######################################
# Do not modify these:
cd $TENFINGERS_SRC_DIR
python3 tenfingers_book.py $BOOK_NAME $BOOK_DIR
```

Run it and you should create three files:

```
my_book.10f
my_book_tr.10f
my_book_sub.10f
```

Those are the files you can now send to people who you want to share your wisdom with!

When you have updated your book, just run the script again, and that's it!

### Checking ouy your book

So someons just sent you three files:

```
my_book.10f
my_book_tr.10f
my_book_sub.10f
```

What should I do with them?

Simple, just make some folder somewhere, put them there and run this script:

> Note: you need to change the TENFINGERS_SRC_DIR variable so it points to where 10f.py lives


```
#!/usr/bin/bash
# Modify these:
# Where you can find the 10f.py file (and all the other python files)
TENFINGERS_SRC_DIR=${HOME}/tenfingers
# The name of the book you want to get (the name of the shortest 10f file)
BOOK_NAME="my_book"

######################################
# Do not modify these:
mkdir -p $BOOK_NAME
cd $BOOK_NAME
rm *.10f  2> /dev/null
cp ../my_book.10f my_book.10f
cp ../my_book_tr.10f my_book_tr.10f
cp ../my_book_sub.10f my_book_sub.10f

TENFINGERS_PY="${TENFINGERS_SRC_DIR}/10f.py"
TENFINGERS_BOOK="${BOOK_NAME}.10f"

python3 $TENFINGERS_PY $TENFINGERS_BOOK

# Clean up
rm "${BOOK_NAME}.10f"
rm "${BOOK_NAME}_tr.10f"
rm "${BOOK_NAME}_tr.atf"
rm "${BOOK_NAME}_sub.10f"
rm "${BOOK_NAME}_sub.atf"

# Get all the chapters:
echo "Get all the chapters"
for file in *.10f; do
    if [ -f "$file" ]; then
        no_extension=${file::-4}
        if [[ $no_extension =~ .(_tr|_sub)$ ]]; then
            #echo "tr/sub file, skipping"
            :
        else
            echo "Get chapter $file"
            python3 $TENFINGERS_PY $file
        fi
    fi
done

# Clean up
rm *.10f  2> /dev/null
rm *.atf  2> /dev/null
```

Open the index.htm file, and voilà !















