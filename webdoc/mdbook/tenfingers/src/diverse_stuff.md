# Diverse stuff


### Here I will list lesser needed information, experimental stuff and things I do not yet know what to do with.

---

There is an inbuilt help function:

```
10f -h
```

If you want help with a specific command, say for example `insert` ( -i ) just add that:

```
10f -h -i
```


---

The git repo is here :

```
https://codeberg.org/Valmond/Tenfingers
```


---

DEPRECATED: You can use the 10f frozen executable alone to download 10f link files.

---

There is an experimental GUI you can use together with the python installation, it lets you do basic things like upload and download and set parameters.

Just download and run the start_gui.sh in your tenfingers python folder.

You can get it here in the [git repositorys script folder](https://codeberg.org/Valmond/Tenfingers/src/branch/main/tenfingers)


---

The number of fingers is how many nodes shares your data, it might take some time to find nodes that wants to share your data, especially if the data is big!

Small files are often shared pro bono, the default is that files up to 64KB is stored for free, up to a total of 64MB.

---

A special thank you to Beej and his extraordinary book: Beej's guide to network programming. Never met him but the book is a must have for any network curius programmer!

---

If you want to make a chat program, a backup soft or any other kind of tech based on Tenfingers, you might want to check out the python file 'access.py' which contains some simplified functions to do basic things like inserting data or downloading.

---


