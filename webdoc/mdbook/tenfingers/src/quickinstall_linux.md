# QuickInstall Linux

To set up and install Tenfingers on Linux, you only need to do these two steps:

1: forward port 46000 to your PC

2: Run this script:

```
# Copyright (C) 2024 Ludvig Larsson aka Valmond ludviglarsson@protonmail.com
# This code is licensed under the terms of the GNU Public License, version 3 or later (GPL-3.0-or-later)

# This script installs and sets up Tenfingers on Linux Mint.

# Check if pycryptodomex is installed
if [ "$(pip3 list | grep pycryptodomex)" ]; then
  echo "PyCryptodomeX is installed."
else
  echo "PyCryptodomeX is not installed, installing..."
  pip3 install pycryptodomex
  # To uninstall:
  # pip3 uninstall pycryptodomex
fi


# Check there is not a listener already running
if [ "$(ps aux | grep './[l]istener.py')" ]; then
  echo "A running listener is detected, we need to stop it..."
  kill $(ps aux | grep './[l]istener.py' | awk '{print $2}')
else
  echo "No running listener detected. Continuing with the install/upgrade."
fi


# Add the listener to crontab so that after each reboot, it starts
echo "Setup restart on reboot in crontab"
cmd="@reboot cd ~/Tenfingers/tenfingers && python3 ./listener.py&"
(crontab -l ; echo $cmd) | sort - | uniq - | crontab -
# To remove the command from the crontab:
# ( crontab -l | grep -v -F "$croncmd" ) | crontab -
# Or edit manually
# crontab -e


dir="$HOME/Tenfingers"
if [ -d "$dir" ]; then
  if [ -z "$( ls -A $dir )" ]; then
    echo "$dir exists but is empty, go ahead"
  else
    cd $dir
    git fetch > /dev/null
    if [ -z "$( git diff main origin/main )" ]; then
      echo "Actual Tenfingers version: $(<tenfingers/version )"
      echo "No changes detected, you are already running the latest branch."
    else
      echo "$dir is not empty. Updating ..."
      echo "A newer branch is detected, updating..."
      git reset --hard origin/main > git_reset.log
      echo "Actual Tenfingers version: $(<tenfingers/version )"
      echo "Tenfingers updated, you can now start the listener."
    fi
  fi
else
  echo "Setup..."
  # Make a folder for Tenfingers
  mkdir $dir
  cd $HOME
  # Get the git repositorys content
  git clone https://codeberg.org/Valmond/Tenfingers.git

  cd Tenfingers/tenfingers
  python3 ./setup.py
fi

# Make a convenient alias '10f'
if alias 10f >/dev/null 2>&1; then
  echo "No need to create the alias for 10f as it does already exist."
else
  echo "Create alias for 10f"
  echo "#Tenfingers" >> ~/.bashrc
  echo "alias 10f='python3 $HOME/Tenfingers/tenfingers/10f.py'" >> ~/.bashrc
  source ~/.bashrc
  # As the source ~/.bashrc doesn't always work in skripts, let's make the temporary alias too
  # This doesn't always work, if so restart the terminal
  alias 10f='python3 $HOME/Tenfingers/tenfingers/10f.py'
fi

echo "Start the listener"
# Start the listener in another terminal, so that it works if we launch the script with a mouse double click too
cd ~/Tenfingers/tenfingers && gnome-terminal -- python3 ./listener.py&

# Sleep some time while the terminal starts
sleep 1

```

Instead of copy pasting the script into a terminal, you can copy paste the script in a file:

> install_upgrade_linux.sh

and give it execution rights

> chmod +x ./install_upgrade_linux.sh

Use the script to set up Tenfingers and then subsequently to update it.

# Bootstrap

[Download a test link](https://www.mindoki.com/10f/testlink.10f)

And in a freshly opened terminal (alias might not work in the terminal if you just set it all up):

> 10f testlink

Should download a testlink.txt and give your listener its first address.


For more: see [Usage](/usage.html) 


