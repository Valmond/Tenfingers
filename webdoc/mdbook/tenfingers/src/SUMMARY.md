# introductionSummary

[Tenfingers Sharing Protocol]()

[Introduction](introduction.md)

[Setup and usage]()
- [Quick Install Linux](./quickinstall_linux.md)
- [Quick Install Windows](./quickinstall_windows.md)
- [Manual Install (LEGACY)](./install.md)
  - [Prerequisite](./prerequisites.md)
  - [Install](./manualinstall.md)
- [Usage](./usage.md)
- [More](./more.md)
  - [Weekend Projects](./weekend_projects.md)
    - [A Book](./tenfingers_book.md)
    - [A Chat](./tenfingers_chat.md)
  - [Diverse stuff](./diverse_stuff.md)
- [Legal](./legal.md)
- [Whitepaper](./whitepaper.md)

