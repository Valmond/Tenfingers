# Introduction

Tenfingers is a decentralized sharing protocol that lets you share any file or data with anyone, or just a selected few!

Make a blog, a website or share your latest photos, it is all possible with Tenfingers. Share your link, and your friends will have access at all times about your latest whereabouts.

With oversharing, your data can be accessed at all times, even when your computer is turned off!

All it costs is some storage and some bandwidth for sharing, the rest is **totally free.**

### Basically:

You share data from others, because they share yours.

A simple and elegant incentive.

It is all encrypted so no one knows what they are sharing, and the only way to access your data is by the link file which is yours to distribute as you see fit.

Overshare, and many people will share your data, making it accessible at any time.

Use the Quick install [Linux](/quickinstall_linux.html) / [Windows](/quickinstall_windows.html) or go for the [Manual install](/prerequisites.html) to see what you need to use Tenfingers, or continue browsing this page for more in depth technical information:

### Technically:

You launch a listener that, with the help of a first link, connects to a loosely connected trust-less node swarm.

Basic handshake goes over RSA 4096 and then everything else is over AES 256-CTR.

The sharing is made reciprocally, with a default over sharing of 10 times (so ten nodes will hold and serve your data while you share data from those ten nodes).

Two extra tenfingers files are shared with the link (a translation file, and a substitution file) to prevent link decay. Share them alongside the link file if the longevity of the data shared is important.

## It is:

* FOSS (GPL-3.0-or-later, hit me up if you need another FOSS licence)

* Decentralized (Once you get a single address to the node in the swarm, it is 100% decentralized).

* Using Strong Encryption (RSA 4096, AES 256)

* Takedown robust (if the data is shared from many countries it will make it hard to take down)

* Possible to access your data even when your PC is turned off

* Possible to modify your shared data or your computer address (IP:PORT) without the need to re-distribute the link.

* Using a strong incentive to share.

* Accessible to anyone who can spare some disk space and some bandwith.

* Free from crypto or other payments.


The more you share, automatically, the more you are shared!

This is all inbuilt in the protocol, there is no central power controlling anything, and everything is free.



By the way, this is the official Tenfingers logo:

<img src="img/logo.png" alt="tenfingers logo" width="320"/>


## Next step

Now you can go on to the Quick install [Linux](/quickinstall_linux.html) / [Windows](/quickinstall_windows.html) to set it up,


or go to the [Prerequisite page](/prerequisites.html) to see more in depth what you need to set up Tenfingers manually.

