# QuickInstall Windows

To set up and install Tenfingers on Linux, you only need to do these two steps:

1: forward port 46000 to your PC

Download, configure and run Tenfingers:

1. Install python (https://www.python.org/downloads/windows/)

2. Install CryptoDomeX:

>  pip3 install pycryptodomex

3. Create a folder, for example a folder /Tenfingers/ on your desktop, so with USERNAME being your, you guessed it, user name, the folders full path is:

```C:\Users\USERNAME\Desktop\Tenfingers\```

In this folder, run:

> git clone https://codeberg.org/Valmond/Tenfingers.git

or if you do not have git installed, go to https://codeberg.org/Valmond/Tenfingers and in the [...] menu, download a zipped version that you can unzip in your folder.

4. Add "C:\Users\USERNAME\Desktop\Tenfingers\Tenfingers\tenfingers" to PATH   (this is where the python files live)

  In the start menu, search for "PATH" and select "Edit the system environment variables"

  Click [Environment Variables...]

  Add it to the PATH variable

  Note: commandline windows needs to be restarted for the new PATH variable to be detected

5. In a command line window, go to the folder you just added to PATH

> cd "C:\Users\USERNAME\Desktop\Tenfingers\Tenfingers\tenfingers"

6. In that folder, run the setup:

> python setup.py

7. In that folder, run:

> python listener.py

to run the listener (you want to make this automatically at start up)

8. in that folder, make a file "10f.bat" and paste this into it:

```python "C:\Users\USERNAME\Desktop\Tenfingers\Tenfingers\tenfingers\10f.py" %*```

You can now type:

> 10f.bat

to use the 10f command line any where

# Bootstrap

[Download a test link](https://www.mindoki.com/10f/testlink.10f)

And in a freshly opened terminal (the PATH will not be found in the terminal if you just set it all up):

> 10f.bat testlink

Should download a testlink.txt and give your listener its first address.

Note:

Examples might use ```10f``` which is a Linux alias, you will need to une ```10f.bat``` instead because you are on windows.

For more: see [Usage](/usage.html) 


