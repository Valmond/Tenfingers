# Weekend Projects

### Here I have gathered some less quality projects, made quickly to test the network and to see what it can be useful for.

Take the projects as a learning experience, it's a mix of shell commands and python, made with the intent to teach whenever possible. This is not always a success of course.

> Note: those are mostly made for Linux with Python, but can surely be tweaked easily.

